<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $vars;
    public $js = [];
    public $seo = [
        'title'  => '',
        'desc'   => '',
        'robots' => ''
    ];
    public $customer;
    public $wishlist;

    function __construct()
    {
        parent::__construct();

        // Settings Settings & Variables
        $site = $this->SiteSettings->findOne(1);
        $vars = $this->SiteVars->findAll();

        $params = array();

        if($site)
        {
            $site = $site->toArray();

            unset($site->id);
            unset($site->created_at);
            unset($site->updated_at);

            $params['site'] = $site;
            $params['site']['social_links'] = isset($params['site']['social_links']) ? json_decode($params['site']['social_links'], 1) : [];
        }

        foreach ($vars as $d)
        {
            $params[$d->key] = $d->value;
        }

        $this->vars = $params;

        $customer = $this->session->has_userdata('logged_user') ? $this->session->userdata('logged_user') : '';

        if($customer)
        {
            $customer = $this->Customers
                ->find()
                ->where('id', $customer['id'])
                ->get()
                ->row_array();

            if($customer)
            {
                $this->customer = $customer;
                $this->session->set_userdata('wishlist', json_decode($customer['wishlist'], 1));
            }
        }

        $this->wishlist = $this->session->userdata('wishlist');
        $this->wishlist = $this->wishlist ? $this->wishlist : [];
    }

    public function getCart()
    {
        $this->load->model('Product_attributes_vals_model', 'AttributesValues');

        $cart = $this->session->userdata('cart');


        $cart = $cart ? $cart : [];

        $subtotal = $tax = $tax_per = $tax_inc = $nettotal = 0;

        if($cart)
        {
            $pids = $aids = [];
            $today = time();

            foreach ($cart as $c)
            {
                $pids[] = $c['product'];
                $aids   = array_merge($aids, $c['attribute']);
            }

            $pids = array_unique($pids);
            $aids = array_unique($aids);

            $products = $this->Products
                ->find()
                ->select('id, name, slug, image, price, sale_price, sale_start, sale_end')
                ->where_in('id', $pids)
                ->get()
                ->result_array();






            if(count($aids) > 0 ){
                $attributes = $this->AttributesValues
                    ->find()
                    ->select('id, name')
                    ->where_in('id', $aids)
                    ->get()
                    ->result_array();
            }





            foreach ($cart as $k => $v)
            {
                foreach ($products as $p)
                {
                    if($p['id'] == $v['product'])
                    {
                        $cart[$k]['product_name'] = $p['name'];
                        $cart[$k]['url']   = url('/product/'.$p['slug']);
                        $cart[$k]['image'] = resize_img($p['image'], 70, 93);


                        if($v['attribute'])
                        {
                            $attr = $this->ProductAttributes
                                ->find()
                                ->where('product_id', $p['id'])
                                ->where('attributes', implode(',', $v['attribute']))
                                ->get()
                                ->row_array();

                            $p['price']      = $attr['price'];
                            $p['sale_price'] = $attr['sale_price'];
                            $p['sale_start'] = $attr['sale_start'];
                            $p['sale_end']   = $attr['sale_end'];
                        }

                        $on_sale = $p['sale_price'] ? 1 : 0;
                        $on_sale = $p['sale_start'] && (strtotime($p['sale_start']) > $today) ? 0 : $on_sale;
                        $on_sale = $p['sale_end'] && (strtotime($p['sale_end']) < $today) ? 0 : $on_sale;

                        $cart[$k]['price']      = $p['price'];
                        $cart[$k]['sale_price'] = $on_sale ? $p['sale_price'] : '';
                    }
                }

                foreach ($v['attribute'] as $pa)
                {
                    foreach ($attributes as $a)
                    {
                        if($a['id'] == $pa)
                        {
                            $cart[$k]['attribute_name'][$pa] = $a['name'];
                        }
                    }
                }

                $discountItemId = $v['discountItemId'] ??"0";

                if (!$discountItemId)
                {
                    if($cart[$k]['sale_price'] && $cart[$k]['sale_price'] > 0)
                    {
                        $subtotal += $cart[$k]['sale_price'] * $cart[$k]['quantity'];
                    }
                    else
                    {
                        $subtotal += $cart[$k]['price'] * $cart[$k]['quantity'];
                    }
                }else
                {

                    $this->load->model('discount_items_model', 'discount_items');
                    $discountDetails = $this->discount_items
                        ->setAlias('t1')
                        ->find()
                        ->select('t1.amount,t1.quantity,t2.name,t2.message')
                        ->join("discounts AS t2", "t1.discount_id = t2.id")
                        ->where('t1.id', $discountItemId)
                        ->get()
                        ->row_array();

                    $subtotal        += ($discountDetails)?$discountDetails['amount']:0;
                    $quantity         = ($discountDetails)?$discountDetails['quantity']:0;
                    $discountName     = ($discountDetails)?$discountDetails['name']:"";
                    $discountMessage  = ($discountDetails)?$discountDetails['message']:"";
                    $search  = ['{quantity}','{product name}','{amount}'];
                    $replace = [$discountDetails['quantity'],$cart[$k]['product_name'],$discountDetails['amount']];

                    $cart[$k]['discountedPrice']    = $discountDetails['amount'];
                    $cart[$k]['discountName']    = $discountName;
                    $cart[$k]['discountMessage'] = " ( ".str_replace($search, $replace, $discountMessage)." ) ";
                }
            }

            $tax_per = isset($this->vars['tax_percentage']) ? (int) $this->vars['tax_percentage'] : 0;
            $tax_inc = isset($this->vars['tax_included']) && $this->vars['tax_included'] == 1 ? 1 : 0;

            $tax = round($subtotal * ($tax_per / 100), 2);
            $nettotal = $tax_inc ? $subtotal : ($subtotal + $tax);
        }

        $result = [
            'items'        => $cart,
            'subtotal'     => $subtotal,
            'tax'          => $tax,
            'tax_percent'  => $tax_per,
            'tax_included' => $tax_inc,
            'nettotal'     => $nettotal
        ];

        return $result;
    }
}

class Admin_Controller extends MY_Controller {

    public $pageTitle = '';
    public $user;
    public $permissions = array();

    function __construct()
    {
        parent::__construct();

        // Check for admin
        $is_admin = false;
        $user = $this->session->has_userdata('admin_user') ? $this->session->userdata('admin_user') : '';

        if($user)
        {
            $user = $this->Users->find()
                ->where('id', $user['id'])
                ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                ->get()
                ->row_array();

            if($user)
            {
                $is_admin = true;
                $this->user = $user;
            }
        }

        if(!$is_admin)
        {
            $this->session->unset_userdata('admin_user');
            redirect(url('office'));
        }

        // Check permission for current route
        $controller = strtolower($this->router->fetch_class());
        $action = str_replace(['datatable', 'store'], ['index', 'create'], strtolower($this->router->fetch_method()));

        if(!in_array($controller, array('dashboard', 'crud')))
        {
            // For child controllers
            $controller = str_replace(['menuitems'], ['menu'], $controller);

            // For single method controllers
            $action = in_array($controller, ['permissions', 'sitesettings']) && $action == 'create' ? 'index' : $action;
            $action = in_array($controller, ['menu']) && in_array($action, ['hierarchy', 'edithierarchy']) ? 'update' : $action;

            $permission = $this->Permissions->find()
                ->where('controller', $controller)
                ->where('action', $action)
                ->where('role_id', $user['role_id'])
                ->get()
                ->row_array();

            if (!$permission)
            {
                $this->session->set_flashdata('error', 'You don not have permission to access this page.');
                redirect(admin_url('dashboard'));
            }
        }

        // Allowed Permissions
        $permissions = $this->Permissions->find()
            ->where('role_id', $user['role_id'])
            ->get()
            ->result_array();

        foreach ($permissions as $p) {
            $this->permissions[] = strtolower($p['controller']) .'.'. strtolower($p['action']);
        }
    }
}
