<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Packing Slip</title>
<style type="text/css">
<?= file_get_contents(assets('css/mpdf.css')) ?>
</style>
</head>
<body>
	<table class="head container">
		<tr>
			<td class="header">
				<?php
				if($this->vars['site']['header_logo']) {
					echo '<img src="'. uploaded_url($this->vars['site']['header_logo']) .'" />';
				} else {
					echo $this->vars['site']['site_name'];
				}
				?>
			</td>
			<td class="shop-info">
				<div class="shop-name"><h3><?= $this->vars['site']['site_name'] ?></h3></div>
				<div class="shop-address">
					Retouradres:
					<br />
					<br />
					<?= nl2br($this->vars['return_address']) ?>
				</div>
			</td>
		</tr>
	</table>

	<h1 class="document-type-label">PAKBON</h1>

	<table class="order-data-addresses">
		<tr>
			<td class="address billing-address">
				<?php
                $addr = []; 
                foreach ($address as $a) {
                    if($a['type'] == 1) {
                        $addr = $a;
                    }
                }

                if($addr) {
                    echo $addr['first_name'] . ' ' . $addr['last_name'];
                    echo '<br />';
                    echo $addr['address1'];
                    echo '<br />';
                    echo $addr['address2'];
                    echo '<br />';
                    echo $addr['city'] . ', ' . $addr['postalcode'];
                    echo '<br />';
                    echo $addr['country'];
                    echo '<br />';
                    echo '<br />';
                }
                ?>
				<div class="billing-email"><?= $order['email'] ?></div>
				<div class="billing-phone"><?= $order['phone'] ?></div>
			</td>
			<td class="address shipping-address">
				<?php
                $addr = []; 
                foreach ($address as $a) {
                    if($a['type'] == 2) {
                        $addr = $a;
                    }
                }

                if($addr) {
                	echo '<h3>Ship To:</h3>';
                    echo $addr['first_name'] . ' ' . $addr['last_name'];
                    echo '<br />';
                    echo $addr['address1'];
                    echo '<br />';
                    echo $addr['address2'];
                    echo '<br />';
                    echo $addr['city'] . ', ' . $addr['postalcode'];
                    echo '<br />';
                    echo $addr['country'];
                }
                ?>
			</td>
			<td class="order-data">
				<table>
					<tr class="order-number">
						<th>Ordernummer:</th>
						<td><?= $order['id'] ?></td>
					</tr>
					<tr class="order-date">
						<th>Datum order:</th>
						<td><?= date('d-M-Y', strtotime($order['created_at'])) ?></td>
					</tr>
					<tr class="payment-method">
						<th>Verzending:</th>
						<td>Gratis verzending</td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>

	<table class="order-details">
		<thead>
			<tr>
				<th class="product">Product</th>
				<th class="quantity">Aantal</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($products as $p) { ?>
            <tr>
                <td class="product"><?= $p['name'] ?></td>
                <td class="quantity"><?= $p['quantity'] ?></td>
            </tr>
            <?php } ?>
		</tbody>
	</table>
	<div id="footer">
		Retouren binnen 14 dagen volgens onze retourvoorwaarden; niet gewassen, alleen gepast verder niet gedragen,<br />met aangehechte kaartjes en in originele verpakking.
	</div>
</body>
</html>