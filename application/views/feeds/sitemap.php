<?php $tags = []; ?>
<?= '<?xml version="1.0" encoding="UTF-8" ?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo url();?></loc>
        <priority>1.0</priority>
        <changefreq>daily</changefreq>
        <lastmod><?= $homepage ? date('c', strtotime($homepage['updated_at'])) : date('c') ?></lastmod>
    </url>

    <?php foreach($pages as $p) { ?>
    <url>
        <loc><?= url($p['slug']) ?></loc>
        <priority>0.9</priority>
        <lastmod><?= date('c', strtotime($p['updated_at'])) ?></lastmod>
        <changefreq>monthly</changefreq>
    </url>
    <?php } ?>

    <?php foreach($blogs as $b) { ?>
    <url>
        <loc><?= url($b['slug']) ?></loc>
        <priority>0.7</priority>
        <lastmod><?= date('c', strtotime($b['updated_at'])) ?></lastmod>
        <changefreq>monthly</changefreq>
    </url>
    <?php } ?>

    <?php foreach($products as $p) { ?>
    <?php $tags = $p['tags'] ? array_merge($tags, explode(',', $p['tags'])) : $tags ?>
    <url>
        <loc><?= url($p['slug']) ?></loc>
        <priority>0.9</priority>
        <lastmod><?= date('c', strtotime($p['updated_at'])) ?></lastmod>
        <changefreq>weekly</changefreq>
    </url>
    <?php } ?>

    <?php foreach($blog_cats as $c) { ?>
    <url>
        <loc><?= url('category/' . $c['slug']) ?></loc>
        <priority>0.3</priority>
        <lastmod><?= date('c', strtotime($c['updated_at'])) ?></lastmod>
        <changefreq>monthly</changefreq>
    </url>
    <?php } ?>

    <?php foreach($product_cats as $c) { ?>
    <url>
        <loc><?= url('product-category/' . $c['slug']) ?></loc>
        <priority>0.3</priority>
        <lastmod><?= date('c', strtotime($c['updated_at'])) ?></lastmod>
        <changefreq>monthly</changefreq>
    </url>
    <?php } ?>

    <?php $tags = array_unique($tags); ?>
    <?php foreach($tags as $t) { ?>
    <url>
        <loc><?= url('/product-tag/' . str_replace(' ', '-', $t)) ?></loc>
        <priority>0.3</priority>
        <changefreq>monthly</changefreq>
    </url>
    <?php } ?>
</urlset>