<?php if(in_array('orders.index', $this->permissions)): ?>
<li class="<?= is_request('orders') ? 'active' : '' ?>">
    <a href="<?= admin_url('orders') ?>"><i class="fa fa-star"></i> <span>Orders</span></a>
</li>
<?php endif; ?>

<?php if(in_array('customers.index', $this->permissions)): ?>
<li class="<?= is_request('customers') ? 'active' : '' ?>">
    <a href="<?= admin_url('customers') ?>"><i class="fa fa-users"></i> <span>Customers</span></a>
</li>
<?php endif; ?>

<?php if(in_array('pages.index', $this->permissions)): ?>
<li class="<?= is_request('pages') ? 'active' : '' ?>">
    <a href="<?= admin_url('pages') ?>"><i class="fa fa-files-o"></i> <span>Pages</span></a>
</li>
<?php endif; ?>

<?php if(in_array('blogcategories.index', $this->permissions) ||  in_array('blogs.index', $this->permissions)): ?>
<li class="treeview <?= is_request('blogcategories') || is_request('blogs') ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-files-o"></i> <span>Blogs</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <?php if(in_array('blogs.index', $this->permissions)): ?>
        <li class="<?= is_request('blogs') ? 'active' : '' ?>">
            <a href="<?= admin_url('blogs') ?>"><i class="fa fa-angle-right"></i> <span>List All</span></a>
        </li>
        <?php endif; ?>

        <?php if(in_array('blogcategories.index', $this->permissions)): ?>
        <li class="<?= is_request('blogcategories') ? 'active' : '' ?>">
            <a href="<?= admin_url('blogcategories') ?>"><i class="fa fa-angle-right"></i> <span>Categories</span></a>
        </li>
        <?php endif; ?>
    </ul>
</li>
<?php endif; ?>

<?php if(in_array('productcategories.index', $this->permissions) || in_array('productattributes.index', $this->permissions) || in_array('products.index', $this->permissions) || in_array('feeds.index', $this->permissions)): ?>
<li class="treeview <?= is_request('productcategories') || is_request('productattributes') || is_request('products') || is_request('feeds') || is_request('discounts') ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-cubes"></i> <span>Products</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <?php if(in_array('products.index', $this->permissions)): ?>
        <li class="<?= is_request('products') ? 'active' : '' ?>">
            <a href="<?= admin_url('products') ?>"><i class="fa fa-angle-right"></i> <span>List All</span></a>
        </li>
        <?php endif; ?>

        <?php if(in_array('productcategories.index', $this->permissions)): ?>
        <li class="<?= is_request('productcategories') ? 'active' : '' ?>">
            <a href="<?= admin_url('productcategories') ?>"><i class="fa fa-angle-right"></i> <span>Categories</span></a>
        </li>
        <?php endif; ?>

        <?php if(in_array('productattributes.index', $this->permissions)): ?>
        <li class="<?= is_request('productattributes') ? 'active' : '' ?>">
            <a href="<?= admin_url('productattributes') ?>"><i class="fa fa-angle-right"></i> <span>Attributes</span></a>
        </li>
        <?php endif; ?>

        <?php if(in_array('feeds.index', $this->permissions)): ?>
        <li class="<?= is_request('feeds') ? 'active' : '' ?>">
            <a href="<?= admin_url('feeds') ?>"><i class="fa fa-angle-right"></i> <span>Feeds</span></a>
        </li>
        <?php endif; ?>

        <?php if(in_array('discounts.index', $this->permissions)): ?>
        <li class="<?= is_request('discounts') ? 'active' : '' ?>">
            <a href="<?= admin_url('discounts') ?>"><i class="fa fa-angle-right"></i> <span>Quantity Discounts</span></a>
        </li>
        <?php endif; ?>
    </ul>
</li>
<?php endif; ?>

<?php if(in_array('media.index', $this->permissions)): ?>
<li class="<?= is_request('media') ? 'active' : '' ?>">
    <a href="<?= admin_url('media') ?>"><i class="fa fa-picture-o"></i> <span>Media</span></a>
</li>
<?php endif; ?>

<?php if(in_array('widgets.index', $this->permissions)): ?>
<li class="<?= is_request('widgets') ? 'active' : '' ?>">
    <a href="<?= admin_url('widgets') ?>"><i class="fa fa-th"></i> <span>Widgets</span></a>
</li>
<?php endif; ?>

<?php if(in_array('menu.index', $this->permissions)): ?>
<li class="<?= is_request('menu') || is_request('menuitems') ? 'active' : '' ?>">
    <a href="<?= admin_url('menu') ?>"><i class="fa fa-bars"></i> <span>Menus</span></a>
</li>
<?php endif; ?>

<?php if(in_array('faqcategories.index', $this->permissions) ||  in_array('faqs.index', $this->permissions)): ?>
<li class="treeview <?= is_request('faqcategories') || is_request('faqs') ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-question-circle"></i> <span>FAQs</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <?php if(in_array('faqs.index', $this->permissions)): ?>
        <li class="<?= is_request('faqs') ? 'active' : '' ?>">
            <a href="<?= admin_url('faqs') ?>"><i class="fa fa-angle-right"></i> <span>FAQ List</span></a>
        </li>
        <?php endif; ?>

        <?php if(in_array('faqcategories.index', $this->permissions)): ?>
        <li class="<?= is_request('faqcategories') ? 'active' : '' ?>">
            <a href="<?= admin_url('faqcategories') ?>"><i class="fa fa-angle-right"></i> <span>Categories</span></a>
        </li>
        <?php endif; ?>
    </ul>
</li>
<?php endif; ?>

<?php if(in_array('admins.index', $this->permissions) || in_array('roles.index', $this->permissions) || in_array('permissions.index', $this->permissions)): ?>
<li class="treeview <?= is_request('admins') || is_request('roles') || is_request('permissions') ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-user-circle"></i> <span>Administrators</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <?php if(in_array('admins.index', $this->permissions)): ?>
        <li class="<?= is_request('admins') ? 'active' : '' ?>">
            <a href="<?= admin_url('admins') ?>"><i class="fa fa-angle-right"></i> List Admins</a>
        </li>
        <?php endif; ?>

        <?php if(in_array('roles.index', $this->permissions)): ?>
        <li class="<?= is_request('roles') ? 'active' : '' ?>">
            <a href="<?= admin_url('roles') ?>"><i class="fa fa-angle-right"></i> Roles</a>
        </li>
        <?php endif; ?>

        <?php if(in_array('permissions.index', $this->permissions)): ?>
        <li class="<?= is_request('permissions') ? 'active' : '' ?>">
            <a href="<?= admin_url('permissions') ?>"><i class="fa fa-angle-right"></i> Permissions</a>
        </li>
        <?php endif; ?>
    </ul>
</li>
<?php endif; ?>

<?php if(in_array('sitesettings.index', $this->permissions)): ?>
<li class="treeview <?= is_request('sitesettings') ? 'active menu-open' : '' ?>">
    <a href="#">
        <i class="fa fa-cogs"></i> <span>Settings</span>
        <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
    </a>
    <ul class="treeview-menu">
        <?php if(in_array('sitesettings.index', $this->permissions)): ?>
        <li class="<?= is_request('sitesettings') ? 'active' : '' ?>">
            <a href="<?= admin_url('sitesettings') ?>"><i class="fa fa-angle-right"></i> General Settings</a>
        </li>
        <?php endif; ?>
    </ul>
</li>
<?php endif; ?>



<?php if(in_array('slider.index', $this->permissions)): ?>
    <li class="treeview <?= is_request('slider') ? 'active menu-open' : '' ?>">
        <a href="#">
            <i class="fa fa-cogs"></i> <span>Slider</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <?php if(in_array('slider.index', $this->permissions)): ?>
                <li class="<?= is_request('slider') ? 'active' : '' ?>">
                    <a href="<?= admin_url('slider') ?>"><i class="fa fa-angle-right"></i> Slider</a>
                </li>
            <?php endif; ?>
        </ul>
    </li>
<?php endif; ?>
