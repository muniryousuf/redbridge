<aside class="main-sidebar" id="sidebar-wrapper">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= assets('images/avatar.png') ?>" class="img-circle" alt="" />
            </div>
            <div class="pull-left info">
                <p><?= $this->user['first_name'] . ' ' . $this->user['last_name'] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?= is_request('dashboard') ? 'active' : '' ?>">
                <a href="<?= admin_url('dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            <?= $this->view('layouts/includes/admin/menu', array(), true) ?>
            <li><a href="<?= admin_url('logout') ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
        </ul>
    </section>
</aside>