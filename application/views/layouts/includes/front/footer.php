<footer class="footer">

    <div class="container-fluid">
        <div class="row top-footer">
            <div class="col-sm-12 text-center">
                <h3>CONNECT WITH US ON</h3>
                <a href="https://www.facebook.com" target="_blank"><i class="ion-social-facebook" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/Redbridge_Builders_Merchants/" target="_blank"><i class="ion-social-instagram-outline" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row middle-footer">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="widget">
                    <?php $col2 = widget('footer-col2'); ?>
                    <h6 class="widget_title"><?= strtoupper($col2['name']) ?></h6>
                    <?= $col2['content'] ?>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="widget">
                    <h6 class="widget_title">Category</h6>
                    <?php $categories = $this->ProCategories->getAllCategories(); ?>
                    <ul class="widget_links">
                        <?php foreach ($categories

                        as $value) { ?>
                        <a href="<?php echo url('/product-category/' . $value['slug']) ?>"><?php echo $value['name'] ?>
                            <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="widget">
                    <h6 class="widget_title">My Account</h6>
                    <ul class="widget_links">
                        <?php foreach (menu_items('footer-service') as $mi) { ?>
                            <li><a class="nav-link" href="<?= $mi['link'] ?>"><?= $mi['label'] ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="widget">
                    <?php $col1 = widget('footer-col1'); ?>
                    <h6 class="widget_title"><?= strtoupper($col1['name']) ?></h6>
                    <p><?= $col1['content'] ?></p>
                </div>
                <div class="widget">
                    <ul class="social_icons social_white">
                        <?php $social_icons = $this->vars['site']['social_links'];
                        print_r($social_icons); ?>
                        <?php if (isset($social_icons['facebook'])) { ?>
                            <li><a href="<?php echo $social_icons['facebook'] ?>" target="_blank"><i
                                            class="ion-social-facebook"></i></a></li>
                        <?php } ?>
                        <?php if (isset($social_icons['instagram'])) { ?>
                            <li><a href="<?php echo $social_icons['instagram'] ?>" target="_blank"><i
                                            class="ion-social-instagram-outline"></i></a></li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
            <!--            -->
            <!--            <div class="col-sm-3">-->
            <!--                <form class="newsletter" type="mail">-->
            <!--                    <i class="fa fa-envelope" aria-hidden="true"></i><input type="text"-->
            <!--                                                                            placeholder="Enter your email address">-->
            <!--                    <button type="submit"><i class="fa fa-search"></i></button>-->
            <!--                </form>-->
            <!--            </div>-->

        </div>
    </div>

    <!--    <div class="container-fluid">-->
    <!--        <div class="row bottom-footer">-->
    <!--            <div class="col-sm-12">-->
    <!--                <p style="font-size: 12px">© 2020 by BuildStop LTD T/A MGN Builders Merchants Reg. Office - Litho House,-->
    <!--                    65 Faringdon Ave, Romford RM3 8ST. Company Reg. No. 12437168 VAT No. 346483969</p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <div class="bottom_footer border-top-tran">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-md-0 text-center text-md-left">© 2020 All Rights Reserved by Softechweb</p>
                </div>
                <div class="col-md-6">
                    <ul class="footer_payment text-center text-lg-right">
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/visa.png" alt="visa"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/discover.png" alt="discover"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/master_card.png" alt="master_card"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images//paypal.png" alt="paypal"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/amarican_express.png" alt="amarican_express"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!---->
<!---->
<!--<footer class="footer_dark">-->
<!--    <div class="footer_top">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-3 col-md-3 col-sm-6">-->
<!--                    <div class="widget">-->
<!--                        --><?php //$col2 = widget('footer-col2'); ?>
<!--                        <h6 class="widget_title">--><?//= strtoupper($col2['name']) ?><!--</h6>-->
<!--                        --><?//= $col2['content'] ?>
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-3 col-sm-6">-->
<!--                    <div class="widget">-->
<!--                        <h6 class="widget_title">Category</h6>-->
<!--                        --><?php //$categories = $this->ProCategories->getAllCategories(); ?>
<!--                        <ul class="widget_links">-->
<!--                            --><?php //foreach ($categories
//
//                            as $value) { ?>
<!--                            <a href="--><?php //echo url('/product-category/' . $value['slug']) ?><!--">--><?php //echo $value['name'] ?>
<!--                                --><?php //} ?>
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
<!--                    <div class="widget">-->
<!--                        <h6 class="widget_title">My Account</h6>-->
<!--                        <ul class="widget_links">-->
<!--                            --><?php //foreach (menu_items('footer-service') as $mi) { ?>
<!--                                <li><a class="nav-link" href="--><?//= $mi['link'] ?><!--">--><?//= $mi['label'] ?><!--</a></li>-->
<!--                            --><?php //} ?>
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-12">-->
<!--                    <div class="widget">-->
<!--                        --><?php //$col1 = widget('footer-col1'); ?>
<!--                        <h6 class="widget_title">--><?//= strtoupper($col1['name']) ?><!--</h6>-->
<!--                        <p>--><?//= $col1['content'] ?><!--</p>-->
<!--                    </div>-->
<!--                    <div class="widget">-->
<!--                        <ul class="social_icons social_white">-->
<!--                            --><?php //$social_icons = $this->vars['site']['social_links'];
//                            print_r($social_icons); ?>
<!--                            --><?php //if (isset($social_icons['facebook'])) { ?>
<!--                                <li><a href="--><?php //echo $social_icons['facebook'] ?><!--" target="_blank"><i-->
<!--                                                class="ion-social-facebook"></i></a></li>-->
<!--                            --><?php //} ?>
<!--                            --><?php //if (isset($social_icons['instagram'])) { ?>
<!--                                <li><a href="--><?php //echo $social_icons['instagram'] ?><!--" target="_blank"><i-->
<!--                                                class="ion-social-instagram-outline"></i></a></li>-->
<!--                            --><?php //} ?>
<!---->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="bottom_footer border-top-tran">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-md-6">-->
<!--                    <p class="mb-md-0 text-center text-md-left">© 2020 All Rights Reserved by Softechweb</p>-->
<!--                </div>-->
<!--                <div class="col-md-6">-->
<!--                    <ul class="footer_payment text-center text-lg-right">-->
<!--                        <li><a href="#"><img src="assets/theme1/images/visa.png" alt="visa"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images/discover.png" alt="discover"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images/master_card.png" alt="master_card"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images//paypal.png" alt="paypal"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images/amarican_express.png" alt="amarican_express"></a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</footer>-->
<!---->

