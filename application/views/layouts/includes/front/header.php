<?php $cart = get_cart() ?>

<!DOCTYPE html>
<html>
<head>
    <title>REDBRIDGE</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style-front.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
<!--    <script src="--><?php //echo base_url() ?><!--assets/js/owl.carousel.js"></script>-->
</head>
<body>

<header>

    <div class="container-fluid">

        <div class="row top-header">
            <div class="col-sm-12">

                <p>Contact Us - <a href="tel:<?= $this->vars['site']['phone'] ?>"><?= $this->vars['site']['phone'] ?></a></p>
            </div>
        </div>

        <div class="row middle-header">
            <div class="col-sm-3">
                <a href="<?= url('/') ?>">
                    <img class="logo" src="<?= url('/') ?>assets/images/lredbridge-logo.png"></a>
            </div>
            <div class="col-sm-6">
                <form class="serach" action="<?= shop_url() ?>" method="GET">
                    <input type="text" placeholder="Search" id="search_input" name="s"
                           value="<?= isset($_GET['s']) ? $_GET['s'] : '' ?>">
                    <button type="submit"><i class="fa fa-search"></i></button>
                    <!--                        <button type="submit" class="search_icon"><i class="ion-ios-search-strong" id="basic-text1"></i></button>-->
                </form>
            </div>

            <div style="display: inherit;" class="col-sm-3 text-center">
                <a href=<?= url('/my-account') ?>><i class="fa fa-user" aria-hidden="true"></i><br>My Account</a>
                <ul class="navbar-nav attr-nav align-items-center my-cart-menu">

                    <li class="dropdown cart_dropdown">
                        <a class="nav-link cart_trigger" href="<?= cart_url() ?>" data-toggle="dropdown" style="padding-left: 35px;" href="#">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            <span class="amt"><?= format_money($cart['nettotal']) ?></span>
                            <span class="cart_count"><?= count($cart['items']) ?></span>
                            <br>

                            <!-- <p class="text-bask">Basket</p> -->
                        </a>
                        <!--                                <a class="nav-link cart_trigger" href="--><?//= cart_url() ?><!--" data-toggle="dropdown">-->
                        <!--                                    <i class="linearicons-cart"></i> <span-->
                        <!--                                            class="amt">--><?//= format_money($cart['nettotal']) ?><!--</span>-->
                        <!--                                    <span class="cart_count">--><?//= count($cart['items']) ?><!--</span>-->
                        <!--                                </a>-->
                        <?php if (count($cart['items']) > 0) { ?>
                            <div class="cart_box dropdown-menu dropdown-menu-right">
                                <ul class="cart_list">

                                    <?php
                                    $total = 0;
                                    foreach ($cart['items'] as $cart) {
                                        ?>
                                        <li>
                                            <a href="<?= cart_url() ?>"><img src="<?php echo $cart['image'] ?>"
                                                                             alt="cart_thumb1"><?php echo $cart['product_name'] ?>
                                            </a>
                                            <



                                                 <?php
                                                 if (isset($cart['discountedPrice']) && $cart['discountedPrice'] > 0) {
                                                     ?>
                                                     <span style="color: white">
                                                         <?php echo format_money($cart['discountedPrice']) ; $total += $cart['discountedPrice']; ?> </span>
                                                 <?php } else if ($cart['sale_price'] && $cart['sale_price'] > 0) { ?>

                                            <span class="cart_quantity"> <?php echo $cart['quantity'] ?>  x  <span
                                                        class="cart_amount">
                                                    <span
                                                            class="price_symbole"></span></span>
                                                     <span class="text_black"><?= format_money($cart['sale_price']) ?></span>
                                                     <?php $total += ($cart['sale_price'] * $cart['quantity']);
                                                 } else  { ?>
                                                          <span class="cart_quantity"> <?php echo $cart['quantity'] ?>  x  <span
                                                                      class="cart_amount">
                                                    <span
                                                            class="price_symbole"></span></span>
                                                     <span class="text_black"> <?= format_money($cart['price']);
                                                         $total += ($cart['price'] * $cart['quantity'])
                                                         ?></span>
                                                 <?php } ?></span>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="cart_footer">
                                    <p class="cart_total"><strong>Total:</strong> <span
                                                class="cart_price"> <span
                                                    class="price_symbole"></span></span><?php echo format_money($total) ."(Exl Vat)" ; ?>
                                    </p>
                                    <p class="cart_buttons"><a href="<?= cart_url() ?>"
                                                               class="btn btn-fill-line rounded-0 view-cart">View
                                            Cart</a><a
                                                href="<?= checkout_url() ?> "
                                                class="btn btn-fill-out rounded-0 checkout">Checkout</a></p>
                                </div>
                            </div>
                        <?php } ?>
                    </li>
                    <!-- <li class="wishlist"><a href="<?= wishlist_url() ?>" class="text_black mr-3">
                      <i class="fas fa-heart"></i> <span class="amt">Wish List</span>
                      <span
                        class="wishlist_count badge bg_pink ml-2 rounded-circle text-white"><?= count($this->wishlist) ?></span>
                    </a>
                </li> -->
                </ul>
            </div>
        </div>

<!--        <div class="row bottom-header">-->
<!--            <div class="col-sm-12 menu">-->
<!--                <ul class="nav navbar-nav">-->
<!--                    <li><a href="#">Home</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                    <li><a href="#">Product</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->
    </div>

    <!-- Simulate a smartphone / tablet -->
<!--    <div class="mobile-container">-->
<!--        Top Navigation Menu -->
<!--        <div class="topnav">-->
<!--            <a href="#home" class="active"></a>-->
<!--            <div id="myLinks">-->
<!--                <a href="#">Home</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#">Product</a>-->
<!--                <a href="#"></a>-->
<!--            </div>-->
<!--            <a href="javascript:void(0);" class="icon" onclick="myFunction()">-->
<!--                <i class="fa fa-bars"></i>-->
<!--            </a>-->
<!--        </div>-->
<!--    </div>-->
    <!-- Simulate a smartphone / tablet -->

</header>


<!-- START HEADER -->
<header class="header_wrap fixed-top header_with_topbar">
    <div class="bottom_header dark_skin main_menu_uppercase">
        <div class="container">
            <nav class="navbar">
                <div class="row" style="width: 100%;">
                   <!-- <div class="col-md-4 logo-mb">
                        <a class="navbar-brand" href="<?/*= url('/') */?>">
                            <img class="logo_light" src="<?/*= uploaded_url($this->vars['site']['header_logo']) */?>"
                                 alt="<?/*= $this->vars['site']['site_name'] */?>" style=""/>
                            <img class="logo_dark" src="<?/*= uploaded_url($this->vars['site']['header_logo']) */?>"
                                 alt="<?/*= $this->vars['site']['site_name'] */?>" style="max-width: 160px;"/>
                        </a>
                    </div>-->
                    <div class="center col-md-6">

                    </div>
                    <div class="right col-md-2">
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent"
                                aria-expanded="false">
                            <span class="ion-android-menu"></span>
                        </button>


                    </div>
                </div>
            </nav>
        </div>
    </div>
    <aside id="mobile-menu" class="d-none" style="background-color: #000;">
        <div class="sidebar_close_btn">
            <span></span>
        </div>
        <ul class="navbar-nav mx-auto">
                <?php $categories = $this->ProCategories->getAllCategories();
                foreach ($categories as $key => $parent) { ?>
                <li class="mobile-menu-nav">
                    <a href="<?php echo url('/product-category/' . $parent['slug']) ?>"
                       class="nav-link nav-mob-link"><?php echo $parent['name'] ?>
                    </a>
                       <span> > </span>

                    <ul class="nav-mob-submenu">
                                <?php if (isset($parent['second_level']) && count($parent['second_level']) > 0) {
                                    foreach ($parent['second_level'] as $child) { ?>
                        <li>
                            <a href="<?php echo url('/product-category/' . $child['slug']) ?>"><?php echo $child['name'] ?></a>
                        </li>
                                    <?php }
                                } ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
        <div class="col-lg-6 pl-0 pr-1 search_form d-none d-lg-block">
            <form action="<?= shop_url() ?>" method="GET">
                <div class="input-group pl-0 mr-auto mt-5 mb-3">
                    <input class="form-control my-0 py-1 pr-0 border-0" type="text" placeholder="Product Search"
                           name="s"
                           value="<?= isset($_GET['s']) ? $_GET['s'] : '' ?>" aria-label="Search">
                    <div class="input-group-append">
                        <button type="submit" class="input-group-text bg-white border-0" id="basic-text1"><i
                                    class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>

    </aside>
</header>
<!-- END HEADER -->
<?php $categories = $this->ProCategories->getAllCategories();


?>
<div class="navbar custom-nav navbar-expand-lg navbar-light bg-light desktop-menu">
    <div class="container">
        <div class="collapse navbar-collapse header-menu" id="navbarSupportedContent">
            <ul class="navbar-nav nav-menu-bar">
                <?php foreach (menu_items('top-bar') as $mi) { ?>
                    <!--<li class="">-->
                    <!--        <a href="<?= $mi['link'] ?>" class="nav-link" <?= $mi['attributes'] ?> class="text-dark"><?= $mi['label'] ?></a>-->

                    <!--</li>-->
                <?php } ?>

                <?php $categories = $this->ProCategories->getAllCategories();
                foreach ($categories as $key => $parent) { ?>
                    <li class="<?php echo $key; ?>">
                        <a href="<?php echo url('/product-category/' . $parent['slug']) ?>"
                           class="nav-link dropdown-toggle" id="dropDown1" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false"><?php echo $parent['name'] ?></a>
                        <div class="dropdown-menu level10" aria-labelledby="dropDown1">
                            <ul class="d-menu-inner">
                                <?php if (isset($parent['second_level']) && count($parent['second_level']) > 0) {
                                    foreach ($parent['second_level'] as $child) { ?>
                                        <li class="dmenu-item">
                                            <a class="heading"
                                               href="<?php echo url('/product-category/' . $child['slug']) ?>"><?php echo $child['name'] ?></a>

                                        </li>
                                    <?php }
                                } ?>

                            </ul>
                        </div>
                    </li>
                <?php } ?>


            </ul>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>

    $( document ).ready(function() {
        $(".mobile-menu-nav span").click(function() {
            $(this).closest(".mobile-menu-nav").find(".nav-mob-submenu").slideToggle();          return false
        });

        $(".navbar-toggler").click(function() {
          $('.nav-mob-submenu').css("display", "none");
        });



    });

</script>
