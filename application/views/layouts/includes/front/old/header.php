<?php $cart = get_cart() ?>

<!-- START HEADER -->
<header class="header_wrap fixed-top header_with_topbar">
    <div class="top-header">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <div class="d-flex align-items-center justify-content-center justify-content-md-start">
                        <!-- <ul class="contact_detail text-center text-lg-left">
              <li><i class="ti-mobile"></i><span><?= $this->vars['site']['phone'] ?></span></li>
            </ul> -->
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="text-center text-md-right">
                        <ul class="header_list">
                            <!-- <li><a href="compare.html"><i class="ti-control-shuffle"></i><span>Compare</span></a></li>
                                          <li><a href="wishlist.html"><i class="ti-heart"></i><span>Wishlist</span></a></li>
                                          <li><a href="login.html"><i class="ti-user"></i><span>Login</span></a></li> -->

                            <li><span class="mr-3 text_black">FREE SHIPPING12222!</span></li>
                            <?php foreach ($this->vars['site']['social_links'] as $k => $v) { ?>
                                <li><a href="<?= $v ?>" class="d-inline-flex justify-content-center align-items-center"><i
                                                class="fab fa-<?= $k == 'facebook' ? $k . '-f' : $k ?>"></i></a></li>
                            <?php } ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_header dark_skin main_menu_uppercase">
        <div class="container">
            <nav class="navbar">

                <a class="navbar-brand" href="<?= url('/') ?>">


                    <img class="logo_light" src="<?= uploaded_url($this->vars['site']['header_logo']) ?>"
                         alt="<?= $this->vars['site']['site_name'] ?>" style="height: 80px"/>
                    <img class="logo_dark" src="<?= uploaded_url($this->vars['site']['header_logo']) ?>"
                         alt="<?= $this->vars['site']['site_name'] ?>" style="height: 80px"/>
                </a>
                <div class="right">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-expanded="false">
                        <span class="ion-android-menu"></span>
                    </button>

                    <ul class="navbar-nav attr-nav align-items-center">
                        <!-- <li><a href="javascript:void(0);" class="nav-link search_trigger"><i class="linearicons-magnifier"></i></a>
              <div class="search_wrap">
                <span class="close-search"><i class="ion-ios-close-empty"></i></span>
                <form action="<?= shop_url() ?>" method="GET">
                  <input type="text" placeholder="Search" class="form-control" id="search_input" name="s"
                      value="<?= isset($_GET['s']) ? $_GET['s'] : '' ?>">
                  <button type="submit" class="search_icon"><i class="ion-ios-search-strong" id="basic-text1"></i></button>
                </form>
              </div>
              <div class="search_overlay"></div>
            </li> -->
                        <li class="dropdown cart_dropdown">
                            <a class="nav-link cart_trigger" href="<?= cart_url() ?>" data-toggle="dropdown">
                                <i class="linearicons-cart"></i> <span
                                        class="amt"><?= format_money($cart['nettotal']) ?></span>
                                <span class="cart_count"><?= count($cart['items']) ?></span>
                            </a>
                            <?php if(count($cart['items']) > 0) { ?>
                            <div class="cart_box dropdown-menu dropdown-menu-right">
                                <ul class="cart_list">

                                    <?php
                                    $total = 0;

                                    foreach ($cart['items'] as $cart) {

                                        print_r($cart);
                                        exit;
                                        ?>
                                        <li>
                                            <a href="<?= cart_url() ?>"><img src="<?php echo $cart['image'] ?>"
                                                                             alt="cart_thumb1"><?php echo $cart['product_name'] ?>
                                            </a>
                                            <span class="cart_quantity"> <?php echo $cart['quantity'] ?> x <span
                                                        class="cart_amount"> <span
                                                            class="price_symbole"></span></span><?php if ($cart['sale_price'] && $cart['sale_price'] > 0) { ?>
                                                    <span class="text_black"><?= format_money($cart['sale_price']) ?></span>
                                                    <?php $total += ($cart['sale_price'] * $cart['quantity']);
                                                } else { ?>
                                                    <span class="text_black"><?= format_money($cart['price']);
                                                        $total += ($cart['price'] * $cart['quantity'])
                                                        ?></span>
                                                <?php } ?></span>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="cart_footer">
                                    <p class="cart_total"><strong>Total:</strong> <span class="cart_price"> <span
                                                    class="price_symbole">£</span></span><?php echo $total; ?></p>
                                    <p class="cart_buttons"><a href="<?= cart_url() ?>" class="btn btn-fill-line rounded-0 view-cart">View
                                            Cart</a><a
                                                href="<?= checkout_url() ?> "
                                                class="btn btn-fill-out rounded-0 checkout">Checkout</a></p>
                                </div>
                            </div>
                            <?php  }?>
                        </li>
                        <li class="wishlist"><a href="<?= wishlist_url() ?>" class="text_black mr-3">
                                <i class="fas fa-heart"></i> <span class="amt">Wish List</span>
                                <span
                                        class="wishlist_count badge bg_pink ml-2 rounded-circle text-white"><?= count($this->wishlist) ?></span>
                            </a>
                        </li>

                        <li><span class="mr-3 text_black"> <a href="<?php echo url('my-account'); ?>">My Account</a></span></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <aside id="mobile-menu" class="d-none" style="background-color: #000;">
        <div class="sidebar_close_btn">
            <span></span>
        </div>
        <ul class="navbar-nav mx-auto">
            <?php $categories = $this->ProCategories->getAllCategories(); ?>
            <?php foreach ($categories as $value) { ?>
                <li>
                    <a href="<?php echo url('/product-category/' . $value['slug']) ?>"
                       class="nav-link"><?php echo $value['name'] ?>
                    </a>
                </li>
            <?php } ?>
        </ul>
        <div class="col-lg-6 pl-0 pr-1 search_form d-none d-lg-block">
            <form action="<?= shop_url() ?>" method="GET">
                <div class="input-group pl-0 mr-auto mt-5 mb-3">
                    <input class="form-control my-0 py-1 pr-0 border-0" type="text" placeholder="Product Search"
                           name="s"
                           value="<?= isset($_GET['s']) ? $_GET['s'] : '' ?>" aria-label="Search">
                    <div class="input-group-append">
                        <button type="submit" class="input-group-text bg-white border-0" id="basic-text1"><i
                                    class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <?php foreach (menu_items('top-bar') as $mi) { ?>
            <p><a href="<?= $mi['link'] ?>" <?= $mi['attributes'] ?> class="text-dark"><?= $mi['label'] ?></a></p>
        <?php } ?>
        <p class="text-dark">
            FREE SHIPPING!</p>
    </aside>
</header>
<!-- END HEADER -->

<div class="main-menu navbar navbar-expand-lg" style="background: #000;">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="dropdown">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <?php $categories = $this->ProCategories->getAllCategories(); ?>
                <?php foreach ($categories as $value) { ?>
                    <li>
                        <a href="<?php echo url('/product-category/' . $value['slug']) ?>"
                           class="nav-link"><?php echo $value['name'] ?>
                        </a>
                    </li>
                <?php } ?>
                <li><a class="nav-link nav_item" href="<?php echo url('contact-us') ?>">Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
