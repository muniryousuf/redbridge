<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $this->pageTitle ? $this->pageTitle . ' - ' : '' ?>Admin Panel - <?= SITE_NAME ?></title>
    <!-- Styles -->
    <link rel="stylesheet" href="<?= assets('css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/dataTables.bootstrap.min.css') ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.4/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?= assets('css/select2.min.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/AdminLTE.min.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/AdminLTE-red.min.css') ?>">
    <link rel="stylesheet" href="<?= assets('js/fancybox/jquery.fancybox.css') ?>">
    <link rel="stylesheet" href="<?= assets('css/admin_custom.css') ?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/css/multi-select.min.css" integrity="sha256-l4jVb17uOpLwdXQPDpkVIxW9LrCRFUjaZHb6Fqww/0o=" crossorigin="anonymous" />

    <!-- Scripts -->
    <script src="<?= assets('js/jquery.min.js') ?>"></script>
    <script src="<?= assets('js/jquery-ui.min.js') ?>"></script>
    <script src="<?= assets('js/bootstrap.min.js') ?>"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">
        <?= $header ?>
        <?= $sidebar ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <div class="box-body">
                <?php
                $success = $this->session->flashdata('success');
                $errors = $this->session->flashdata('error');

                $this->session->set_flashdata('success', '');
                $this->session->set_flashdata('error', '');
                ?>
                
                <?php if($success) { ?>
                <div class="alert alert-success"><?= $success ?></div>
                <?php } ?>

                <?php if($errors) { ?>
                <div class="alert alert-danger"><?= $errors ?></div>
                <?php } ?>

                <?php if($this->pageTitle) { ?>
                <section class="content-header">
                    <h1><?= $this->pageTitle ?></h1>
                </section>
                <?php } ?>

                <div class="content">
                    <?= $body ?>
                </div>
            </div>
        </div><!-- /.content-wrapper -->

        <footer class="main-footer">
            <strong>Copyright &copy; <?= date('Y') ?> <a href="<?= admin_url('dashboard') ?>"><?= SITE_NAME ?></a>.</strong> All rights reserved.
        </footer>
    </div>

<script src="<?= assets('js/jquery.validate.min.js') ?>"></script>
<script src="<?= assets('js/additional-methods.min.js') ?>"></script>
<script src="<?= assets('js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= assets('js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.datatables.net/responsive/2.2.4/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
<script src="<?= assets('js/adminlte.min.js') ?>"></script>
<script src="<?= assets('js/select2.min.js') ?>"></script>
<script src="<?= assets('js/nestable.js') ?>"></script>
<script src="<?= assets('js/ckeditor/ckeditor.js') ?>"></script>
<script src="<?= assets('js/ckeditor/adapters/jquery.js') ?>"></script>
<script src="<?= assets('js/fancybox/jquery.fancybox.min.js') ?>"></script>
<script src="<?= assets('js/admin_custom.js') ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/multi-select/0.9.12/js/jquery.multi-select.min.js" integrity="sha256-qe8stzuAY8ixwMqLzEMX7hTs1TGKpHagLXWQ11tm7kg=" crossorigin="anonymous"></script>
</body>
</html>