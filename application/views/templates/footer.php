<footer class="footer">

    <div class="container-fluid">
        <div class="row top-footer">
            <div class="col-sm-12 text-center">
                <h3>CONNECT WITH US ON</h3>
                <a><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row middle-footer">
            <div class="col-sm-6">
                <a href="#">Privacy and Cookie Policy</a><br>
                <a href="#">Advanced Search</a><br>
                <a href="#">Contact Us</a><br>
                <a href="#">Delivery Information</a><br>
                <a href="#">Terms and Conditions</a>
            </div>

            <div class="col-sm-6">
                <form class="newsletter" type="mail">
                    <i class="fa fa-envelope" aria-hidden="true"></i><input type="text"
                                                                            placeholder="Enter your email address">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>

        </div>
    </div>

    <div class="container-fluid">
        <div class="row bottom-footer">
            <div class="col-sm-12">
                <p style="font-size: 12px">© 2020 by BuildStop LTD T/A MGN Builders Merchants Reg. Office - Litho House,
                    65 Faringdon Ave, Romford RM3 8ST. Company Reg. No. 12437168 VAT No. 346483969</p>
            </div>
        </div>
    </div>

</footer>


<script>
    function myFunction() {
        var x = document.getElementById("myLinks");
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
        }
    }

    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            navigation: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true,
                    loop: true
                },
                600: {
                    items: 3,
                    nav: false,
                    loop: true
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: true
                }
            },
            margin: 10,
            loop: 10,
            width: 20,
            nav: true,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true

        });
    });
</script>

<!-- Option 1: Bootstrap Bundle with Popper -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>


<!-- Option 2: Separate Popper and Bootstrap JS -->

</body>
</html>
