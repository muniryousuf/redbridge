<?= form_open() ?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
        <li><a href="#tab_2" data-toggle="tab">Images</a></li>
        <li><a href="#tab_3" data-toggle="tab">Pricing</a></li>
        <li><a href="#tab_4" data-toggle="tab">Variations</a></li>
        <li><a href="#tab_5" data-toggle="tab">Meta Info</a></li>
        <li><a href="#tab_6" data-toggle="tab">Linked Products</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="form-group">
                <label for="name" class="required">Name</label>
                <input type="text" id="name" name="name" class="form-control" value="<?= isset($row['name']) ? $row['name'] : "" ?>" required />
            </div>

            <div class="row">
                <div class="form-group col-sm-3">
                    <label for="slug" class="required">Slug</label>
                    <input type="text" id="slug" readonly name="slug" class="form-control" value="<?= isset($row['slug']) ? $row['slug'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-3">
                    <label for="name">SKU</label>
                    <input type="text" id="sku" name="sku" class="form-control" value="<?= isset($row['sku']) ? $row['sku'] : "" ?>" />
                </div>

                <div class="form-group col-sm-3">
                    <label for="status">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option value="1" <?= isset($row['status']) && $row['status'] == 1 ? 'selected' : '' ?>>Active</option>
                        <option value="0" <?= isset($row['status']) && $row['status'] == 0 ? 'selected' : '' ?>>Inactive</option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="sort" class="required">Sort</label>
                    <input type="number" id="sort" name="sort" class="form-control" value="<?= isset($row['sort']) ? $row['sort'] : 0 ?>"  />
                </div>



                <div class="form-group col-sm-3">
                    <label for="status">Feature</label>
                    <select id="feature" name="feature" class="form-control">
                        <option value="1" <?= isset($row['is_feature']) && $row['is_feature'] == 1 ? 'selected' : '' ?>>Active</option>
                        <option value="0" <?= isset($row['is_feature']) && $row['is_feature'] == 0 ? 'selected' : '' ?>>Inactive</option>
                    </select>
                </div>


            </div>

            <div class="form-group">

                <label for="categories">Categories</label>
                <select id="categories" name="categories[]" class="form-control select2" multiple style="width: 100%">
                    <?php foreach ($categories as $cid => $cname) { ?>
                    <option value="<?= $cid ?>" <?= isset($row['categories']) && in_array($cid, $row['categories']) ? 'selected' : '' ?>><?= $cname ?></option>
                    <?php } ?>
                </select>



            </div>

            <div class="form-group">
                <label for="tags">Tags</label>
                <select id="tags" name="tags[]" class="form-control select2-tags" multiple style="width: 100%">
                    <?php if(isset($row['tags'])) { ?>
                        <?php foreach (explode(',', $row['tags']) as $tag) { ?>
                        <option value="<?= $tag ?>" selected><?= $tag ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>



            <div class="form-group">
                <label for="short_description">Short Description</label>
                <textarea id="short_description" name="short_description" class="form-control editor" ><?= isset($row['short_description']) ? $row['short_description'] : "" ?></textarea>
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea id="description" name="description" class="form-control editor"><?= isset($row['description']) ? $row['description'] : "" ?></textarea>
            </div>
        </div>

        <div class="tab-pane" id="tab_2">
            <div class="row">
                <div class="form-group col-sm-4">
                    <label for="image" class="required">Featured Image</label>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <a href="<?= file_manager('image') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                        <input type="text" id="image" name="image" value="<?= isset($row['image']) ? $row['image'] : "" ?>" class="form-control" required readonly />
                        <span class="input-group-btn">
                            <a href="javascript:void(0)" class="btn btn-danger clear-img"><i class="fa fa-times"></i></a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 5px; margin-top: 10px;">
                        <img class="img-responsive" id="image-preview" style="max-height: 100px;" src="<?= isset($row['image']) ? uploaded_url($row['image']) : '' ?>" />
                    </div>
                </div>
            </div>

            <h4>Other Images</h4>

            <div id="other_images" class="row">
                <?php if(isset($row['images'])) { ?>
                    <?php foreach ($row['images'] as $k => $img) { ?>
                    <div class="form-group col-sm-4 img-form-group">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a href="<?= file_manager('oimage' . $k) ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                            </span>
                            <input type="text" id="oimage<?= $k ?>" name="images[<?= $k ?>][image]" value="<?= $img['image'] ?>" class="form-control" readonly />
                            <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-danger remove-img"><i class="fa fa-timex"></i></a>
                            </span>
                        </div>
                        <div style="background: #ccc; display: inline-block; padding: 5px; margin-top: 10px;">
                            <img class="img-responsive" id="oimage<?= $k ?>-preview" style="max-height: 100px;" src="<?= $img['image'] ? uploaded_url($img['image']) : '' ?>" />
                        </div>
                        <input type="hidden" name="images[<?= $k ?>][id]" value="<?= $img['id'] ?>" />
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>

            <a href="javascript:void(0)" class="btn btn-primary another-img">+ Add Another Image</a>
        </div>

        <div class="tab-pane" id="tab_3">
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="price">Regular Price</label>
                    <input type="text" id="price" name="price" class="form-control money" value="<?= isset($row['price']) && $row['price'] > 0 ? $row['price'] : "" ?>" />
                </div>

                <div class="form-group col-sm-6">
                    <label for="stock">Available Stock</label>
                    <input type="text" id="stock" name="stock" class="form-control numberonly" value="<?= isset($row['stock']) ? $row['stock'] : "" ?>" />
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-4">
                    <label for="sale_price">Sale Price</label>
                    <input type="text" id="sale_price" name="sale_price" class="form-control money" value="<?= isset($row['sale_price']) && $row['sale_price'] ? $row['sale_price'] : "" ?>" />
                </div>

                <div class="form-group col-sm-4">
                    <label for="sale_start">Sale Start Date</label>
                    <input type="text" id="sale_start" name="sale_start" class="form-control" value="<?= isset($row['sale_start']) && $row['sale_start'] != '0000-00-00 00:00:00' ? $row['sale_start'] : "" ?>" readonly />
                </div>

                <div class="form-group col-sm-4">
                    <label for="sale_end">Sale End Date</label>
                    <input type="text" id="sale_end" name="sale_end" class="form-control" value="<?= isset($row['sale_end']) && $row['sale_end'] != '0000-00-00 00:00:00' ? $row['sale_end'] : "" ?>" readonly />
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab_4">
            <div class="well">
                <strong>Generate Variations</strong>
                <hr />
                <div class="row">
                    <div class="col-sm-10">
                        <div class="row">
                            <?php foreach ($attributes as $k => $v) { ?>
                            <div class="col-sm-3">
                                <label><input type="checkbox" class="variation_types" value="<?= isset($k)?$k:'' ?>"> &nbsp; <?= isset($v)?$v:'' ?></label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-2 text-right">
                        <button type="button" class="btn btn-primary generate-variations">
                            <i class="fa fa-cog"></i> Generate
                        </button>
                    </div>
                </div>
            </div>

            <h3>Available Variations</h3>

            <div id="variations_list">
                <?php if(isset($row['variations'])) { ?>
                    <table class="table table-bordered table-striped">
                        <?php $cols = explode(',', isset($row['variations'][0]['attributes'])?$row['variations'][0]['attributes']:''); ?>
                        <thead>
                            <tr>
                                <?php
                                if(count($cols) > 0 ){
                                foreach ($cols as $c) {
                                    $lbl = '';
                                    foreach ($attributesValues as $k => $v) {
                                        if(array_key_exists($c, $v)) {
                                            $lbl = $attributes[$k];
                                        }
                                    }
                                }
                                ?>
                                <th><?= $lbl ?></th>

                                <th width="10%">Stock</th>
                                <th width="15%">Price</th>
                                <th width="15%">Sale Price</th>
                                <th width="15%">Start Date</th>
                                <th width="15%">End Date</th>
                                <th width="5%">&nbsp;</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($row['variations'] as $k => $v) { ?>
                            <tr>
                                <?php $cols = explode(',', $v['attributes']); ?>
                                <?php foreach ($cols as $c) { ?>
                                <td><?= $attributesLabels[$c] ?></td>
                                <?php } ?>

                                <td><input type="text" name="variations[<?= $k ?>][stock]" class="form-control numberonly" value="<?= $v['stock'] ?>" /></td>
                                <td><input type="text" name="variations[<?= $k ?>][price]" class="form-control money" value="<?= $v['price'] > 0 ? $v['price'] : '' ?>" /></td>
                                <td><input type="text" name="variations[<?= $k ?>][sale_price]" class="form-control money" value="<?= $v['sale_price'] > 0 ? $v['sale_price'] : '' ?>" /></td>
                                <td><input type="text" name="variations[<?= $k ?>][sale_start]" class="form-control sale_start" value="<?= $v['sale_start'] != '0000-00-00 00:00:00' ? $v['sale_start'] : '' ?>" readonly /></td>
                                <td><input type="text" name="variations[<?= $k ?>][sale_end]" class="form-control sale_end" value="<?= $v['sale_end'] != '0000-00-00 00:00:00' ? $v['sale_end'] : '' ?>" readonly /></td>

                                <td class="text-center">
                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm remove-variation"><i class="fa fa-times"></i></a>
                                    <input type="hidden" name="variations[<?= $k ?>][attributes]" value="<?= $v['attributes'] ?>" />
                                    <input type="hidden" name="variations[<?= $k ?>][id]" value="<?= isset($v['id']) ? $v['id'] : '' ?>" />
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>

                <?php } else { ?>
                <p>No variations found...</p>
                <?php } ?>
            </div>
        </div>

        <div class="tab-pane" id="tab_5">
            <div class="row">
                <div class="form-group col-sm-12">
                    <label for="meta_title" >Meta Title</label>
                    <input type="text" id="meta_title" name="meta_title" class="form-control" value="<?= isset($row['meta_title']) ? $row['meta_title'] : "" ?>"  />
                </div>

                <div class="form-group col-sm-12">
                    <label for="meta_desc" >Meta Desc</label>
                    <textarea id="meta_desc" name="meta_desc" class="form-control"><?= isset($row['meta_desc']) ? $row['meta_desc'] : "" ?></textarea>
                </div>

                <div class="form-group col-sm-12">
                    <label for="index_follow">Robots Tag</label>
                    <select id="index_follow" name="index_follow" class="form-control">
                        <option value="">Index, Follow</option>
                        <option value="index,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'index,nofollow' ? 'selected' : '' ?>>Index, NoFollow</option>
                        <option value="noindex,follow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,follow' ? 'selected' : '' ?>>NoIndex, Follow</option>
                        <option value="noindex,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,nofollow' ? 'selected' : '' ?>>NoIndex, NoFollow</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab_6">
            <div class="form-group">
                <label for="upsell">Upsell</label>
                <select id="upsell" name="upsell[]" class="form-control select2" multiple style="width: 100%">
                    <?php foreach ($all_products as $ap) { ?>
                    <option value="<?= $ap['id'] ?>" <?= isset($row['upsell']) && in_array($ap['id'], $row['upsell']) ? 'selected' : '' ?>><?= $ap['name'] ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="crossell">Cross-sells</label>
                <select id="crossell" name="crossell[]" class="form-control select2" multiple style="width: 100%">
                    <?php foreach ($all_products as $ap) { ?>
                    <option value="<?= $ap['id'] ?>" <?= isset($row['crossell']) && in_array($ap['id'], $row['crossell']) ? 'selected' : '' ?>><?= $ap['name'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="box box-solid">
    <div class="box-body">
        <div class="form-group" style="margin-bottom: 0">
            <button type="submit" name="submit" class="btn btn-primary">Save</button>
            <a href="<?= admin_url('products') ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
</div>

<?= form_close() ?>

<script src="<?= assets('js/jquery.combinations.min.js') ?>"></script>
<script type="text/javascript">
var imgcnt = <?= isset($row['images']) ? count($row['images']) : 0 ?>;
var attributes = <?= json_encode($attributes) ?>;
var attributesVals = <?= json_encode($attributesValues) ?>;
var attributesLbls = <?= json_encode($attributesLabels) ?>;
$(function(){
    if($("#slug").val() == '') {
        $("#name").on('input', function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }

    $('.another-img').click(function(){
        var img  = '<div class="form-group col-sm-4 img-form-group">';
            img += '  <div class="input-group">';
            img += '    <span class="input-group-btn">';
            img += '      <a href="<?= file_manager('oimage') ?>' + imgcnt + '" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>';
            img += '    </span>';
            img += '    <input type="text" id="oimage' + imgcnt + '" name="images[' + imgcnt + '][image]" class="form-control image" readonly />';
            img += '    <span class="input-group-btn">';
            img += '      <a href="javascript:void(0)" class="btn btn-danger remove-img"><i class="fa fa-times"></i></a>';
            img += '    </span>';
            img += '  </div>';
            img += '  <div style="background: #ccc; display: inline-block; padding: 5px; margin-top: 10px;">';
            img += '    <img class="img-responsive" id="oimage' + imgcnt + '-preview" style="max-height: 100px;" src="" />';
            img += '  </div>';
            img += '  <input type="hidden" name="images[' + imgcnt + '][id]" value="" />';
            img += '</div>';

        $('#other_images').append(img);
        imgcnt++;

        $('#other_images .upload-btn').fancybox({
            width: 900,
            minHeight: 600,
            type: 'iframe',
            autoScale: true
        });
    });

    $('.clear-img').click(function(){
        if($('#image').val() != '' ) {
            if(confirm('Are you sure you want to remove this image?')) {
                $('#image').val('');
                $('#image-preview').attr('src', '');
            }
        }
    });

    $(document).on('click', '.remove-img', function() {
        if(confirm('Are you sure you want to remove this image?')) {
            $(this).parents('.img-form-group').remove();
        }
    });

    $('.generate-variations').click(function(){
        var t = $('.variation_types:checked');

        if(t.length > 2) {
            alert('Maximum 2 Variations can be selected at a time!');
        } else if(t.length) {
            var valid = false;

            if($('#variations_list > table').length) {
                if(confirm('Existing variation will be discarded, Are you sure you want to regenerate variations?')) {
                    valid = true;
                }
            } else {
                valid = true;
            }

            if(valid) {
                var cnt = 0;

                var tbl  = '<table class="table table-bordered table-striped">';
                    tbl += '  <thead>';
                    tbl += '    <tr>';

                var arr = [];
                t.each(function(){
                    tbl += '<th>' + attributes[$(this).val()] + '</th>';
                    arr.push($.map(attributesVals[$(this).val()], function(e, i) { return i }));
                });
                var cmb = $.combinations(arr);

                tbl += '      <th width="10%">Stock</th>';
                tbl += '      <th width="15%">Price</th>';
                tbl += '      <th width="15%">Sale Price</th>';
                tbl += '      <th width="15%">Start Date</th>';
                tbl += '      <th width="15%">End Date</th>';
                tbl += '      <th width="5%">&nbsp;</th>';
                tbl += '    </tr>';
                tbl += '  </thead>';
                tbl += '  <tbody>';

                $.each(cmb, function(i1, v1) {
                    tbl += '    <tr>';

                    $.each(v1, function(i2, v2) {
                        tbl += '<td>' + attributesLbls[v2] + '</td>';
                    });

                    tbl += '      <td><input type="text" name="variations[' + cnt + '][stock]" class="form-control numberonly" /></td>';
                    tbl += '      <td><input type="text" name="variations[' + cnt + '][price]" class="form-control money" /></td>';
                    tbl += '      <td><input type="text" name="variations[' + cnt + '][sale_price]" class="form-control money" /></td>';
                    tbl += '      <td><input type="text" name="variations[' + cnt + '][sale_start]" class="form-control sale_start" /></td>';
                    tbl += '      <td><input type="text" name="variations[' + cnt + '][sale_end]" class="form-control sale_end" /></td>';

                    tbl += '      <td class="text-center">';
                    tbl += '        <a href="javascript:void(0)" class="btn btn-danger btn-sm remove-variation"><i class="fa fa-times"></i></a>';
                    tbl += '        <input type="hidden" name="variations[' + cnt + '][attributes]" value="' + v1.join(',') + '" />';
                    tbl += '      </td>';
                    tbl += '    </tr>';

                    cnt++;
                });

                tbl += '  </tbody>';
                tbl += '</table>';

                $('#variations_list').html(tbl);
                init_sale_datepicker();
            }
        } else {
            alert('No variation selected!');
        }
    });

    $(document).on('click', '.remove-variation', function(){
        if(confirm('Are you sure you want to delete this?')) {
            $(this).parents('tr').remove();
        }
    });

    $("#sale_start").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        minDate: new Date(),
        maxDate: '+1y',
        onSelect: function(date){
            var selectedDate = new Date(date);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime() + msecsInADay);

            $("#sale_end").datepicker("option", "minDate", endDate);
            $("#sale_end").datepicker("option", "maxDate", '+1y');
        }
    });

    $("#sale_end").datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true
    });

    init_sale_datepicker();
});

function init_sale_datepicker() {
    $('#variations_list table tbody tr').each(function(){
        var sale_start = $(this).find('.sale_start');
        var sale_end   = $(this).find('.sale_end');

        sale_start.datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            minDate: new Date(),
            maxDate: '+1y',
            onSelect: function(date){
                var selectedDate = new Date(date);
                var msecsInADay = 86400000;
                var endDate = new Date(selectedDate.getTime() + msecsInADay);

                sale_end.datepicker("option", "minDate", endDate);
                sale_end.datepicker("option", "maxDate", '+1y');
            }
        });

        sale_end.datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true
        });
    });
}
</script>
