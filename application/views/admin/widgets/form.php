<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-8">
                    <label for="name" class="required">Name</label>
                    <input type="text" id="name" name="name" class="form-control" value="<?= isset($row['name']) ? $row['name'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-4">
                    <label for="slug" class="required">Slug</label>
                    <input type="text" id="slug" name="slug" class="form-control" value="<?= isset($row['slug']) ? $row['slug'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-4 hidden">
                    <label for="image" >Image</label>
                    <div class="input-group">
                        <input type="text" id="image" name="image" value="<?= isset($row['image']) ? $row['image'] : "" ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('image') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 10px; margin-top: 10px;">
                        <img class="img-responsive" id="image-preview" style="max-height: 100px;" src="<?= isset($row['image']) ? uploaded_url($row['image']) : '' ?>" />
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <label for="content" >Content</label>
                    <textarea id="content" name="content" class="form-control editor" ><?= isset($row['content']) ? $row['content'] : "" ?></textarea>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('widgets') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
$(function(){
    if($("#slug").val() == '') {
        $("#name").on('input', function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }
})
</script>