<h4 class="text-center"><strong>Reset Password</strong></h4>
<p>&nbsp;</p>
<p class="login-box-msg">Enter your new password</p>

<?= form_open() ?>
    <input type="hidden" name="email" value="<?= $email ?>" />
    <input type="hidden" name="code" value="<?= $code ?>" />

    <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="New Password" required /> 
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
        <input type="password" name="repassword" class="form-control" placeholder="Confirm Password" required /> 
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <a href="<?= url('office') ?>">Back To Login</a>
        </div>
        <div class="col-xs-6">
        	<button type="submit" name="reset" value="1" class="btn btn-primary btn-block btn-flat">Update Password</button> 
        </div>
    </div>
<?= form_close() ?>