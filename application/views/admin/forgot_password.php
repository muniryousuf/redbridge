<h4 class="text-center"><strong>Forgot Password</strong></h4>
<p>&nbsp;</p>
<p class="login-box-msg">Enter your email address to reset your password</p>

<?= form_open() ?>
    <div class="form-group has-feedback"> 
    	<input type="email" name="email" class="form-control" placeholder="Email Address" required /> 
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <a href="<?= url('office') ?>">Back To Login</a>
        </div>
        <div class="col-xs-6">
        	<button type="submit" name="forgot" value="1" class="btn btn-primary btn-block btn-flat">Reset Password</button> 
        </div>
    </div>
<?= form_close() ?>