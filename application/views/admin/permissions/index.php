<div class="box box-primary">
    <div class="box-body">
        <?= form_open(admin_url('permissions/store')) ?>
            <table id="permissions" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th rowspan="2" class="text-center">Actions</th>
                        <th colspan="<?= count($roles) ?>" class="text-center">Roles</th>
                    </tr>
                    <tr>
                        <?php foreach($roles as $r): ?>
                        <th class="text-center"><?= $r ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($controllers as $c => $actions): ?>
                    <tr>
                        <td class="h" colspan="<?= count($roles) + 1 ?>">
                            <a href="javascript:void(0)" class="showhide" data-id="<?= $c ?>"><i class="fa fa-plus"></i></a>
                            &nbsp;&nbsp;
                            <?= isset($cLabels[$c]) ? $cLabels[$c] : $c ?>
                        </td>
                    </tr>
                    <?php foreach($actions as $a): ?>
                    <tr class="row-<?= $c ?>" style="display: none;">
                        <td><?= isset($aLabels[$a]) ? $aLabels[$a] : $a ?></td>
                        <?php foreach($roles as $k => $v): ?>
                            <?php
                            $key = $k .'.'. strtolower($c) .'.'. strtolower($a);
                            $checked = in_array($key, $permissions) ? 'checked' : '';
                            ?>
                            <td class="text-center"><input type="checkbox" name="permissions[<?= $k ?>][<?= $c ?>][<?= $a ?>]" value="1" <?= $checked ?> /></td>
                        <?php endforeach; ?>
                    </tr>
                    <?php endforeach; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div class="form-group">
                <button type="submit" name="save" class="btn btn-primary">Save</button>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="application/javascript">
$(function(){
    $('.showhide').click(function(){
        if($(this).find('i').hasClass('fa-plus')) {
            $(this).find('i').removeClass('fa-plus').addClass('fa-minus');
            $('.row-' + $(this).attr('data-id')).show();
        } else {
            $(this).find('i').removeClass('fa-minus').addClass('fa-plus');
            $('.row-' + $(this).attr('data-id')).hide();
        }
    });
})
</script>