<?= form_open(admin_url('sitesettings/store')) ?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
        <li><a href="#tab_2" data-toggle="tab">Contact Info</a></li>
        <li><a href="#tab_3" data-toggle="tab">Logo & Favicon</a></li>
        <li><a href="#tab_4" data-toggle="tab">Social Media</a></li>
        <li><a href="#tab_5" data-toggle="tab">Custom Code</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="row">
                <!-- Site Name Field -->
                <div class="form-group col-sm-6">
                    <label>Site Name</label>
                    <input type="text" name="site_name" value="<?= $settings ? $settings['site_name'] : '' ?>" class="form-control" />
                </div>

                <!-- Site Title Field -->
                <div class="form-group col-sm-6">
                    <label>Site Title</label>
                    <input type="text" name="site_title" value="<?= $settings ? $settings['site_title'] : '' ?>" class="form-control" />
                </div>

                <!-- Tag Line Field -->
                <div class="form-group col-sm-6">
                    <label>Tag Line</label>
                    <input type="text" name="tag_line" value="<?= $settings ? $settings['tag_line'] : '' ?>" class="form-control" />
                </div>

                <!-- Frontpage Field -->
                <div class="form-group col-sm-6">
                    <label>Frontpage</label>
                    <select name="frontpage" class="form-control">
                        <option value=""></option>
                        <?php foreach($pages as $p) { ?>
                        <option value="<?= $p['id'] ?>" <?= $settings && $settings['frontpage'] == $p['id'] ? 'selected' : '' ?>><?= $p['title'] ?></option>
                        <?php } ?>
                    </select>
                </div>

                <!-- Copyrights Field -->
                <div class="form-group col-sm-6">
                    <label>Copyrights Text:</label>
                    <input type="text" name="copyrights" value="<?= $settings ? $settings['copyrights'] : '' ?>" class="form-control" />
                </div>

                <!-- Allow Index Field -->
                <div class="form-group col-sm-6">
                    <label>Allow Search Engine Indexing:</label>
                    <select name="allow_index" class="form-control">
                        <option value="1" <?= $settings && $settings['allow_index'] == 1 ? 'selected' : '' ?>>Yes</option>
                        <option value="0" <?= $settings && $settings['allow_index'] == 0 ? 'selected' : '' ?>>No</option>
                    </select>
                </div>

                <div class="form-group col-sm-12">
                    <label>Cookie Popup Text</label>
                    <textarea name="vars[cookie_text]" class="form-control editor"><?= $vars_val['cookie_text'] ?></textarea>
                </div>

                <div class="form-group col-sm-12">
                    <label>Sitemap URL:</label>
                    <input type="text" value="<?= url('/sitemap.xml') ?>" class="form-control" readonly />
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab_2">
            <div class="row">
                <!-- Address Field -->
                <div class="form-group col-sm-12">
                    <label>Address</label>
                    <textarea name="address" class="form-control"><?= $settings ? $settings['address'] : '' ?></textarea>
                </div>

                <!-- Copyrights Field -->
                <div class="form-group col-sm-6">
                    <label>Phone:</label>
                    <input type="text" name="phone" value="<?= $settings ? $settings['phone'] : '' ?>" class="form-control" />
                </div>

                <!-- Email Field -->
                <div class="form-group col-sm-6">
                    <label>Email:</label>
                    <input type="email" name="email" value="<?= $settings ? $settings['email'] : '' ?>" class="form-control" />
                </div>                        
            </div>
        </div>

        <div class="tab-pane" id="tab_3">
            <div class="row">
                <!-- Header Logo Field -->
                <div class="form-group col-sm-4">
                    <label>Header Logo:</label>
                    <div class="input-group">
                        <input type="text" id="header_logo" name="header_logo" value="<?= $settings ? $settings['header_logo'] : '' ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('header_logo') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 10px; margin-top: 10px;">
                        <img class="img-responsive" id="header_logo-preview" style="max-height: 100px;" src="<?= $settings ? uploaded_url($settings['header_logo']) : '' ?>" />
                    </div>
                </div>

                <!-- Footer Field -->
                <div class="form-group col-sm-4">
                    <label>Footer Logo:</label>
                    <div class="input-group">
                        <input type="text" id="footer_logo" name="footer_logo" value="<?= $settings ? $settings['footer_logo'] : '' ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('footer_logo') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 10px; margin-top: 10px;">
                        <img class="img-responsive" id="footer_logo-preview" style="max-height: 100px;" src="<?= $settings ? uploaded_url($settings['footer_logo']) : '' ?>" />
                    </div>
                </div>

                <!-- Favicon Field -->
                <div class="form-group col-sm-4">
                    <label>Favicon:</label>
                    <div class="input-group">
                        <input type="text" id="favicon" name="favicon" value="<?= $settings ? $settings['favicon'] : '' ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('favicon') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 10px; margin-top: 10px;">
                        <img class="img-responsive" id="favicon-preview" style="max-height: 100px;" src="<?= $settings ? uploaded_url($settings['favicon']) : '' ?>" />
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="tab_4">
            <div class="row">
                <?php foreach($social as $s): ?>
                <div class="form-group col-sm-6">
                    <label><?= ucwords($s) ?>:</label>
                    <input type="text" name="social_links[<?= $s ?>]" value="<?= isset($settings['social_links'][$s]) ? $settings['social_links'][$s] : '' ?>" class="form-control" />
                </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="tab-pane" id="tab_5">
            <div class="form-group">
                <label>Before HEAD close:</label>
                <textarea name="before_head" class="form-control" rows="10"><?= $settings ? $settings['before_head'] : '' ?></textarea>
            </div>

            <div class="form-group">
                <label>After BODY Start:</label>
                <textarea name="after_body" class="form-control" rows="10"><?= $settings ? $settings['after_body'] : '' ?></textarea>
            </div>

            <div class="form-group">
                <label>Before BODY close:</label>
                <textarea name="before_body" class="form-control" rows="10"><?= $settings ? $settings['before_body'] : '' ?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
</div>

<?= form_close() ?>
