<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="key" class="required">Key</label>
                    <input type="text" id="key" name="key" class="form-control" value="<?= isset($row['key']) ? $row['key'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="value">Value </label>
                    <input type="text" id="value" name="value" class="form-control" value="<?= isset($row['value']) ? $row['value'] : "" ?>" />
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('sitevariables') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>