<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="title" class="required">Title</label>
                    <input type="text" id="title" name="title" class="form-control" value="<?= isset($row['title']) ? $row['title'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="slug" class="required">Slug</label>
                    <input type="text" id="slug" name="slug" class="form-control" value="<?= isset($row['slug']) ? $row['slug'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-12">
                    <label for="categories">Categories</label>
                    <select id="categories" name="categories[]" class="form-control select2" multiple style="width: 100%">
                        <?php foreach ($categories as $cat) { ?>
                        <option value="<?= $cat['id'] ?>" <?= isset($row['categories']) && in_array($cat['id'], $row['categories']) ? 'selected' : '' ?>><?= $cat['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group col-sm-4">
                    <label for="image">Featured Image</label>
                    <div class="input-group">
                        <input type="text" id="image" name="image" value="<?= isset($row['image']) ? $row['image'] : "" ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('image') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                            <a href="javascript:void(0)" class="btn btn-danger clear-img"><i class="fa fa-times"></i></a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 10px; margin-top: 10px;">
                        <img class="img-responsive" id="image-preview" style="max-height: 100px;" src="<?= isset($row['image']) ? uploaded_url($row['image']) : '' ?>" />
                    </div>
                </div>

                <div class="form-group col-sm-4">
                    <label for="status" class="required">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option value="1" <?= isset($row['status']) && $row['status'] == 1 ? 'selected' : '' ?>>Published</option>
                        <option value="2" <?= isset($row['status']) && $row['status'] == 2 ? 'selected' : '' ?>>Scheduled</option>
                        <option value="0" <?= isset($row['status']) && $row['status'] == 0 ? 'selected' : '' ?>>Draft</option>
                    </select>
                </div>

                <div id="publish_date_div" class="form-group col-sm-4">
                    <label for="publish_date" class="required">Publish Date/Time</label>
                    <input type="text" id="publish_date" name="publish_date" class="form-control" value="<?= isset($row['publish_date']) && $row['status'] == 2 ? date('d-M-Y H:i a', strtotime($row['publish_date'])) : "" ?>" />
                    <small>Current Server Time: <?= date('d-M-Y h:i a') ?></small>
                </div>

                <div class="form-group col-sm-12">
                    <label for="content" >Content</label>
                    <textarea id="content" name="content" class="form-control editor" ><?= isset($row['content']) ? $row['content'] : "" ?></textarea>
                </div>

                <div class="col-sm-12">
                    <div class="well">
                        <h3 class="meta-heading">META DATA:</h3>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="meta_title" >Meta Title</label>
                                <input type="text" id="meta_title" name="meta_title" class="form-control" value="<?= isset($row['meta_title']) ? $row['meta_title'] : "" ?>"  />
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="meta_desc" >Meta Desc</label>
                                <textarea id="meta_desc" name="meta_desc" class="form-control"><?= isset($row['meta_desc']) ? $row['meta_desc'] : "" ?></textarea>
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="index_follow">Robots Tag</label>
                                <select id="index_follow" name="index_follow" class="form-control">
                                    <option value="">Index, Follow</option>
                                    <option value="index,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'index,nofollow' ? 'selected' : '' ?>>Index, NoFollow</option>
                                    <option value="noindex,follow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,follow' ? 'selected' : '' ?>>NoIndex, Follow</option>
                                    <option value="noindex,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,nofollow' ? 'selected' : '' ?>>NoIndex, NoFollow</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('blogs') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<link rel="stylesheet" href="<?= assets('css/bootstrap-datetimepicker.css') ?>">
<script src="<?= assets('js/moment.min.js') ?>"></script>
<script src="<?= assets('js/bootstrap-datetimepicker.js') ?>"></script>
<script type="application/javascript">
$(function(){
    if($("#slug").val() == '') {
        $("#title").on('input',function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }

    $('#status').change(function(){
        if($(this).val() == '2') {
            $('#publish_date_div').show();
        } else {
            $('#publish_date_div').hide();
            $('#publish_date').val('');            
        }
    });

    $('#status').trigger('change');

    $('#publish_date').datetimepicker({
        format: 'DD-MMM-YYYY h:mm a'
    });

    $('.clear-img').click(function(){
        $('#image').val('');
        $('#image-preview').attr('src', '');
    })
})
</script>