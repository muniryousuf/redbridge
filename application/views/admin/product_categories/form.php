<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="name" class="required">Name</label>
                    <input type="text" id="name" name="name" class="form-control" value="<?= isset($row['name']) ? $row['name'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="slug" class="required">Slug</label>
                    <input type="text"  readonly id="slug" name="slug" class="form-control" value="<?= isset($row['slug']) ? $row['slug'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="sort" class="required">Sort</label>
                    <input type="number" id="sort" name="sort" class="form-control" value="<?= isset($row['sort']) ? $row['sort'] : 0 ?>"  />
                </div>

                <div class="form-group col-sm-6">
                    <label for="image" class="required">Featured Image</label>
                    <div class="input-group">
                        <input type="text" id="image" name="image" value="<?= isset($row['image']) ? $row['image'] : "" ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('image') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 5px; margin-top: 10px;">
                        <img class="img-responsive" id="image-preview" style="max-height: 100px;" src="<?= isset($row['image']) ? uploaded_url($row['image']) : '' ?>" />
                    </div>
                </div>

                <div class="form-group col-sm-6">
                    <label for="parent_id">Parent Category</label>
                    <select id="parent_id" name="parent_id" class="form-control"  ">
                        <option value=""></option>
                        <?php foreach ($categories as $cid => $cname) { ?>
                        <option value="<?= $cid ?>" <?= isset($row['parent_id']) && $row['parent_id'] == $cid ? 'selected' : '' ?>><?= $cname ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group col-sm-12">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control ckeditor"><?= isset($row['description']) ? $row['description'] : "" ?></textarea>
                </div>

                <div class="col-sm-12">
                    <div class="well">
                        <h3 class="meta-heading">META DATA:</h3>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="meta_title" >Meta Title</label>
                                <input type="text" id="meta_title" name="meta_title" class="form-control" value="<?= isset($row['meta_title']) ? $row['meta_title'] : "" ?>"  />
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="meta_desc" >Meta Desc</label>
                                <textarea id="meta_desc" name="meta_desc" class="form-control"><?= isset($row['meta_desc']) ? $row['meta_desc'] : "" ?></textarea>
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="index_follow">Robots Tag</label>
                                <select id="index_follow" name="index_follow" class="form-control">
                                    <option value="">Index, Follow</option>
                                    <option value="index,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'index,nofollow' ? 'selected' : '' ?>>Index, NoFollow</option>
                                    <option value="noindex,follow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,follow' ? 'selected' : '' ?>>NoIndex, Follow</option>
                                    <option value="noindex,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,nofollow' ? 'selected' : '' ?>>NoIndex, NoFollow</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    <a href="<?= admin_url('productcategories') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
$(function(){
    if($("#slug").val() == '') {
        $("#name").on('input', function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }
})
</script>
