<div class="row">
	<div id="iframe-div" class="col-sm-12">
		<iframe  width="100%" height="600" frameborder="0" src="<?= assets('js/filemanager/dialog.php?akey=d357621c8fdb0300fe9db6d81a063a73&type=1&relative_url=1&field_id=media') ?>" style="border: 1px solid #aaa;"></iframe>
	</div>

	<div id="media-details" class="col-sm-3" style="display: none;">
		<div  class="box box-primary">
		    <div class="box-body">
		    	<div class="form-group text-center">
	    			<div style="background: #ccc; display: inline-block; padding: 2px;">
	                    <img class="img-responsive" id="media-preview" style="max-height: 150px;" />
	                    <input type="hidden" id="media" name="media" value="" />
	                </div>
	            </div>
				<div class="form-group">
					<label>Image URL:</label>
					<input type="text" id="image-url" class="form-control" readonly />
				</div>
				<div class="form-group">
					<label>Shortcode:</label>
					<input type="text" id="image-shortcode" class="form-control" readonly>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var uploadUrl = '<?= assets('uploads/') ?>';
</script>