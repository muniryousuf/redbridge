<?php 
$products_list = array();
foreach (($all_products?:array()) as $key => $value) 
{
     $products_list[$value['id']]   = $value['name'];
}

?>
<div class="box box-primary">
    <div class="box-body">
        <?= form_open('', ['id' => 'assignProductFrm']) ?>
            <div class="form-group row">
                <label class="col-md-2 col-form-label">Products</label>
                <div class="col-md-10">
                  <?php echo form_dropdown("AssignedProducts[]",$products_list,$selected,'class="multiSelect" multiple required') ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('discounts') ?>" class="btn btn-default">Cancel</a>
                </div>    
            </div>
            
        <?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
$(function(){
    $(document).ready(function(){
        $('.multiSelect').multiSelect();

    })
})
</script>