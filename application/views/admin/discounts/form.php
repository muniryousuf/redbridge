<div class="box box-primary">
    <div class="box-body">
        <?= form_open('', ['id' => 'discountFrm']) ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="name" class="required">Discount Title</label>
                    <input type="text" id="name" name="name" class="form-control" value="<?= isset($row['name']) ? $row['name'] : "" ?>" required />
                </div>
                <div class="form-group col-sm-6">
                    <label for="message" class="required">Discount Message Template</label>
                    <input type="text" id="message" name="message" class="form-control" value="<?= isset($row['message']) ? $row['message'] : "" ?>" placeholder="Buy {quantity} {product name} for Rs {amount}" required />
                </div>
                <div class="col-sm-8">
                    <table id="values-tbl" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Quantity <span style="color: red">*</span></th>
                                <th>Amount <span style="color: red">*</span></th>
                                <th>Default</th>
                                <th width="5%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($row['values'])) { ?>
                                <?php foreach ($row['values'] as $k => $v) { ?>
                                <tr>
                                    <td><input type="text" name="values[<?= $k ?>][quantity]" value="<?= $v['quantity'] ?>" class="form-control text-right" required /></td>
                                    <td><input type="text" name="values[<?= $k ?>][amount]"  value="<?= $v['amount'] ?>"  class="form-control text-right" required /></td>
                                    <td><input type="checkbox" class="defaultCheckbox" name="values[<?= $k ?>][is_default]" value="1" <?= ($v['is_default'] == 1)?"checked":"" ?>></td>
                                    <td align="right">
                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger remove-value">x</a>
                                        <input type="hidden" name="values[<?= $k ?>][id]" value="<?= $v['id'] ?>" />
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php }
                            else
                                { ?>
                                <tr>
                                    <td><input type="text" name="values[0][quantity]"  class="form-control text-right" required /></td>
                                    <td><input type="text" name="values[0][amount]"  class="form-control text-right" required /></td>
                                    <td><input type="checkbox" class="defaultCheckbox" name="values[0][is_default]" value="1"></td>
                                    <td align="right">
                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger remove-value">x</a>
                                        <input type="hidden" name="values[0][id]" value="" />
                                    </td>
                                </tr>
                                <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"><a href="javascript:void(0)" class="btn btn-primary add-value">+ Add New Discount</a></td>
                            </tr>
                        </tfoot>                
                    </table>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('discounts') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
var vcnt = <?= isset($row['values']) ? count($row['values']) : 1; ?>;

$(function(){
    $('#discountFrm').validate();

    $('.add-value').click(function(){
        var tr  = '<tr>';
            tr += '  <td><input type="text" name="values[' + vcnt + '][quantity]" value="" class="form-control text-right" required /></td>';
            tr += '  <td><input type="text" name="values[' + vcnt + '][amount]" value="" class="form-control text-right" required /></td>';
            tr += '  <td><input type="checkbox" class="defaultCheckbox" name="values['+vcnt+'][is_default]" value="1"></td>';
            tr += '  <td align="right">';
            tr += '    <a href="javascript:void(0)" class="btn btn-sm btn-danger remove-value">x</a>';
            tr += '    <input type="hidden" name="values[' + vcnt + '][id]" value="" />';
            tr += '  </td>';
            tr += '</tr>';


        $('#values-tbl tbody').append(tr);
        vcnt++;
    });

    $(document).on('click', '.remove-value', function(){
        if(confirm('Are you sure you want to delete this?')) {
            $(this).parents('tr').remove();
        }
    });

    $(document).on("click",".defaultCheckbox",function(){
        if ($(this).is(":checked")) 
        {
            $(".defaultCheckbox").not(this).prop("checked",false);
        }
    });
})
</script>