<style type="text/css">
.dt-body-nowrap
{
    white-space: nowrap;
}    
</style>
<div class="box box-primary">
    <div class="box-body">
        <p class="text-right">
            <a class="btn btn-primary" href="<?= admin_url('discounts/create') ?>">Add New</a>
        </p>
        
        <hr />

        <table id="discounts-table" class="table table-responsive">
            <thead>
                <tr>
                    <th class="text-center">ID</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script type="application/javascript">
$(function () {
    $('#discounts-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url": "<?= admin_url('discounts/datatable') ?>",
            "dataType": "json",
            "type": "POST",
        },
        columnDefs: [
            {targets: [0], className: "text-center"},
            {targets: [4], orderable: false, searchable: false, className: "dt-body-nowrap"}
        ]
    });
})
</script>