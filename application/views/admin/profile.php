<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="first_name" class="required">First Name</label>
                    <input type="text" id="first_name" name="first_name" class="form-control" value="<?= $user['first_name'] ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="last_name" >Last Name</label>
                    <input type="text" id="last_name" name="last_name" class="form-control" value="<?= $user['last_name'] ?>"  />
                </div>

                <div class="form-group col-sm-6">
                    <label for="email" class="required">Email</label>
                    <input type="text" id="email" name="email" class="form-control" value="<?= $user['email'] ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="profile_photo" >Profile Photo</label>
                    <div class="input-group">
                        <input type="text" id="profile_photo" name="profile_photo" value="<?= $user['profile_photo'] ?>" class="form-control" readonly />
                        <span class="input-group-btn">
                            <a href="<?= file_manager('profile_photo') ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                    </div>
                    <div style="background: #ccc; display: inline-block; padding: 10px; margin-top: 10px;">
                        <img class="img-responsive" id="profile_photo-preview" style="max-height: 100px; margin-top: 10px;" src="<?= uploaded_url($user['profile_photo']) ?>" />
                    </div>
                </div>

                <div class="col-sm-12">
                    <hr />

                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="password">New Password</label>
                            <input type="password" id="password" name="password" class="form-control" />
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="repassword">Confirm Password</label>
                            <input type="password" id="repassword" name="repassword" class="form-control" />
                        </div>
                    </div>
                </div>
               
                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Update</button> 
                    <a href="<?= admin_url('dashboard') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>