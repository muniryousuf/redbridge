<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="page_id" class="required">Page</label>
                    <select id="page_id" name="page_id" class="form-control" requiredd>
                        <option value="">Select Page Or Custom Link Option</option>
                        <option value="custom" <?= isset($row['page_id']) && $row['page_id'] == 0 ? 'selected' : '' ?>>-- Custom Link --</option>
                        <?php foreach ($pages as $p) { ?>
                        <option value="<?= $p['id'] ?>" <?= isset($row['page_id']) && $row['page_id'] == $p['id'] ? 'selected' : '' ?>><?= $p['title'] ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="label" class="required">Label</label>
                    <input type="text" id="label" name="label" class="form-control" value="<?= isset($row['label']) ? $row['label'] : "" ?>" requiredd />
                </div>

                <div id="custom-link" class="form-group col-sm-12" style="display:none;">
                    <label for="link" class="required">Custom Link</label>
                    <input type="text" id="link" name="link" class="form-control" value="<?= isset($row['link']) ? $row['link'] : "" ?>"  />
                </div>

                <div class="form-group col-sm-12">
                    <label for="attributes" >Attributes</label>
                    <input type="text" id="attributes" name="attributes" class="form-control" value="<?= isset($row['attributes']) ? $this->Menu_items->htmlEncode($row['attributes']) : "" ?>"  />
                </div>

                <div class="form-group col-sm-6">
                    <label for="parent_id" >Parent</label>
                    <select id="parent_id" name="parent_id" class="form-control">
                        <option value=""></option>
                        <?php foreach ($parents as $p) { ?>
                        <option value="<?= $p['id'] ?>" <?= isset($row['parent_id']) && $row['parent_id'] == $p['id'] ? 'selected' : '' ?>><?= $p['label'] ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="status" class="required">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option value="1" <?= isset($row['status']) && $row['status'] ? 'selected' : "" ?>>Active</option>
                        <option value="0" <?= isset($row['status']) && !$row['status'] ? 'selected' : "" ?>>Inactive</option>
                    </select>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('menuitems/index/' . $menu['id']) ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="application/javascript">
$(function(){
    $('#page_id').change(function(){
        if($(this).val() == 'custom') {
            $('#custom-link').show();
        } else {
            $('#custom-link').hide();
            $('#link').val('');
        }

        if($('#label').val() == '' && $(this).val() > 0) {
            $('#label').val($("#page_id option:selected").text());
        }
    });

    $('#page_id').trigger('change');
});
</script>