<p>Hello <?= $name ?>,</p>
<p>
    We have received your message and will respond within 2 working days. Below you will find a copy of your message.</p>
<p><strong>Name:</strong> <?= $name ?></p>
<p><strong>E-mail:</strong> <?= $email ?></p>
<p><strong>Telephone:</strong> <?= $phone ?></p>
<p><strong>Message:</strong> <?= nl2br($message) ?></p>
