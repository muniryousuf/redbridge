<p>You have received the following order</p>
<h2>[Bestelling #<?= $order['id'] ?>] (<?= date('d M Y', strtotime($order['created_at'])) ?>)</h2>

<div style="margin-bottom: 40px;">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<thead>
			<tr>
				<th class="td">Product</th>
				<th class="td">Quantity</th>
				<th class="td">Pirce</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($products as $product) { ?>
			<tr>
				<td class="td"><?= $product['name'] ?></td>
				<td class="td"><?= $product['quantity'] ?></td>
				<td class="td"><?= format_money($product['price'] * $product['quantity']) ?></td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th class="td" colspan="2">Sub total:</th>
				<td class="td"><?= format_money($order['sub_total']) ?></td>
			</tr>
			<tr>
				<th class="td" colspan="2">Shipment:</th>
				<td class="td">Free Shipping
                </td>
			</tr>
			<tr>
				<th class="td" colspan="2">Payment method
                    :</th>
				<td class="td"><?= $order['payment_method'] ?></td>
			</tr>
			<tr>
				<th class="td" colspan="2">Total:</th>
				<td class="td"><?= format_money($order['net_total']) ?></td>
			</tr>
		</tfoot>
	</table>
</div>

<table id="addresses" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top; margin-bottom: 40px; padding:0;" border="0">
	<tr>
		<td style="font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; border:0; padding:0;" valign="top" width="50%">
			<h2>Billing address
            </h2>

			<address class="address">
				<?php
				if($billing) {
	                echo $billing['first_name'] . ' ' . $billing['last_name'];
	                echo '<br />';
	                echo $billing['address1'];
	                echo '<br />';
	                echo $billing['address2'];
	                echo '<br />';
	                echo $billing['city'] . ', ' . $billing['postalcode'];
	                echo '<br />';
	                echo $billing['country'];
	            }
	            ?>
	        </address>
		</td>
		<td style="font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; padding:0;" valign="top" width="50%">
			<h2>Shipping Address
            </h2>
			<address class="address">
				<?php
				if($billing) {
		            echo $shipping['first_name'] . ' ' . $shipping['last_name'];
		            echo '<br />';
		            echo $shipping['address1'];
		            echo '<br />';
		            echo $shipping['address2'];
		            echo '<br />';
		            echo $shipping['city'] . ', ' . $shipping['postalcode'];
		            echo '<br />';
		            echo $shipping['country'];
		        }
	            ?>
	        </address>
		</td>
	</tr>
</table>

<p>
    Congratulations on the sale.</p>
