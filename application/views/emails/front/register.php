<p>
    Hey <?= $first_name ?>,</p>
<p>Thanks for creating an account with <?= site_title() ?>.</p>
<p>You can access your account area to view orders, change your password and more, at: <a href="<?= account_url() ?>"><?= account_url() ?></a></p>
<p>
    We look forward to seeing you.</p>
