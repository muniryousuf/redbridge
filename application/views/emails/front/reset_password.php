<p>Hey <?= $first_name ?>,</p>
<p>Someone has requested a new password for the next account on<?= site_title() ?>:</p>
<p>E-mail: <?= $email ?>
<p>if you have not requested this, you can ignore this email. If you want to continue:</p>
<p><a href="<?= $reset_url ?>">
        Click here to reset your password</a></p>
<p>
    Thank you for reading.</p>
