<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $page['title'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current"><?= $page['title'] ?></li>
			</ol>
    	</div>

    	<?php
    	$faqcats = $this->FaqCats
    					->find()
                        ->get()
                        ->result_array();

        foreach ($faqcats as $fc)
        {
        	$faqs = $this->Faqs
        				->find()
        				->where('category_id', $fc['id'])
        				->where('status', 1)
        				->order_by('sort_order', 'asc')
        				->get()
        				->result_array();

        	if($faqs)
        	{
        		?>
        		<div class="faq-wrap">
        			<h2><?= $fc['name'] ?></h2>
        			<p><?= $fc['description'] ?></p>

        			<div class="accordion" id="faqs<?= $fc['id'] ?>">
        				<?php foreach ($faqs as $k => $f) { ?>
        				<div class="faq-single">
		                    <div id="heading<?= $fc['id'].'_'.$k ?>">
	                            <button class="btn btn-link question" type="button" data-toggle="collapse" data-target="#collapse<?= $fc['id'].'_'.$k ?>" aria-expanded="true" aria-controls="collapse<?= $fc['id'].'_'.$k ?>">
	                             	<?= $f['question'] ?>
	                            </button>
		                    </div>

		                    <div id="collapse<?= $fc['id'].'_'.$k ?>" class="answer collapse" aria-labelledby="heading<?= $fc['id'].'_'.$k ?>" data-parent="#faqs<?= $fc['id'] ?>">
		                        <?= $f['answer'] ?>
		                    </div>
		                </div>		
        				<?php } ?>
        			</div>
        		</div>
        		<?php
        	}
        }
        ?>

        <?= prepare_content($page['content']) ?>
    </div>
</div>