<?php
// echo "<pre>";
// print_r($cart);
 ?>
<div id="main" class="section">
    <div class="container">
    	<?php if($cart['items']) { ?>
        <div class="row">
        	<div class="col-md-8">
        		<form id="frm-cart" action="<?= url('/cart/update') ?>" method="post">
        			<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th class="product-remove"></th>
									<th class="product-name">Product</th>
									<th class="product-thumbnail"></th>
									<th class="product-price">Price</th>
									<th class="product-quantity">Quantity</th>
									<th class="product-subtotal">Total</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($cart['items'] as $key => $item) { ?>
								<tr>
									<td class="product-remove">
										<a href="<?= url('/cart/remove?key=' . $key) ?>" class="remove-item"><i class="fas fa-times"></i></a>
									</td>
									<td class="product-thumbnail">
										<a href="<?= $item['url'] ?>"><img src="<?= $item['image'] ?>" class="img-fluid"></a>
									</td>
									<td class="product-name">
										<a href="<?= $item['url'] ?>"><?= $item['product_name'] ?></a>
										<?php

										if ($item['attribute'])
										{

											if (!empty($item['attribute_name']))
											{
                                                echo "<br /><small>". implode(", ",$item['attribute_name'])."</small>";
//												foreach ($item['attribute_name'] as $key1 => $value1)
//												{
//												  echo "<br /><small>".($key1+1).") ".implode(", ",$value1)."</small>";
//												}
											 ?>

										 	 <?php
											}
										}
										?>
										<br />
										<small><?= ($item['discountName']??"").($item['discountMessage']??"") ?></small>
									</td>
									<td class="product-price">
										<?php if($item['sale_price'] && $item['sale_price'] > 0) { ?>
					                    <?= format_money($item['sale_price']) ?>
					                    <?php } else { ?>
					                    <?= format_money($item['price']) ?>
					                    <?php } ?>
									</td>
									<td class="product-quantity">
										<div class="form-row">
					                        <div class="form-group m-0">
					                            <input  <?php echo isset($item['discountedPrice']) && $item['discountedPrice'] > 0 ? 'disabled':''?> type="text" name="quantity[<?= $key ?>]" value="<?= $item['quantity'] ?>" min="1" class="form-control form_elements text-center numberonly quantity">
					                        </div>
					                    </div>
									</td>
									<td class="product-subtotal">
										<?php
										if(isset($item['discountedPrice'])  && $item['discountedPrice'] > 0) {
											echo format_money($item['discountedPrice']);
										}
										else if($item['sale_price'] && $item['sale_price'] > 0) { ?>
					                    <?= format_money($item['sale_price'] * $item['quantity']) ?>
					                    <?php } else { ?>
					                    <?= format_money($item['price'] * $item['quantity']) ?>
					                    <?php } ?>
									</td>
								</tr>
								<?php } ?>

								<tr>
									<td colspan="6" class="actions">
										<button type="submit" class="btn btn-update-cart" name="update"><i class="fas fa-sync"></i> Update shopping cart</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</form>
        	</div>

        	<div class="col-md-4">
        		<div class="cart-total">
					<h2>Total shopping cart
                    </h2>
					<table class="table">
						<tbody>
							<tr class="cart-subtotal">
								<th>Subtotal</th>
								<td><?= format_money($cart['subtotal']) ?></td>
							</tr>
							<tr class="cart-shipping">
								<th>Shipment</th>
								<td>Enter your address to view shipping option</td>
							</tr>

							<tr class="cart-shipping">
								<th>ETA</th>
								<td>Estimated  date :

                                   <?php
                                   $day = date("D");
                                   if($day === 'Sat'){
                                       echo date('D M j', strtotime(' +2 day'));
                                   }else if($day === 'Sun'){
                                       echo date('D M j', strtotime(' +1 day'));
                                   }else {
                                       echo date('D M j', strtotime(' +1 day'));
                                   }
                                   ?> .</td>
							</tr>

                            <tr class="cart-tax">
                                <th><?= $cart['tax_percent'] ?>% VAT</th>
                                <td><?= format_money($cart['tax']) ?></td>
                            </tr>

							<tr class="order-total">
								<th>Total</th>
								<td>
									<strong><?= format_money($cart['nettotal']) ?></strong>
								</td>
							</tr>
						</tbody>
					</table>

					<p class="checkout-bt">
						<a href="<?= checkout_url() ?>" class="d-block theme_btn text-uppercase text-center btnProceedToCheckout" style="">Continue to checkout</a>
					</p>
					<p class="checkout-bt">
						<a href="<?= shop_url() ?>" class="d-block theme_btn text-uppercase text-center btnProceedToCheckout" style="">Continue to Shopping</a>
					</p>
				</div>
        	</div>
        </div>
    	<?php } else { ?>
    	<div class="alert alert-info">
            Your shopping cart is currently empty.</div>

    	<p class="py-5 text-center">
			<a class="theme_btn text-uppercase" href="<?= shop_url() ?>">Back to shop
            </a>
		</p>
    	<?php } ?>
    </div>
</div>

<div id="main" class="section">
    <div class="container">
    	<?= prepare_content($page['content']) ?>
    </div>
</div>

<?php if($crossell_products) { ?>
<section id="product_related">
    <div class="container">
        <div class="row pb-5">
            <div class="col-12">
                <h4 class="border_bottom text-uppercase">You May Also Like ...
                </h4>
            </div>
            <?php foreach ($crossell_products as $pro) { ?>
            <div class="col-lg-4 pb-4">
                <div class="row px-3 related_product_inner">
                    <div class="col-3 px-0 img_wrapper">
                    	<?php if($pro['sale_price'] && $pro['sale_price'] > 0)  { ?>
                        <span class="percent bg_pink text-white d-flex align-items-center justify-content-center">%</span>
                    	<?php } ?>
                        <a href="<?= url('/product/' . $pro['slug']) ?>">
                        	<img src="<?= $pro['image'] ?>" class="img-fluid d-block mx-auto" />
                        </a>
                    </div>
                    <div class="col-9 pr-0 related_product_description">
                        <h5> <a href="<?= url('/product/' . $pro['slug']) ?>" class="text_black"><?= $pro['name'] ?></></h5>
                        <p>
                            <?php if($pro['sale_price'] && $pro['sale_price'] > 0)  { ?>
		                    <del class="text_gray"><?= format_money($pro['price']) ?></del>
		                    <span class="text_black"><?= format_money($pro['sale_price']) ?></span>
		                    <?php } else { ?>
		                    <span class="text_black"><?= format_money($pro['price']) ?></span>
		                    <?php } ?>
                        </p>
                        <div class="btn_wrapper">
                            <a href="<?= url('/product/' . $pro['slug']) ?>" class="theme_btn text-white">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>

