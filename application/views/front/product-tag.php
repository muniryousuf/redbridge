<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $tag ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current">Products tagged"<?= $tag ?>"</li>
			</ol>
    	</div>

        <div id="supergave">
		    <div class="row sale_wrapper py-5">
		    	<?php
		    	foreach ($products as $pro) {
		        	include APPPATH . 'views/front/partials/single-product.php';
		    	}
		    	?>
		    </div>
		</div>
    </div>
</div>