<?php if($orders) { ?>
<?php $status = order_statuses('dt'); ?>
<div class="table-responsive">
	<table class="table table-striped">
		<thead>
			<tr>
				<th width="10%" class="text-uppercase">Order</th>
				<th class="text-uppercase">Date</th>
				<th class="text-uppercase">Status</th>
				<th class="text-uppercase">Total</th>
				<th width="10%" class="text-uppercase">Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($orders as $o) { ?>
			<tr>
				<td><a href="<?= account_url() . '?view-order&id='. $o['id'] ?>">#<?= $o['id'] ?></a></td>
				<td><?= date('d M Y', strtotime($o['created_at'])) ?></td>
				<td><?= isset($status[$o['status']]) ? $status[$o['status']] : 'N/A' ?></td>
				<td><?= format_money($o['net_total']) ?></td>
				<td><a href="<?= account_url() . '?view-order&id='. $o['id'] ?>" class="text-uppercase theme_btn">See</a></td>
			<?php } ?>		
		</tbody>
	</table>
</div>
<?php } else { ?>
<p>No order placed yet.</p>
<p><a href="<?= shop_url() ?>" class="theme_btn text-uppercase">
        Search through products</a></p>
<?php } ?>