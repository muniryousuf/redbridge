<?php
$billing = isset($inputs['billing']) ? $inputs['billing'] : $billing;
$shipping = isset($inputs['shipping']) ? $inputs['shipping'] : $shipping;
?>
<form id="frm-address" action="" method="post">
	<input type="hidden" name="action" value="address" />

	<p>The following addresses are used by default on the checkout page.</p>

    <div class="row">
    	<div class="col-lg-6">
    		<h3 class="text-uppercase">Billing address</h3>
			<div class="row">
				<div class="col-12 col-md-6 form-group">
					<label for="billing_first_name">First Name<span class="required">*</span></label>
					<input type="text" id="billing_first_name" name="billing[first_name]" class="form-control" value="<?= isset($billing['first_name']) ? $billing['first_name'] : '' ?>" maxlength="50" required />
				</div>
				<div class="col-12 col-md-6 form-group">
					<label for="billing_last_name">Last name<span class="required">*</span></label>
					<input type="text" id="billing_last_name" name="billing[last_name]" class="form-control" value="<?= isset($billing['last_name']) ? $billing['last_name'] : '' ?>" maxlength="50" required />
				</div>
			</div>

			<div class="form-group">
				<label for="billing_country">Country <span class="required">*</span></label>
				<input type="text" id="billing_country" class="form-control" value="Pakistan" readonly />
			</div>

			<div class="form-group">
				<label for="billing_address1">
                    Street and house number<span class="required">*</span></label>
				<input type="text" id="billing_address1" name="billing[address1]" class="form-control" placeholder="Street and house number" value="<?= isset($billing['address1']) ? $billing['address1'] : '' ?>" maxlength="255" required />
				<input type="text" id="billing_address2" name="billing[address2]" class="form-control mt-2" placeholder="Appartement, suite, unit etc. (optional)" maxlength="255" value="<?= isset($billing['address2']) ? $billing['address2'] : '' ?>" />
			</div>

			<div class="form-group">
				<label for="billing_postalcode">
                    Postal Code <span class="required">*</span></label>
				<input type="text" id="billing_postalcode" name="billing[postalcode]" class="form-control" value="<?= isset($billing['postalcode']) ? $billing['postalcode'] : '' ?>" maxlength="10" required />
			</div>

			<div class="form-group">
				<label for="billing_city">Place <span class="required">*</span></label>
				<input type="text" id="billing_city" name="billing[city]" class="form-control" value="<?= isset($billing['city']) ? $billing['city'] : '' ?>" maxlength="50" required />
			</div>
		</div>

		<div class="col-lg-6">
			<h3 class="text-uppercase">Shipping Address
            </h3>
			<div class="row">
				<div class="col-12 col-md-6 form-group">
					<label for="shipping_first_name">
                        First Name</label>
					<input type="text" id="shipping_first_name" name="shipping[first_name]" class="form-control" value="<?= isset($shipping['first_name']) ? $shipping['first_name'] : '' ?>" maxlength="50" />
				</div>
				<div class="col-12 col-md-6 form-group">
					<label for="shipping_last_name">Last name
                    </label>
					<input type="text" id="shipping_last_name" name="shipping[last_name]" class="form-control" value="<?= isset($shipping['last_name']) ? $shipping['last_name'] : '' ?>" maxlength="50" />
				</div>
			</div>

			<div class="form-group">
				<label for="shipping_country">Country</label>
				<input type="text" id="shipping_country" class="form-control" value="Pakistan" readonly />
			</div>

			<div class="form-group">
				<label for="shipping_address1">Street and house number</label>
				<input type="text" id="shipping_address1" name="shipping[address1]" class="form-control" placeholder="Street and house number" value="<?= isset($shipping['address1']) ? $shipping['address1'] : '' ?>" maxlength="255" />
				<input type="text" id="shipping_address2" name="shipping[address2]" class="form-control mt-2" placeholder="Appartement, suite, unit etc. (optional)" value="<?= isset($shipping['address2']) ? $shipping['address2'] : '' ?>" maxlength="255" />
			</div>

			<div class="form-group">
				<label for="shipping_postalcode">
                    Postal Code</label>
				<input type="text" id="shipping_postalcode" name="shipping[postalcode]" class="form-control" value="<?= isset($shipping['postalcode']) ? $shipping['postalcode'] : '' ?>" maxlength="10" />
			</div>

			<div class="form-group">
				<label for="shipping_city">Place</label>
				<input type="text" id="shipping_city" name="shipping[city]" class="form-control" value="<?= isset($shipping['city']) ? $shipping['city'] : '' ?>" />
			</div>
		</div>
    </div>

    <p>
		<button type="submit" class="place-order d-block theme_btn text-uppercase text-center">Save address</button>
	</p>
</form>