<p>Hello <strong><?= $user['first_name'] ?></strong> (not <strong><?= $user['first_name'] ?></strong>? <a href="<?= url('logout') ?>">Log out</a>)</p>
<p>

    From your account dashboard you can
	<a href="<?= account_url() ?>/?orders">
        View your recent orders</a>,
	<a href="<?= account_url() ?>/?address">Manage your shipping and billing addresses
    </a>
	en
	<a href="<?= account_url() ?>/?account">
        Edit your password and account details</a>.
</p>