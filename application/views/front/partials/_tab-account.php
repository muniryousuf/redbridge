<?php
$user['first_name'] = isset($inputs['first_name']) ? $inputs['first_name'] : $user['first_name'];
$user['last_name']  = isset($inputs['last_name']) ? $inputs['last_name'] : $user['last_name'];
$user['email']      = isset($inputs['email']) ? $inputs['email'] : $user['email'];
?>
<form id="frm-account" action="" method="post">
	<input type="hidden" name="action" value="account" />
	<div class="row">
		<div class="col-12 col-md-6 form-group">
			<label for="first_name">First Name<span class="required">*</span></label>
			<input type="text" id="first_name" name="first_name" class="form-control" value="<?= $user['first_name'] ?>" maxlength="50" required />
		</div>
		<div class="col-12 col-md-6 form-group">
			<label for="last_name">Last name<span class="required">*</span></label>
			<input type="text" id="last_name" name="last_name" class="form-control" value="<?= $user['last_name'] ?>" maxlength="50" required />
		</div>
	</div>

	<div class="form-group">
		<label for="email">E-mail address <span class="required">*</span></label>
		<input type="email" id="email" name="email" class="form-control" value="<?= $user['email'] ?>" maxlength="50" required />
	</div>

	<h3 class="text-uppercase">Password change</h3>

	<div class="form-group">
		<label for="old_password">Current password (leave blank to not change)</label>
		<input type="password" id="old_password" name="old_password" class="form-control" value="" />
	</div>

	<div class="form-group">
		<label for="password">New password (leave blank to not change)</label>
		<input type="password" id="password" name="password" class="form-control" value="" minlength="8" />
	</div>

	<div class="form-group">
		<label for="repassword">Confirm new Password</label>
		<input type="password" id="repassword" name="repassword" class="form-control" value="" minlength="8" />
	</div>

	<p>
		<button type="submit" class="place-order d-block theme_btn text-uppercase text-center">Saving Changes</button>
	</p>
</form>