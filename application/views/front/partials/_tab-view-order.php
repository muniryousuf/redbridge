<?php $status = order_statuses('dt'); ?>
<p>Order #<span class="pink_bg"><?= $order['id'] ?></span> has been placed on <span class="pink_bg"><?= date('d M Y', strtotime($order['created_at'])) ?></span>  and is currently <span class="pink_bg"><?= isset($status[$order['status']]) ? $status[$order['status']] : 'N/A' ?></span>.</p>

<h4 class="py-3">Payment overview</h4>

<table class="table">
	<?php foreach($order['products'] as $product) { ?>
	<tr>
		<td><?= $product['name'] ?> x <?= $product['quantity'] ?></td>
		<td><?= format_money($product['price'] * $product['quantity']) ?></td>
	</tr>
	<?php } ?>
	<tr>
		<td>Subtotal:</td>
		<th><?= format_money($order['sub_total']) ?></th>
	</tr>
	<tr>
		<td>Shipment:</td>
		<th>Free Shipping</th>
	</tr>
	<tr>
		<td>
            Payment method:</td>
		<th><?= $order['payment_method'] ?></th>
	</tr>
	<tr>
		<td>Total:</td>
		<th><?= format_money($order['net_total']) ?></th>
	</tr>
</table>

<table class="table">
	<tr>
		<td>
            E-mail address</td>
		<td><?= $order['email'] ?></td>
	</tr>
	<tr>
		<td>Phone</td>
		<td><?= $order['phone'] ?></td>
	</tr>
	<tr>
		<td>
            Billing address</td>
		<td>
			<?php
            $addr = []; 
            foreach ($order['address'] as $a) {
                if($a['type'] == 1) {
                    $addr = $a;
                }
            }

            if($addr) {
                echo $addr['first_name'] . ' ' . $addr['last_name'];
                echo '<br />';
                echo $addr['address1'];
                echo '<br />';
                echo $addr['address2'];
                echo '<br />';
                echo $addr['city'] . ', ' . $addr['postalcode'];
                echo '<br />';
                echo $addr['country'];
            }
            ?>
		</td>
	</tr>
	<tr>
		<td>Shipping Address</td>
		<td>
			<?php
            $addr = []; 
            foreach ($order['address'] as $a) {
                if($a['type'] == 2) {
                    $addr = $a;
                }
            }

            if($addr) {
                echo $addr['first_name'] . ' ' . $addr['last_name'];
                echo '<br />';
                echo $addr['address1'];
                echo '<br />';
                echo $addr['address2'];
                echo '<br />';
                echo $addr['city'] . ', ' . $addr['postalcode'];
                echo '<br />';
                echo $addr['country'];
            } else {
                echo 'Same as billing address';
            }
            ?>
		</td>
	</tr>
</table>