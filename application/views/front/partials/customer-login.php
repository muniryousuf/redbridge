<div id="login-register" class="row mt-3">
	<?php if(isset($_GET['lost-password']) && $_GET['lost-password'] == 1) { ?>
	<div class="col-12 col-md-6 mx-auto">
		<div class="gray-box" style="min-height: initial;">
			<form id="frm-pwdreset" action="<?= url('dopwdrest') ?>" method="POST">
				<input type="hidden" name="action" value="forgot-password" />
				<p>
                    Forgot your password? Enter your username or email address. You will receive a link via email to set a new password.</p>

				<div class="form-group">
					<label for="email">
                        E-mail address <span class="required">*</span></label>
					<input type="email" name="email" class="form-control" required />
				</div>

				<button type="submit" name="login" class="theme_btn text-uppercase">
                    Reset Password</button>
			</form>
		</div>
	</div>
	<?php } elseif(isset($_GET['reset_password']) && $_GET['reset_password'] == 1) { ?>
	<div class="col-12 col-md-6 mx-auto">
		<div class="gray-box" style="min-height: initial;">
			<form id="frm-pwdupdate" action="<?= url('dopwdupdate') ?>" method="POST">
				<input type="hidden" name="action" value="reset-password" />
				<input type="hidden" name="email" value="<?= $_GET['email'] ?>" />
				<input type="hidden" name="code" value="<?= $_GET['code'] ?>" />
				<p class="text-center">
                    Enter a new password below.</p>

				<div class="form-group">
					<label for="password">
                        New password <span class="required">*</span></label>
					<input type="password" name="password" class="form-control" minlength="8" required />
				</div>

				<div class="form-group">
					<label for="repassword">
                        Repeat the new password <span class="required">*</span></label>
					<input type="password" name="repassword" class="form-control" minlength="8" required />
				</div>

				<button type="submit" name="login" class="theme_btn text-uppercase">Save</button>
			</form>
		</div>
	</div>	
	<?php } else { ?>
	<div class="col-12 col-md-6">
		<div class="gray-box">
			<form id="frm-login" action="<?= url('dologin') ?>" method="POST">
				<input type="hidden" name="action" value="login" />
				<h2>Login</h2>

				<div class="form-group">
					<label for="email">Email address <span class="required">*</span></label>
					<input type="email" name="email" class="form-control" required />
				</div>

				<div class="form-group">
					<label for="password">Password <span class="required">*</span></label>
					<input type="password" name="password" class="form-control" required />
				</div>

				<div class="row">
					<div class="col-12 col-md-6">
						<button type="submit" name="login" class="theme_btn">LOG IN</button>
					</div>
					<div class="col-12 col-md-6 forget-pass">
						<a href="<?= account_url() . '/?lost-password=1' ?>">Lost your password?</a>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="col-12 col-md-6">
		<div class="gray-box">
			<form id="frm-register" action="<?= url('doregister') ?>" method="POST">
				<input type="hidden" name="action" value="register" />
				<h2>Register</h2>

				<div class="form-group">
					<label for="email">Email address <span class="required">*</span></label>
					<input type="email" name="email" class="form-control" required />
				</div>

				<div class="form-group">
					<label for="password">Password <span class="required">*</span></label>
					<input type="password" name="password" class="form-control" minlength="8" required />
				</div>

				<p>
                    Your personal information is used to optimize your experience on this website, to manage access to your account and for other purposes described in our [privacy policy].</p>

				<div class="row">
					<div class="col-12 col-md-6">
						<button type="submit" name="register" class="theme_btn">REGISTER</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php } ?>
</div>