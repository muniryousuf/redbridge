<?php
$user = is_customer();
$tab = isset($_GET['orders']) ? 'orders' : 'dashboard';
$tab = isset($_GET['view-order']) ? 'view-order' : $tab;
$tab = isset($_GET['address']) ? 'address' : $tab;
$tab = isset($_GET['account']) ? 'account' : $tab;
?>
<div id="customer-dashboard" class="row customer-dashboard-section mt-3">
	<div class="col-md-4 col-lg-3">
		<ul class="nav-menu">
			<li <?= $tab == 'dashboard' ? 'class="active"' : '' ?>>
				<a href="<?= account_url() ?>">Dashboard</a>
			</li>
			<li <?= in_array($tab, ['orders', 'view-order']) ? 'class="active"' : '' ?>>
				<a href="<?= account_url() ?>/?orders">Orders</a>
			</li>
			<li <?= $tab == 'address' ? 'class="active"' : '' ?>>
				<a href="<?= account_url() ?>/?address">Addresses</a>
			</li>
			<li <?= $tab == 'account' ? 'class="active"' : '' ?>>
				<a href="<?= account_url() ?>/?account">Account information
                </a>
			</li>
			<li>
				<a href="<?= url('logout') ?>">
                    Log out</a>
			</li>
		</ul>
	</div>
	<div class="col-md-8 col-lg-9 login-status">
		<?php include '_tab-' . $tab . '.php'; ?>
	</div>
</div>