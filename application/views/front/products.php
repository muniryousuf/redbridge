<?php
$id = explode(',', $id);
$products = $this->Products->products_by_ids($id);
?>
<div id="supergave">
    <div class="row sale_wrapper py-5">
    	<?php
        foreach ($products as $pro) {
            include APPPATH . 'views/front/partials/single-product.php';
        }
        ?>
    </div>
</div>