<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title">Category Archives
                : <?= $category['name'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current"><?= $category['name'] ?></li>
			</ol>
    	</div>

    	<?php if($blogs) { ?>
        <div id="supergave">
		    <div class="card-columns py-5">
		    	<?php foreach ($blogs as $blog) { ?>
		    	<div class="card single-blog">
		    		<?php if($blog['image']) { ?>
		    		<img class="card-img-top" src="<?= resize_img($blog['image'], 640, 480) ?>" />
		    		<?php } ?>

		    		<div class="single-blog-content">
						<h3 class="entry-title">
							<a href="<?= url($blog['slug']) ?>" title="<?= $blog['title'] ?>" rel="bookmark"><?= $blog['title'] ?></a>
						</h3>

						<div class="post-meta">
							<span class="post-category"><a href="<?= url('/category/' . $blog['category_slug']) ?>"><?= $blog['category'] ?></a></span>
		        			<span class="post-author">Door <?= $blog['first_name'] ?> <?= $blog['last_name'] ?></span>
		        			<span class="post-time"><time datetime="<?= date('c', strtotime($blog['publish_date'])) ?>"><?= date('d M Y', strtotime($blog['publish_date'])) ?></time></span>
		        		</div>

						<p><?= limit_text($blog['content'], 55) ?></p>
					</div>
		    	</div>
		    	<?php } ?>
		    </div>
		</div>
		<?php } else { ?>
        <h3 class="text-center">Sorry, No blog found...</h3>
        <?php } ?>

    	<?= $this->pagination->create_links(); ?>
    </div>
</div>