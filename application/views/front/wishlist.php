<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $page['title'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current"><?= $page['title'] ?></li>
			</ol>
    	</div>

        <?= prepare_content($page['content']) ?>

        <?php if($products) { ?>
        <div id="supergave">
		    <div class="row sale_wrapper py-5">
		    	<?php
		    	$wishlist_page = 1;
		    	foreach ($products as $pro) {
		        	include APPPATH . 'views/front/partials/single-product.php';
		    	}
		    	?>
		    </div>
		</div>
		<?php } else { ?>
    	<div class="alert alert-info">
            Your wish list is currently empty.</div>

    	<p class="py-5 text-center">
			<a class="theme_btn text-uppercase" href="<?= shop_url() ?>">
                Back to shop</a>
		</p>
    	<?php } ?>
    </div>
</div>