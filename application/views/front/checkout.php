<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<style type="text/css">
    input.small{width:50px;}
    .twoColumn{float:left;margin:0 30px 20px 0;}
    .clearAll{clear:both;}
</style>

<div id="main" class="section">
    <div class="container">

        <form id="frm-checkout" action="<?= url('/process-order') ?>" method="post">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-12 col-md-6 form-group">
                            <label for="billing_first_name">First Name <span class="required">*</span></label>
                            <input type="text" id="billing_first_name" name="billing[first_name]" class="form-control" value="" maxlength="50" required />
                        </div>
                        <div class="col-12 col-md-6 form-group">
                            <label for="billing_last_name">
                                Last name <span class="required">*</span></label>
                            <input type="text" id="billing_last_name" name="billing[last_name]" class="form-control" value="" maxlength="50" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="billing_country">Country <span class="required">*</span></label>
                        <input type="text" id="billing_country" class="form-control" value="UK" readonly />
                    </div>

                    <div class="form-group">
                        <label for="billing_address1">
                            Street and house number <span class="required">*</span></label>
                        <input type="text" id="billing_address1" name="billing[address1]" class="form-control" placeholder="Street name and house number" value="" maxlength="255" required /><br>
                        <input type="text" id="billing_address2" name="billing[address2]" class="form-control mt-2" placeholder="Appartment, suite, unit etc. (optional)" maxlength="255" value="" />
                    </div>

                    <div class="form-group">
                        <label for="billing_postalcode">Postcode <span class="required">*</span></label>
                        <input type="text" id="billing_postalcode" name="billing[postalcode]" class="form-control" value="" maxlength="10" required />
                    </div>
                    <button type="button" id="getCharges">Get Delivery Charges</button>

                    <div class="form-group">
                        <label for="billing_city">
                            Place <span class="required">*</span></label>
                        <input type="text" id="billing_city" name="billing[city]" class="form-control" value="" maxlength="50" required />
                    </div>

                    <div class="form-group">
                        <label for="phone">
                            Telephone (optional)</label>
                        <input type="text" id="phone" name="phone" class="form-control" maxlength="50" value="" />
                    </div>

                    <div class="form-group">
                        <label for="email">
                            E-mail address<span class="required">*</span></label>
                        <input type="email" id="email" name="email" class="form-control" value="" maxlength="50" required />
                    </div>

                    <div class="form-group">
                        <label>Select Custom Date for Delivery: </label>
                        <div id="datepicker" class="input-group date" data-date-format="d M,y">
                            <input id="custom_date_val" class="form-control" type="text" readonly  name="custom_date"/>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                    <div class="form-group checkboxes mt-5 mb-4">
                        <label class="text-uppercase">
                            <input type="checkbox" id="create_account" name="create_account" value="1" />
                            <i class="fas fa-user-circle"></i> <strong>
                                Create an account?</strong>
                        </label>
                    </div>

                    <div id="password-div" class="form-group" style="display: none;">
                        <label for="account_password">
                            Create account password<span class="required">*</span></label>
                        <input type="password" id="account_password" name="account_password" class="form-control" value="" minlength="8" required />
                    </div>

                    <div class="form-group checkboxes mt-4 mb-5">
                        <label class="text-uppercase">
                            <input type="checkbox" id="new_shipping_address" name="new_shipping_address" value="1" />
                            <i class="fas fa-truck fa-flip-horizontal"></i> <strong>
                                Different delivery address?</strong>
                        </label>
                    </div>

                    <div id="shipping-div" style="display: none;">
                        <div class="row">
                            <div class="col-12 col-md-6 form-group">
                                <label for="shipping_first_name">First Name
                                    <span class="required">*</span></label>
                                <input type="text" id="shipping_first_name" name="shipping[first_name]" class="form-control" value="" maxlength="50" required />
                            </div>
                            <div class="col-12 col-md-6 form-group">
                                <label for="shipping_last_name">
                                    Last name <span class="required">*</span></label>
                                <input type="text" id="shipping_last_name" name="shipping[last_name]" class="form-control" value="" maxlength="50" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="shipping_country">
                                Country <span class="required">*</span></label>
                            <input type="text" id="shipping_country" class="form-control" value="Uk" readonly />
                        </div>

                        <div class="form-group">
                            <label for="shipping_address1">
                                Street and house number<span class="required">*</span></label>
                            <input type="text" id="shipping_address1" name="shipping[address1]" class="form-control" placeholder="Street name and house number" value="" maxlength="255" required />
                            <input type="text" id="shipping_address2" name="shipping[address2]" class="form-control mt-2" placeholder="Apartment, suite, unit etc. (optional)" value="" maxlength="255" />
                        </div>

                        <div class="form-group">
                            <label for="shipping_postalcode">Postcode <span class="required">*</span></label>
                            <input type="text" id="shipping_postalcode" name="shipping[postalcode]" class="form-control" value="" maxlength="10" required />
                        </div>
                        <button type="button" id="getChargesDelivery">Get Delivery Charges</button>

                        <div class="form-group">
                            <label for="shipping_city">
                                Place <span class="required">*</span></label>
                            <input type="text" id="shipping_city" name="shipping[city]" class="form-control" value="" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comments">Order notes (optional)</label>
                        <textarea id="comments" name="comments" class="form-control" placeholder="Notes about your order, for example special notes for delivery."></textarea>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="cart-total">
                        <h2>
                            Your order</h2>
                        <table class="table">
                            <tbody>
                            <?php foreach ($cart['items'] as $key => $item) { ?>
                                <tr class="cart-product">
                                    <td>
                                        <?= $item['product_name'] ?>
                                        x <?= $item['quantity'] ?>
                                    </td>
                                    <th>
                                        <?php
                                        if(isset($item['discountedPrice'])  && $item['discountedPrice'] > 0)
                                        {
                                            echo format_money($item['discountedPrice']);
                                        ?>
                                        <?php }else if($item['sale_price'] && $item['sale_price'] > 0) { ?>
                                            <?= format_money($item['sale_price'] * $item['quantity']) ?>
                                        <?php } else { ?>
                                            <?= format_money($item['price'] * $item['quantity']) ?>
                                        <?php } ?>
                                    </th>
                                </tr>
                            <?php } ?>

                            <tr class="cart-subtotal">
                                <th>Subtotal</th>
                                <td><?= format_money($cart['subtotal']) ?></td>
                                <input type="hidden" id="subtotal" value="<?= $cart['subtotal']?>"/>
                            </tr>
                            <tr class="cart-shipping">
                                <th>
                                    shipment</th>
                                <td id="shipping_charges">
                                    Free Shipping</td>
                            </tr>
                            <tr class="cart-shipping">
                                <th>ETA</th>
                                <td>Estimated  date  :
                                    <span  id="update_date">
                                    <?php
                                    $day = date("D");
                                    if($day === 'Sat'){
                                        echo date('D M j', strtotime(' +2 day'));
                                    }else if($day === 'Sun'){
                                        echo date('D M j', strtotime(' +1 day'));
                                    }else {
                                        echo date('D M j', strtotime(' +1 day'));
                                    }
                                    ?> </span></td>
                            </tr>
                            <?php if(!$cart['tax_included']) { ?>
                                <tr class="cart-tax">
                                    <th><?= $cart['tax_percent'] ?>% VAT</th>
                                    <td><?= format_money($cart['tax']) ?></td>
                                </tr>
                            <?php } ?>
                            <tr class="order-total">
                                <th>Total</th>
                                <td>
                                    <span id="total_amount" style="display: none"><?= $cart['nettotal']; ?></span>
                                    <strong id="show_total"> £ <?= number_format($cart['nettotal'], 2) ?></strong>
                                    <?php if($cart['tax_included']) { ?>
                                        <small class="includes_tax">(including <strong><?= format_money($cart['tax']) ?></strong> <?= $cart['tax_percent'] ?>% VAT)</small>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="payment-methods">
<!--                            --><?php //foreach (payment_methods() as $k => $pay) { ?>
                                <div class="single-payment-method">
                                    <label>
                                        <input type="hidden"  name="payment_method" class="payment-method" value="1"  />
<!--                                        <img src="--><?//= $pay['image'] ?><!--" />-->
                                    </label>

                                </div>
<!--                            --><?php //} ?>

                            <hr class="mb-5">

                            <h4 class="mb-3">Enter Your Card Details:</h4>
                            <label for="cardNumber">Card Number:</label>

                            <input type="tel" id="cardNumber" name="card-number" class="form-control mb-3"/>

                            <label for="cardholderName">Cardholder Name:</label>

                            <input type="text" id="cardholderName" name="cardholder-name" class="form-control mb-3" />

                            <p class="twoColumn mb-0">
                                <label>Expiry Date: </label>
                                <input type="tel" id="expiryDateMM" name="expiry-date-mm" placeholder="MM" class="small expiry-form mb-3"/>
                                <input type="tel" id="expiryDateYY" name="expiry-date-yy" placeholder="YY" class="small expiry-form mb-3"/></p>

                            <p class="twoColumn">
                                <label for="cvn">Security Code:</label>
                                <input style="width: 100px;" type="tel" id="cvn" name="cvn" class="small expiry-form" /></p>

                        </div>

                        <p class="privacy-text clearAll">

                            Your personal data is used to process your order and improve your experience on this website and it is described in our <a href="<?= url('/privacy-policy') ?>" target="_blank">
                                privacy policy</a>.
                        </p>

                        <p class="term-checkbox mb-4">
                            <label>
                                <input type="checkbox" name="terms" id="terms" value="1" required />

                                I have the <a href="<?= url('/terms-and-conditions') ?>" target="_blank">
                                    read and agree to the general terms and conditions </a> of the website <span class="required">*</span>
                            </label>
                        </p>

                        <?php if($cart['nettotal'] > 120) {?>

                        <p>
                            <button type="submit" id="submitOrder" class="place-order d-block theme_btn text-uppercase text-center">
                                Place an order</button>
                        </p>
                        <?php } else { ?>
                            <span  class="alert" style="color: red"> The minimum order for delivery is £120 </span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>

<div id="main" class="section">
    <div class="container">
        <?= prepare_content($page['content']) ?>
    </div>
</div>

<script>

    $('#datepicker').datepicker({
        clearBtn: true,
        format: "d M,yyyy",
        startDate: new Date(),
        daysOfWeekDisabled: [0, 6]
    }).change(function (val) {
        $('#update_date').html($('#custom_date_val').val())
    });


    $(document).on("submit","#frm-checkout",function(e){

        var cardNumberCheck = RealexRemote.validateCardNumber(document.getElementById('cardNumber').value);
        var cardHolderNameCheck = RealexRemote.validateCardHolderName(document.getElementById('cardholderName').value);
        var expiryDate = document.getElementById('expiryDateMM').value.concat(document.getElementById('expiryDateYY').value);
        var expiryDateFormatCheck = RealexRemote.validateExpiryDateFormat(expiryDate);
        var expiryDatePastCheck = RealexRemote.validateExpiryDateNotInPast(expiryDate);
        if (document.getElementById('cardNumber').value.charAt(0) == "3") {
            cvnCheck = RealexRemote.validateAmexCvn(document.getElementById('cvn').value);
        } else {
            cvnCheck = RealexRemote.validateCvn(document.getElementById('cvn').value);
        }
        if (cardNumberCheck == false || cardHolderNameCheck == false || expiryDateFormatCheck == false || expiryDatePastCheck == false || cvnCheck == false) {
            // code here to inform the cardholder of an input error and prevent the form submitting

            e.preventDefault()
            return false

        } else {
            return true;
        }
    });


</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
