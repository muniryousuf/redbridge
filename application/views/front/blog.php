<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $blog['title'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<?php if($category) { ?>
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/category/' . $category['slug']) ?>"><?= $category['name'] ?></a>
				</li>
				<?php } ?>
				<li class="current"><?= $blog['title'] ?></li>
			</ol>
    	</div>

    	<div class="row">
    		<div class="col-lg-9 pr-lg-5 blog-leftcol">
    			<?php if($blog['image']) { ?>
    			<div class="single-post-thumbnail">
    				<div class="fancy-date">
    					<span class="entry-month"><?= date('M', strtotime($blog['publish_date'])) ?></span>
    					<span class="entry-date"><?= date('d', strtotime($blog['publish_date'])) ?></span>
    					<span class="entry-year"><?= date('Y', strtotime($blog['publish_date'])) ?></span>
    				</div>
    				<span class="fancy-categories">
    					<a href="<?= url('/category/' . $category['slug']) ?>" rel="category tag"><?= $category['name'] ?></a>
    				</span>
    				<img src="<?= resize_img($blog['image'], 640, 320) ?>" />
    			</div>
    			<?php } ?>

        		<?= prepare_content($blog['content']) ?>

        		<div class="post-meta">
        			<div class="meta-detail">
	        			<span class="post-category">Categories&nbsp;<a href="<?= url('/category/' . $category['slug']) ?>"><?= $category['name'] ?></a></span>
	        			<span class="post-author">Through <?= $author['first_name'] ?> <?= $author['last_name'] ?></span>
	        			<span class="post-time"><time datetime="<?= date('c', strtotime($blog['publish_date'])) ?>"><?= date('d F Y', strtotime($blog['publish_date'])) ?></time></span>
	        		</div>
        		</div>

        		<?php if($next || $prev) { ?>
        		<nav class="navigation post-navigation" role="navigation">
        			<h2 class="screen-reader-text">Post navigation</h2>
        			<div class="nav-links">
        				<?php if($prev) { ?>
        				<a class="nav-previous" href="<?= url($prev['slug']) ?>" rel="prev">
        					<i class="fa fa-angle-left" aria-hidden="true"></i>
        					<span class="meta-nav" aria-hidden="true">Previous</span>
        					<span class="screen-reader-text">Previous post:</span>
        					<span class="post-title"><?= $prev['title'] ?></span>
        				</a>
        				<?php } else { ?>
    					<span class="nav-previous"></span>
        				<?php } ?>

        				<?php if($next) { ?>
        				<a class="nav-next" href="<?= url($next['slug']) ?>" rel="prev">
        					<i class="fa fa-angle-right" aria-hidden="true"></i>
        					<span class="meta-nav" aria-hidden="true">Next</span>
        					<span class="screen-reader-text">Next post:</span>
        					<span class="post-title"><?= $next['title'] ?></span>
        				</a>
        				<?php } else { ?>
    					<span class="nav-next"></span>
        				<?php } ?>
        			</div>
        		</nav>
        		<?php } ?>
        	</div>

        	<div class="col-lg-3 pl-lg-5 blog-rightcol">
        		<?php if($latest) { ?>
        		<div class="widget">
        			<div class="widget-title">Latest new
                        s</div>
        			<ul>
        				<?php foreach ($latest as $l) { ?>
        				<li><a href="<?= url($l['slug']) ?>"><?= $l['title'] ?></a> <?= date('d F Y', strtotime($l['publish_date'])) ?></li>
        				<?php } ?>
        			</ul>
        		</div>
        		<?php } ?>

        		<?php if($products) { ?>
        		<div class="widget">
        			<div class="widget-title">Products</div>
        			<ul class="product_list">
        				<?php foreach ($products as $p) { ?>
        				<li>
							<a href="<?= url('product/' . $p['slug']) ?>">
								<img src="<?= $p['image'] ?>" />
								<span class="product-title"><?= $p['name'] ?></span>
							</a>
							<span class="price"><?= $p['sale_price'] ? format_money($p['sale_price']) : format_money($p['price']) ?></span>
						</li>
        				<?php } ?>
        			</ul>
        		</div>
        		<?php } ?>
        	</div>
    </div>
</div>