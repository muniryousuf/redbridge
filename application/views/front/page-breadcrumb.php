<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $page['title'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current"><?= $page['title'] ?></li>
			</ol>
    	</div>

        <?= prepare_content($page['content']) ?>
    </div>
</div>