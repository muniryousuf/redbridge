


<section class="home-sec-1 container-fluid main-banner-section">
    <div class="row">
        <div class="col-sm-12">
            <a href=""><img class="banner" src="<?php echo base_url() ?>assets/images/Web_Header_v2.jpg"></a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <a href="<?php echo base_url('covid-19')?>"><p class="text-color">Covid-19 Updates </p></a>
        </div>
    </div>
</section>


<section class="home-sec-5 container">
<?= prepare_content($page['content']) ?>
</section>


<section class="home-sec-5 container">
    <div class="row">
        <div class="col-sm-12">
            <h2>TOP CATEGO<span>RIES</span></h2>
        </div>
    </div>
    <?php $categories = $this->ProCategories->getAllCategories(); ?>
    <div style="padding-top: 40px;" class="row">
        <?php foreach ($categories as $key => $parent){  ?>
            <?php if($key < 4) { ?>
            <div class="col-sm-3">

            <img src="<?= uploaded_url($parent['image']) ?>">
            <h3> <a href="<?php echo url('/product-category/' . $parent['slug']) ?>"> <?php echo $parent['name'] ?></a></h3>
            <ul class="item-list">
                <?php if (isset($parent['second_level']) && count($parent['second_level']) > 0) {
                foreach ($parent['second_level'] as $child) { ?>
                    <li><a href="<?php echo url('/product-category/' . $child['slug']) ?>"><?php echo $child['name'] ?></a></li>
                    <?php

                } }?>

            </ul>

        </div>

        <?php }} ?>
<!--        <div class="col-sm-3">-->
<!--            <img src="--><?php //echo base_url() ?><!--assets/images/top-category-3.jpg">-->
<!--            <h3>Timber & Joinery </h3>-->
<!--            <ul class="item-list">-->
<!--                <li><a href="#">Timbers</a></li>-->
<!--                <li><a href="#">Sheet Materials</a></li>-->
<!--                <li><a href="#">Stairs</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            <img src="--><?php //echo base_url() ?><!--assets/images/plast_1.jpg">-->
<!--            <h3>Plastering & Drylining</h3>-->
<!--            <ul class="item-list">-->
<!--                <li><a href="#">Plasterboards & Coving</a></li>-->
<!--                <li><a href="#">Plasters & Renders</a></li>-->
<!--                <li><a href="#">Plasters & Renders</a></li>-->
<!--                <li><a href="#">Plasters & Render Beads</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--        <div class="col-sm-3">-->
<!--            <img src="--><?php //echo base_url() ?><!--assets/images/top-category-2.jpg">-->
<!--            <h3>Garden & Landscaping</h3>-->
<!--            <ul class="item-list">-->
<!--                <li><a href="#">Paving & Slabs</a></li>-->
<!--                <li><a href="#">Fencing</a></li>-->
<!--            </ul>-->
<!--        </div>-->
    </div>
</section>




