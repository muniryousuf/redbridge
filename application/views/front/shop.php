<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title">
    			<?php if($search) { ?>
                    Search results: "<?= $search ?>"
    			<?php } else { ?>
    			<?= $page['title'] ?>
    			<?php } ?>
    		</h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<?php if($search) { ?>
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= shop_url() ?>"><?= $page['title'] ?></a>
				</li>
				<li class="current">Search results : "<?= $search ?>"</li>
				<?php } else { ?>
				<li class="current"><?= $page['title'] ?></li>
				<?php } ?>
			</ol>
    	</div>

        <?= prepare_content($page['content']) ?>

        <div id="supergave">
		    <div class="row sale_wrapper py-5">
		    	<?php
		    	foreach ($products as $pro) {
		        	include APPPATH . 'views/front/partials/single-product.php';
		    	}
		    	?>
		    </div>
		</div>
    </div>
</div>