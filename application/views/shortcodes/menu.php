<?php if(isset($id) && $id) { ?>
<ul class="nav flex-column">
    <?php foreach (menu_items($id) as $mi) { ?>
    <li class="nav-item">
        <a href="<?= $mi['link'] ?>" <?= $mi['attributes'] ?> class="nav-link px-0 text-white"><?= $mi['label'] ?></a>
    </li>
    <?php } ?>
</ul>
<?php } ?>