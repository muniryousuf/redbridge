<?php
$products = $this->ProCategories->getAllFeaturesProducts();
$tax_per = isset($this->vars['tax_percentage']) ? (int) $this->vars['tax_percentage']/100 : 0;

?>



<section class="home-sec-3 container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>FEATU<span>RED</span> PROD<span>UCTS</span></h2>
        </div>
    </div>

    <!--carousel fedi-->
    <?php if($products && count($products)> 0) { ?>
    <div class="owl-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer">
            <div class="owl-stage"
                 style="transform: translate3d(-4246px, 0px, 0px); transition: all 0.25s ease 0s; width: 100%;">

                <?php foreach ($products as $product)  {?>
                    <div class="owl-item active"  >
                        <div class="item product-box">
                            <a href="<?= url('/product/' . $product['slug']) ?>"><img src="<?php echo $product['image'] ?>"
                                                                                      class="d-block w-100" alt=""></a>
                            <p class="truncate"><?php  echo $product['name']?></p>
                            <p class="text-before-2"><?php  echo format_money($product['price'])?><span>Excl Vat</span></p>
                            <p class="text-before-2"><?php  $taxPrice = $product['price'] + $product['price']* $tax_per; echo format_money($taxPrice); ?><span>Incl Vat</span></p>
                            <a href="<?= url('/product/' . $product['slug']) ?>">
                                <button>Details</button>
                            </a>
                        </div>
                    </div>
                <?php  } ?>




            </div>
        </div>
        <div class="owl-nav disabled">
            <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
            <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
        </div>
        <div class="owl-dots">
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot active"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
        </div>
    </div>
<?php }  ?>


</section>
