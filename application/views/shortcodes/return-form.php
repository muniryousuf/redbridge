<?php
$this->js = [
    assets('js/jquery.validate.min.js'),
    assets('js/additional-methods.min.js'),
];
?>
<form id="frm-return" action="" method="post">
    <div class="row">
		<div class="col-md-6">
			<div class="err"></div>

			<div class="form-group">
				<label for="orderno">Ordernummer <span class="required">*</span></label>
				<input type="text" id="orderno" name="orderno" class="form-control" value="" required />
			</div>

			<div class="form-group">
				<label for="name">Voor- en achternaam <span class="required">*</span></label>
				<input type="text" id="name" name="name" class="form-control" value="" required />
			</div>

			<div class="form-group">
				<label for="email">E-mailadres <span class="required">*</span></label>
				<input type="email" id="email" name="email" class="form-control" value="" required />
			</div>

			<div class="form-group">
				<label for="phone">Telefoon</label>
				<input type="text" id="phone" name="phone" class="form-control" value="" />
			</div>

			<div class="form-group">
				<label for="delivery_date">Leverdatum<span class="required">*</span></label>
				<input type="text" id="delivery_date" name="delivery_date" class="form-control" value="" required />
			</div>

			<div class="form-group">
				<label for="reason1">Reden retour<span class="required">*</span></label>
				<select id="reason1" name="reason1" class="form-control" required>
					><option value="">Selecteer reden</option>
					<option value="Te klein">Te klein</option>
					<option value="Te groot">Te groot</option>
					<option value="Voldoet niet aan verwachtingen">Voldoet niet aan verwachtingen</option>
					<option value="Productiefout, kapot, vlek, enz.">Productiefout, kapot, vlek, enz.</option>
					<option value="Verkeerd artikel geleverd">Verkeerd artikel geleverd</option>
				</select>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="item1">Artikel</label>
						<input type="text" id="item1" name="item1" class="form-control" value="" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="measure1">Maat</label>
						<input type="text" id="measure1" name="measure1" class="form-control" value="" />
					</div>
				</div>
			</div>

			<hr />

			<div class="form-group">
				<label for="reason2">Reden retour</label>
				<select id="reason2" name="reason2" class="form-control">
					><option value="">Selecteer reden</option>
					<option value="Te klein">Te klein</option>
					<option value="Te groot">Te groot</option>
					<option value="Voldoet niet aan verwachtingen">Voldoet niet aan verwachtingen</option>
					<option value="Productiefout, kapot, vlek, enz.">Productiefout, kapot, vlek, enz.</option>
					<option value="Verkeerd artikel geleverd">Verkeerd artikel geleverd</option>
				</select>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="item2">Artikel</label>
						<input type="text" id="item2" name="item2" class="form-control" value="" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="measure2">Maat</label>
						<input type="text" id="measure2" name="measure2" class="form-control" value="" />
					</div>
				</div>
			</div>

			<hr />

			<div class="form-group">
				<label for="reason3">Reden retour</label>
				<select id="reason3" name="reason3" class="form-control">
					><option value="">Selecteer reden</option>
					<option value="Te klein">Te klein</option>
					<option value="Te groot">Te groot</option>
					<option value="Voldoet niet aan verwachtingen">Voldoet niet aan verwachtingen</option>
					<option value="Productiefout, kapot, vlek, enz.">Productiefout, kapot, vlek, enz.</option>
					<option value="Verkeerd artikel geleverd">Verkeerd artikel geleverd</option>
				</select>
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="item3">Artikel</label>
						<input type="text" id="item3" name="item3" class="form-control" value="" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="measure3">Maat</label>
						<input type="text" id="measure3" name="measure3" class="form-control" value="" />
					</div>
				</div>
			</div>

			<hr />

			<div class="form-group">
				<label for="comments">Opmerkingen <span class="required">*</span></label>
				<textarea id="comments" name="comments" class="form-control"></textarea>
			</div>

			<button type="submit" class="theme_btn text-uppercase text-center">Verzenden</button>
		</div>
	</div>
</form>
