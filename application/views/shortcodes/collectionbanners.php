<?php
$id = explode(',', $id);
$categories = $this->ProCategories->category_by_ids($id);
?>




<!-- START SECTION BANNER -->
<div class="section pb_20">
	<div class="container">
    	<div class="row">
        <?php foreach ($categories as $cat) { ?>
        	<div class="col-md-6">
            	<div class="single_banner">
                    <img src="<?= $cat['image'] ?>"  />
                    <div class="single_banner_info">
                        <h5 class="single_bn_title1">Super Sale</h5>
                        <h3 class="single_bn_title"><?= $cat['name'] ?></h3>
                        <a href="shop-left-sidebar.html" class="single_bn_link">Shop Now</a>
                    </div>
                </div>
            </div>
        <?php } ?>
        </div>
    </div>
</div>
<!-- END SECTION BANNER -->