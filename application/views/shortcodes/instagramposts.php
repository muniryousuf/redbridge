<!-- START SECTION INSTAGRAM IMAGE -->
<div class="section small_pt small_pb">
	<div class="container-fluid p-0">
        <div class="row no-gutters">
        	<div class="col-12">
            	
            	<div class="follow_box">
                    <i class="ti-instagram"></i>
                    <h3>instagram</h3>
                    <span>@shoppingzone</span>
                </div>
            	<div class="client_logo carousel_slider owl-carousel owl-theme" data-dots="false" data-margin="0" data-loop="true" data-autoplay="true" data-responsive='{"0":{"items": "2"}, "480":{"items": "3"}, "767":{"items": "4"}, "991":{"items": "6"}}'>
                	<div class="item">
                    	<div class="instafeed_box">
                        	<a href="#">
                                <img src="assets/theme1/images/furniture_insta1.jpg" alt="furniture_insta1"/>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="instafeed_box">
                        	<a href="#">
                                <img src="assets/theme1/images/furniture_insta2.jpg" alt="furniture_insta2"/>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="instafeed_box">
                        	<a href="#">
                                <img src="assets/theme1/images/furniture_insta3.jpg" alt="furniture_insta3"/>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="instafeed_box">
                        	<a href="#">
                                <img src="assets/theme1/images/furniture_insta4.jpg" alt="furniture_insta4"/>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="instafeed_box">
                        	<a href="#">
                                <img src="assets/theme1/images/furniture_insta5.jpg" alt="furniture_insta5"/>
                            </a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="instafeed_box">
                        	<a href="#">
                                <img src="assets/theme1/images/furniture_insta6.jpg" alt="furniture_insta6"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION INSTAGRAM IMAGE --> 