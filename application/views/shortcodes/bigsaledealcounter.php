<!-- START SECTION BANNER --> 
<div class="section background_bg" data-img-src="assets/theme1/images/furniture_banner3.jpg">
	<div class="container">
    	<div class="row">
            <div class="col-lg-6 col-md-8 col-sm-9">
            	<div class="furniture_banner">
                    <h3 class="single_bn_title">Big Sale Deal</h3>
                    <h4 class="single_bn_title1 text_default">Sale 40% Off</h4>
                    <div class="countdown_time countdown_style3 mb-4" data-time="2020/06/02 12:30:15"></div>
                    <a href="#" class="btn btn-fill-out">Shop Now</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END SECTION BANNER -->