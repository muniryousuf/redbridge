<?php
$id = explode(',', $id);
$categories = $this->ProCategories->category_by_ids($id,272,170);
?>
<div id="categories_carousel" class="row">
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="owl-carousel desk_carousel">
            <?php foreach ($categories as $k => $value){ ?>
                <div class="item">
                    <a href="<?php echo url('/product-category/' . $value['slug']) ?>">
                        <div class="img_wrapper">
                            <img src="<?php echo $value['image'] ?>" alt="image" />
                            <div id="categorypointer"><?php echo $value['name'] ?></div>
                        </div>
                        <div class="card-footer"><?php echo  $value['name'] ?></div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>