<?php
$products = $this->ProCategories->getBestSeller();
$tax_per = isset($this->vars['tax_percentage']) ? (int) $this->vars['tax_percentage']/100 : 0;

?>
<section class="home-sec-4 container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>BE<span>ST</span> SELL<span>ERS</span></h2>
        </div>
    </div>

    <?php if($products && count($products)> 0){ ?>

    <div class="owl-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer">
            <div class="owl-stage"
                 style="transform: translate3d(-4246px, 0px, 0px); transition: all 0.25s ease 0s; width: 100%;">

                <?php foreach ($products as $product)  {?>
                    <div class="owl-item active"  >
                        <div class="item product-box">
                            <a href="<?= url('/product/' . $product['slug']) ?>"><img src="<?php echo $product['image'] ?>"
                                                                                      class="d-block w-100" alt=""></a>
                            <p class="truncate"><?php  echo $product['name']?></p>
                            <p class="text-before-2"> <?php  echo format_money($product['price'])?><span>Excl Vat</span></p>
                            <p class="text-before-2"> <?php $taxPrice = $product['price'] + $product['price']* $tax_per; echo format_money($taxPrice); ?><span>Incl Vat</span></p>
                            <a href="<?= url('/product/' . $product['slug']) ?>">
                                <button>Details</button>
                            </a>
                        </div>
                    </div>
                <?php  } ?>
<!--                <div class="owl-item active ">-->
<!--                    <div class="item product-box">-->
<!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
<!--                                        class="d-block w-100" alt=""></a>-->
<!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
<!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
<!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
<!--                        <a style="padding-left: 25px;" href="">-->
<!--                            <button>ADD TO CART</button>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="owl-item active ">-->
<!--                    <div class="item product-box">-->
<!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
<!--                                        class="d-block w-100" alt=""></a>-->
<!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
<!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
<!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
<!--                        <a style="padding-left: 25px;" href="">-->
<!--                            <button>ADD TO CART</button>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="owl-item active ">-->
<!--                    <div class="item product-box">-->
<!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
<!--                                        class="d-block w-100" alt=""></a>-->
<!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
<!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
<!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
<!--                        <a style="padding-left: 25px;" href="">-->
<!--                            <button>ADD TO CART</button>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="owl-item active ">-->
<!--                    <div class="item product-box">-->
<!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
<!--                                        class="d-block w-100" alt=""></a>-->
<!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
<!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
<!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
<!--                        <a style="padding-left: 25px;" href="">-->
<!--                            <button>ADD TO CART</button>-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->


            </div>
        </div>
        <div class="owl-nav disabled">
            <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
            <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
        </div>
        <div class="owl-dots">
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot active"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
        </div>
    </div>

<?php }  ?>
</section>
