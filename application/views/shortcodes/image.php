<?php
$width  = isset($width) ? (int) $width : 0;
$height = isset($height) ? (int) $height : 0;

echo '<img src="' . resize_img($src, $width, $height) . '" class="img-fluid" />';