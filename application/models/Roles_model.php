<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles_model extends My_Model {

	public $fillables = ["name"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|required'
			)
		);

		return $rules;
    }
}
