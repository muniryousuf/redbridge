<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends My_Model {

	public $fillables = ["name","slug","sku","short_description","description","tags","image","stock","price","sale_price","sale_start","sale_end","status","meta_title","meta_desc","index_follow","sort"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'image',
				'label' => 'Image',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|required|callback_validate_slug',
				'errors' => array(
					'validate_slug' => 'Slug already taken.'
				)
			)
		);

		return $rules;
    }

    public function products_by_ids($ids, $img_width = 300, $img_height = 400)
    {
    	$products = $this->Products
    					->find()
    					->select('id, name, slug, image, price, sale_price, sale_start, sale_end')
    					->where_in('id', $ids)
    					->where('status', 1)
//    					->order_by('field(id, ' . implode(',', $ids) . ')')
                        ->order_by('sort', 'Asc')
    					->get()
    					->result_array();

    	$today = time();

    	foreach ($products as $k => $p)
    	{
    		if(!$p['price'])
    		{
    			$attr = $this->ProductAttributes
    						->find()
    						->where('product_id', $p['id'])
    						->where('(price > 0 OR sale_price > 0)')
    						->order_by('sale_price', 'asc')
    						->order_by('price', 'asc')
    						->get()
    						->row_array();

    			$products[$k]['price']      = $attr['price'];
    			$products[$k]['sale_price'] = $attr['sale_price'];
    			$products[$k]['sale_start'] = $attr['sale_start'];
    			$products[$k]['sale_end']   = $attr['sale_end'];
    		}

    		$on_sale = $products[$k]['sale_price'] ? 1 : 0;
    		$on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
    		$on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

    		$products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
    		unset($products[$k]['sale_start']);
    		unset($products[$k]['sale_end']);

    		$sec_img = $this->ProductImages
    						->find()
    						->where('product_id', $p['id'])
    						->order_by('id', 'asc')
    						->get()
    						->row_array();

    		$products[$k]['image']     = resize_img($products[$k]['image'], $img_width, $img_height);
    		$products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], $img_width, $img_height) : $p['image'];
    	}

    	return $products;
    }

    public function related_products($id, $cids, $limit = 3)
    {
        $products = $this->Products
                        ->setAlias('p')
                        ->find()
                        ->select('p.id, p.name, p.slug, p.image, p.price, p.sale_price, p.sale_start, p.sale_end')
                        ->join('product_categories_map AS c', 'c.product_id = p.id')
                        ->where('p.id != ' .  $id)
                        ->where_in('c.category_id', $cids)
                        ->where('p.status', 1)
//                        ->order_by('p.id', 'desc')
                        ->order_by('p.sort', 'Asc')
                        ->limit($limit)
                        ->get()
                        ->result_array();

        $today = time();

        foreach ($products as $k => $p)
        {
            if(!$p['price'])
            {
                $attr = $this->ProductAttributes
                            ->find()
                            ->where('product_id', $p['id'])
                            ->where('(price > 0 OR sale_price > 0)')
                            ->order_by('sale_price', 'asc')
                            ->order_by('price', 'asc')
                            ->get()
                            ->row_array();

                $products[$k]['price']      = $attr['price'];
                $products[$k]['sale_price'] = $attr['sale_price'];
                $products[$k]['sale_start'] = $attr['sale_start'];
                $products[$k]['sale_end']   = $attr['sale_end'];
            }

            $on_sale = $products[$k]['sale_price'] ? 1 : 0;
            $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
            $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

            $products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
            unset($products[$k]['sale_start']);
            unset($products[$k]['sale_end']);

            $sec_img = $this->ProductImages
                            ->find()
                            ->where('product_id', $p['id'])
                            ->order_by('id', 'asc')
                            ->get()
                            ->row_array();

            $products[$k]['image']     = resize_img($products[$k]['image'], 100, 133);
            $products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], 100, 133) : $p['image'];
        }

        return $products;
    }

    public function also_like($ids = [0], $limit = 3)
    {
        $products = $this->Products
                        ->setAlias('p')
                        ->find()
                        ->select('p.id, p.name, p.slug, p.image, p.price, p.sale_price, p.sale_start, p.sale_end')
                        ->join('product_categories_map AS c', 'c.product_id = p.id')
                        ->where_not_in('p.id', $ids)
                        ->where('p.status', 1)
//                        ->order_by('p.id', 'desc')
                        ->order_by('p.sort', 'Asc')
                        ->limit($limit)
                        ->get()
                        ->result_array();

        $today = time();

        foreach ($products as $k => $p)
        {
            if(!$p['price'])
            {
                $attr = $this->ProductAttributes
                            ->find()
                            ->where('product_id', $p['id'])
                            ->where('(price > 0 OR sale_price > 0)')
                            ->order_by('sale_price', 'asc')
                            ->order_by('price', 'asc')
                            ->get()
                            ->row_array();

                $products[$k]['price']      = $attr['price'];
                $products[$k]['sale_price'] = $attr['sale_price'];
                $products[$k]['sale_start'] = $attr['sale_start'];
                $products[$k]['sale_end']   = $attr['sale_end'];
            }

            $on_sale = $products[$k]['sale_price'] ? 1 : 0;
            $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
            $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

            $products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
            unset($products[$k]['sale_start']);
            unset($products[$k]['sale_end']);

            $sec_img = $this->ProductImages
                            ->find()
                            ->where('product_id', $p['id'])
                            ->order_by('id', 'asc')
                            ->get()
                            ->row_array();

            $products[$k]['image']     = resize_img($products[$k]['image'], 100, 133);
            $products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], 100, 133) : $p['image'];
        }

        return $products;
    }

    public function upsell_products($id, $limit = 3)
    {
        return $this->upsell_crossell($id, 1, $limit);
    }

    public function crossell_products($id, $limit = 3)
    {
        return $this->upsell_crossell($id, 2, $limit);
    }

    private function upsell_crossell($id, $type, $limit = 3)
    {
        $where = is_array($id) ? 'r.product_id IN(' . implode(',', $id) . ')' : 'r.product_id = ' . $id;

        $products = $this->Products
                        ->setAlias('p')
                        ->find()
                        ->select('p.id, p.name, p.slug, p.image, p.price, p.sale_price, p.sale_start, p.sale_end')
                        ->join('product_related_products AS r', 'r.related_id = p.id')
                        ->where('r.related_type', $type)
                        ->where($where)
                        ->where('p.status', 1)
                        ->order_by('RAND()')
                        ->limit($limit)
                        ->get()
                        ->result_array();

        $today = time();

        foreach ($products as $k => $p)
        {
            if(!$p['price'])
            {
                $attr = $this->ProductAttributes
                            ->find()
                            ->where('product_id', $p['id'])
                            ->where('(price > 0 OR sale_price > 0)')
                            ->order_by('sale_price', 'asc')
                            ->order_by('price', 'asc')
                            ->get()
                            ->row_array();

                $products[$k]['price']      = $attr['price'];
                $products[$k]['sale_price'] = $attr['sale_price'];
                $products[$k]['sale_start'] = $attr['sale_start'];
                $products[$k]['sale_end']   = $attr['sale_end'];
            }

            $on_sale = $products[$k]['sale_price'] ? 1 : 0;
            $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
            $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

            $products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
            unset($products[$k]['sale_start']);
            unset($products[$k]['sale_end']);

            $sec_img = $this->ProductImages
                            ->find()
                            ->where('product_id', $p['id'])
                            ->order_by('id', 'asc')
                            ->get()
                            ->row_array();

            $products[$k]['image']     = resize_img($products[$k]['image'], 100, 133);
            $products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], 100, 133) : $p['image'];
        }

        return $products;
    }
}
