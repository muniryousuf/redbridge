<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_model extends My_Model
{
    const UPDATED_AT = NULL;
    const SOFT_DELETED = NULL;

    public $fillables = ["controller", "action", "role_id"];
}