<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_items_model extends My_Model {

	public $fillables = ["label","page_id","link","attributes","menu_id","parent_id","status","sort_order"];

	public function rules()
    {
    	$rules = array(
    		array(
				'field' => 'page_id',
				'label' => 'Page',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'label',
				'label' => 'Label',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'link',
				'label' => 'Link',
				'rules' => 'trim|callback_required_link',
				'errors' => array(
					'required_link' => 'The Custom link field is required.'
				)
			),
			array(
				'field' => 'menu_id',
				'label' => 'Menu Id',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|numeric'
			)
		);

		return $rules;
    }
}
