<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_categories_model extends My_Model {

public $fillables = ["name","slug","image","description","parent_id","meta_title","meta_desc","index_follow","sort"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|required|callback_validate_slug',
                'errors' => array(
                    'validate_slug' => 'Slug already taken.'
                )
            )
        );

        return $rules;
    }

    public function category_by_ids($ids, $img_width = 200, $img_height = 300)
    {
        $categories = $this->ProCategories
                            ->find()
                            ->select('id, name, slug, image')
                            ->where_in('id', $ids)
                            ->order_by('field(id, ' . implode(',', $ids) . ')')
                            ->get()
                            ->result_array();

        foreach ($categories as $k => $c)
        {
            $categories[$k]['image'] = $categories[$k]['image'] ? $categories[$k]['image'] : 'no-image.jpg';
            $categories[$k]['image'] = resize_img($categories[$k]['image'], $img_width, $img_height);
        }

        return $categories;
    }

   public function getAllCategories()
    {
        $firstLevel = $this->ProCategories
            ->find()
            ->select('id, name, slug, image,parent_id,sort')
            ->order_by('sort', 'Asc')
            ->where('parent_id', 0)
            ->get()
            ->result_array();
        $bunches = array();
        foreach ($firstLevel as $k => $v) {
            $bunch = array();
            $bunch['id'] = $v['id'];
            $bunch['name'] = $v['name'];
            $bunch['slug'] = $v['slug'];
            $bunch['image'] = $v['image'];

            $secondLevel = $this->ProCategories
                ->find()
                ->select('id, name, slug, image,parent_id')
                ->where('parent_id', $v['id'])
                ->order_by('sort', 'Asc')
                ->get()
                ->result_array();
            $child = array();
            if (count($secondLevel) > 0) {
                foreach ($secondLevel as $sl => $slv) {
                    $child['id'] = $slv['id'];
                    $child['name'] = $slv['name'];
                    $child['slug'] = $slv['slug'];
                    $child['image'] = $slv['image'];
                    $thirdLevel = $this->ProCategories
                        ->find()
                        ->select('id, name, slug, image,parent_id')
                        ->where('parent_id', $slv['id'])
                        ->order_by('sort', 'Asc')
                        ->get()
                        ->result_array();


                    if (count($thirdLevel) > 0) {
                        $third_bunch = array();
                        foreach ($thirdLevel as $tl => $tlv) {

                            $third_level['id'] = $tlv['id'];
                            $third_level['name'] = $tlv['name'];
                            $third_level['slug'] = $tlv['slug'];
                            $third_level['image'] = $tlv['image'];
                            $third_bunch[] = $third_level;
                        }
                        $child['sub_childs'] = $third_bunch;
                    }

                    $bunch['second_level'][] = $child;
                }

            }

            $bunches[] = $bunch;
        }
     return $bunches;
    }

    public function getCategoryProduct($name)
    {


        $categories = $this->ProCategories
            ->find()
            ->select('id, name, slug, image')
            ->where('slug', $name)
            ->get()
            ->result_array();



        $productAgainstCat = [];

        foreach ($categories as $value) {
            $productIDs = $this->ProductCategoriesMap->find()->where('category_id', $value['id'])->where('is_feature', 1)
                ->select('product_id')
                ->order_by('id', 'desc')
                ->limit(3)
                ->get()->result_array();

            if (count($productIDs) > 0) {
                $productIDs = $this->return_array($productIDs);
                $productAgainstCat[$value['name']] = $this->Products->products_by_ids($productIDs);
            }
        }
        return $productAgainstCat;
    }

    public function return_array($array){
        $return = array();

        foreach ($array as $v){
            $return[]= $v['product_id'];
        }

        return $return;
    }



    public function getCategoryBaner($array){


        $categories = $this->ProCategories
            ->find()
            ->select('id, name, slug, image')
            ->where_in('slug', $array)
            ->limit(6)
            ->get()
            ->result_array();
        return $categories;
    }

    public function getAllFeaturesProducts(){

        $productData = array();
        $productIDs = $this->ProductCategoriesMap->find()->where('is_feature', 1)
            ->select('product_id')
            ->order_by('id', 'desc')
            ->limit(12)
            ->get()->result_array();

       if(count($productIDs) > 0 ){
           $productIDs =  array_column($productIDs,'product_id');

           $productIDs =array_unique($productIDs);

           $productData = $this->Products->products_by_ids($productIDs);
       }


       return $productData;
    }


    public function getBestSeller(){

        $productData = array();
        $products = $this->Products
            ->find()
            ->select('id')
            ->where('status', 1)

            ->limit(12)
            ->get()

            ->result_array();



        if(count($products)>0){
            $productIDs =  array_column($products,'id');

            $productIDs =array_unique($productIDs);

            $productData = $this->Products->products_by_ids($productIDs);
        }



        return $productData;
    }





}
