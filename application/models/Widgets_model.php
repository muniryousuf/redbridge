<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Widgets_model extends My_Model {

	public $fillables = ["name","slug","image","content"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|required|callback_validate_slug',
				'errors' => array(
					'validate_slug' => 'Slug already taken.'
				)
			)
		);

		return $rules;
    }
}
