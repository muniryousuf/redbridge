<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Seo_urls_model extends My_Model {

	const SOFT_DELETED = NULL;

	public $fillables = ["slug","tbl_name","row_id"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'tbl_name',
				'label' => 'Tbl Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'row_id',
				'label' => 'Row Id',
				'rules' => 'trim|required|numeric'
			)
		);

		return $rules;
    }
}
