<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_assigned_products_model extends My_Model {

    public $fillables = ["discount_id","product_id"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'discount_id',
                'label' => 'Discount',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'product_id',
                'label' => 'Product',
                'rules' => 'trim|required'
            )
        );

        return $rules;
    }
}
