<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_related_products_model extends My_Model {

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    const SOFT_DELETED = NULL;

    public $fillables = ["product_id","related_id","related_type"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'product_id',
                'label' => 'Product',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'related_id',
                'label' => 'Related Product',
                'rules' => 'trim|required'
            )
        );

        return $rules;
    }
}
