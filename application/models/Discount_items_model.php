<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_items_model extends My_Model {

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    const SOFT_DELETED = NULL;
    
    public $fillables = ["quantity","amount","discount_id","is_default"];

    public function rules()
    {
        $rules = array(
             array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'trim|required|numeric'
            ),
            array(
                'field' => 'discount_id',
                'label' => 'Attribute',
                'rules' => 'trim|required'
            )
        );

        return $rules;
    }
}
