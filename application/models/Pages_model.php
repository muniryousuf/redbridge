<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_model extends My_Model {

	public $fillables = ["title","content","template","meta_title","meta_desc","index_follow","status"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'slug',
				'label' => 'Slug',
				'rules' => 'trim|required|callback_validate_slug',
				'errors' => array(
					'validate_slug' => 'Slug already taken.'
				)
			)
		);

		return $rules;
    }
}
