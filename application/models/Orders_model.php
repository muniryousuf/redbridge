<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends My_Model {

	public $fillables = ["customer_id","email","phone","status","payment_method","payment_id","sub_total","shipping","tax","discount","net_total","comments","invoice_file","packing_slip_file","custom_date"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'payment_method',
				'label' => 'Payment Method',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'sub_total',
				'label' => 'Sub Total',
				'rules' => 'trim|required'
			)
		);

		return $rules;
    }
}
