<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_settings_model extends My_Model
{
    const SOFT_DELETED = NULL;

    public $fillables = ['site_title', 'tag_line', 'site_name', 'frontpage', 'allow_index', 'header_logo', 'footer_logo', 'favicon', 'copyrights', 'address', 'phone', 'email', 'social_links', 'before_head', 'after_body', 'before_body'];
}