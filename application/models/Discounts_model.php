<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discounts_model extends My_Model {

    public $fillables = ["name","message","status"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|callback_validate_name',
                'errors' => array(
                    'validate_name' => 'Discount with same name already exists.'
                )
            ),
            array(
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'trim|required'
            )
        );

        return $rules;
    }
}
