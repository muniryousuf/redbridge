<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Product_categories_map_model', 'ProductCategories');
        $this->load->model('Product_attributes_model', 'Attributes');
        $this->load->model('Product_attributes_vals_model', 'AttributesValues');
        $this->load->model('Discount_assigned_products_model', 'DiscountAssignedProducts');
    }

    public function index()
    {
        $product = $this->Products
                    ->find()
                    ->where('slug', $this->uri->segment(2))
                    ->order_by('sort', 'Asc')
                    ->get()
                    ->row_array();

        if($product)
        {
            $today = time();
            $this->seo = generate_meta($product);

            $images = $this->ProductImages
                            ->find()
                            ->where('product_id', $product['id'])
                            ->order_by('id', 'asc')
                            ->get()
                            ->result_array();

            $categories = $this->ProductCategories
                            ->setAlias('pc')
                            ->find()
                            ->select('c.id, c.name, c.slug')
                            ->join('product_categories AS c', 'c.id = pc.category_id')
                            ->where('pc.product_id', $product['id'])
                            ->order_by('pc.id', 'asc')
                            ->get()
                            ->result_array();

            $pro_attrs = $this->ProductAttributes
                            ->find()
                            ->where('product_id', $product['id'])
                            ->get()
                            ->result_array();



            $pro_discs = $this->DiscountAssignedProducts
                            ->setAlias('t1')
                            ->find()
                            ->join("discounts AS t2", "t1.discount_id = t2.id")
                            ->join("discount_items AS t3", "t2.id = t3.discount_id")
                            ->where('t2.status', "Activated")
                            ->where('t2.deleted', "0")
                            ->where('t1.product_id', $product['id'])
                            ->get()
                            ->result_array();


            if(count($pro_attrs) > 0 )
            {

                $attribute_vals = [];
                foreach ($pro_attrs as $pa)
                {
                    $attribute_vals = array_merge($attribute_vals, explode(',', $pa['attributes']));
                }

                $attribute_vals = array_unique($attribute_vals);

                $tmp = $this->AttributesValues
                            ->reset()
                            ->setAlias('av')
                            ->find()
                            ->select('a.id, av.id AS av_id, a.name, av.name AS av_name')
                            ->join('product_attributes AS a', 'a.id = av.attribute_id')
                            ->where_in('av.id', $attribute_vals)
                            ->get()
                            ->result_array();

                $attributes = $attribute_vals = $attribute_cats = [];
                foreach ($tmp as $avt)
                {
                    $attribute_cats[$avt['av_id']] = $avt['id'];

                    if(isset($attribute_vals[$avt['id']]))
                    {
                        $attribute_vals[$avt['id']]['childs'][$avt['av_id']] = $avt['av_name'];
                    }
                    else
                    {
                        $attribute_vals[$avt['id']] = [
                            'id'  => $avt['id'],
                            'name' => $avt['name'],
                            'childs' => [
                                $avt['av_id'] => $avt['av_name']
                            ]
                        ];
                    }
                }

                foreach ($pro_attrs as $pa)
                {
                    $attr = [];
                    foreach (explode(',', $pa['attributes']) as $a)
                    {
                        $attr[$attribute_cats[$a]] = $a;
                    }

                    $on_sale = $pa['sale_price'] ? 1 : 0;
                    $on_sale = $pa['sale_start'] && (strtotime($pa['sale_start']) > $today) ? 0 : $on_sale;
                    $on_sale = $pa['sale_end'] && (strtotime($pa['sale_end']) < $today) ? 0 : $on_sale;

                    $attributes[] = [
                        'attributes'          => $attr,
                        'stock'               => $pa['stock'] != '' ? (int) $pa['stock'] : 999,
                        'price'               => $pa['price'],
                        'price_formated'      => format_money($pa['price']),
                        'sale_price'          => $on_sale ? $pa['sale_price'] : '',
                        'sale_price_formated' => $on_sale ? format_money($pa['sale_price']) : '',
                    ];
                }
            }


            if(!$product['price'] && count($pro_attrs) > 0 )
            {
                $product['stock']      = $attributes[0]['stock'];
                $product['price']      = $attributes[0]['price'];
                $product['sale_price'] = $attributes[0]['sale_price'];
            }


            $cids = [];
            foreach ($categories as $c)
            {
                $cids[] = $c['id'];
            }

            $related_products = $this->Products->related_products($product['id'], $cids);
            $upsell_products  = $this->Products->upsell_products($product['id']);

            if(isset($attribute_vals)){
                $this->load->front('product', compact('product', 'categories', 'images', 'attributes', 'attribute_vals', 'related_products', 'upsell_products','pro_discs'));
            }else {
                $this->load->front('product', compact('product', 'categories', 'images',  'related_products', 'upsell_products','pro_discs'));
            }

        }
        else
        {
            show_404();
        }
    }
}
