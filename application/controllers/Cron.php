<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

    public function publish_blogs()
    {
        $blogs = $this->Blogs
                    ->find()
                    ->where('status', 2)
                    ->where('publish_date <= "' . date('Y-m-d H:i:s') . '"')
                    ->limit(10)
                    ->get()
                    ->result_array();

        foreach ($blogs as $blog)
        {
            $this->Blogs->update(['status' => 1], $blog['id'], false);
        }

        echo count($blogs) . ' blogs published.';
    }

    public function restock_alerts()
    {
        $this->load->model('Restock_alerts_model', 'RestockAlerts');
        $this->load->model('Product_attributes_model', 'Attributes');

        $rows = $this->RestockAlerts
                    ->find()
                    ->where('status', 2)
                    ->limit(10)
                    ->get()
                    ->result_array();

        foreach($rows as $row)
        {
            $product = $this->Products
                            ->find()
                            ->where('id', $row['product_id'])
                            ->get()
                            ->row_array();

            $product_name = $product['name'];

            // $attributes = [];

            // if($row['attributes'])
            // {
            //     $tmp =  $this->Attributes
            //                 ->find()
            //                 ->where_in('id', explode(',', $row['attributes']))
            //                 ->get()
            //                 ->result_array();

            //     foreach ($tmp as $t)
            //     {
            //         $attributes[] = $t['name'];
            //     }

            //     $product_name .= ' ' . implode(' ', $attributes);
            // }

            $title = 'Product ' . $product_name . ' heeft weer op voorraad';

            $email_body = $this->load->front_email('restock_alert', compact('title', 'product', 'product_name'));

            $this->load->library('email');

            $this->email->from(NO_REPLY_EMAIL, site_name());
            $this->email->to($row['email']);
            $this->email->subject($title);
            $this->email->message($email_body);
            $this->email->send();

            $this->RestockAlerts->update(['status' => 1], $row['id'], false);
        }

        echo count($rows) . ' alerts sent.';
    }
}