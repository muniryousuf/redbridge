<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductTag extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Product_categories_model', 'Categories');
    }

    public function index()
    {
        $tag = str_replace('-', ' ', $this->uri->segment(2));

        $this->seo['title'] = $tag . ' Archieven - ' . $this->vars['site']['site_title'];

        $products = $this->Products
                        ->find()
                        ->select('id, name, slug, image, price, sale_price, sale_start, sale_end')
                        ->where('FIND_IN_SET("'.$tag.'", tags)')
                        ->where('status', 1)
                        ->order_by('id', 'desc')
                        ->get()
                        ->result_array();

        $today = time();

        foreach ($products as $k => $p)
        {
            if(!$p['price'])
            {
                $attr = $this->ProductAttributes
                            ->find()
                            ->where('product_id', $p['id'])
                            ->where('(price > 0 OR sale_price > 0)')
                            ->order_by('sale_price', 'asc')
                            ->order_by('price', 'asc')
                            ->get()
                            ->row_array();
                            
                $products[$k]['price']      = $attr['price'];
                $products[$k]['sale_price'] = $attr['sale_price'];
                $products[$k]['sale_start'] = $attr['sale_start'];
                $products[$k]['sale_end']   = $attr['sale_end'];
            }

            $on_sale = $products[$k]['sale_price'] ? 1 : 0;
            $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
            $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

            $products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
            unset($products[$k]['sale_start']);
            unset($products[$k]['sale_end']);

            $sec_img = $this->ProductImages
                            ->find()
                            ->where('product_id', $p['id'])
                            ->order_by('id', 'asc')
                            ->get()
                            ->row_array();

            $products[$k]['image']     = resize_img($products[$k]['image'], 300, 400);
            $products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], 300, 400) : $p['image'];
        }

        $this->load->front('product-tag', compact('tag', 'products'));
    }
}