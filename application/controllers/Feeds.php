<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds extends MY_Controller {

	public function sitemap()
	{
		header('Content-type: text/xml');

		$this->load->model('Blogs_model', 'Blogs');
        $this->load->model('Blog_categories_model', 'BlogCategories');
        $this->load->model('Products_model', 'Products');
        $this->load->model('Product_categories_model', 'ProductCategories');

        $data['homepage'] = $this->Pages
								->find()
								->select('updated_at')
			                    ->where('id', $this->vars['site']['frontpage'])
			                    ->get()
			                    ->row_array();

		$data['pages'] = $this->Pages
							->setAlias('p')
							->find()
							->select('u.slug, p.updated_at')
							->join('seo_urls AS u', 'u.row_id = p.id AND u.tbl_name = "pages"')
		                    ->where('p.status', 1)
		                    ->where('p.id != ' . $this->vars['site']['frontpage'])
		                    ->get()
		                    ->result_array();

		$data['blog_cats'] = $this->BlogCategories
								->find()
								->select('slug, updated_at')
			                    ->get()
			                    ->result_array();

        $data['blogs'] = $this->Blogs
							->setAlias('b')
							->find()
							->select('u.slug, b.updated_at')
							->join('seo_urls AS u', 'u.row_id = b.id AND u.tbl_name = "blogs"')
		                    ->where('b.status', 1)
		                    ->get()
		                    ->result_array();

		$data['product_cats'] = $this->ProductCategories
									->find()
									->select('slug, updated_at')
				                    ->get()
				                    ->result_array();

		$data['products'] = $this->Products
								->find()
								->select('slug, tags, updated_at')
			                    ->get()
			                    ->result_array();

		$this->load->view('feeds/sitemap', $data);
	}

	public function fb()
	{
		header('Content-type: text/xml');

        $data['products'] = $this->feed_products('fb');

		$this->load->view('feeds/fb', $data);
	}

	public function google()
	{
		header('Content-type: text/xml');

        $data['products'] = $this->feed_products('google');

		$this->load->view('feeds/google', $data);
	}

	private function feed_products($type = '')
	{
		$this->load->model('Products_model', 'Products');
		$this->load->model('Product_attributes_vals_model', 'Attributes');

		$today = time();
		$attributes = $products = $gtm = [];

		if(!empty($this->vars[$type . '_utm_source']))
		{
			$gtm = [
				'utm_source=' . urlencode($this->vars[$type . '_utm_source'])
			];

			if($this->vars[$type . '_utm_campaign'])
			{
				$gtm[] = 'utm_campaign=' . urlencode($this->vars[$type . '_utm_campaign']);
			}

			if($this->vars[$type . '_utm_medium'])
			{
				$gtm[] = 'utm_medium=' . urlencode($this->vars[$type . '_utm_medium']);
			}

			if($this->vars[$type . '_utm_content'])
			{
				$gtm[] = 'utm_content=' . urlencode($this->vars[$type . '_utm_content']);
			}
		}

		$attributes_tmp = $this->Attributes
								->find()
								->get()
								->result_array();

		foreach ($attributes_tmp as $a)
		{
			$attributes[$a['id']] = $a['name'];
		}

		$products = $this->Products
                            ->setAlias('p')
                            ->find()
                            ->select('p.id, a.id AS aid, p.name, p.slug, p.description, p.image, a.attributes, p.stock, p.price, p.sale_price, p.sale_start, p.sale_end, a.stock AS a_stock, a.price AS a_price, a.sale_price AS a_sale_price, a.sale_start AS a_sale_start, a.sale_end AS a_sale_end')
                            ->join('product_attributes_map AS a', 'a.product_id = p.id')
                            ->where('p.status', 1)
                            ->order_by('p.sort', 'Asc')
                            //->order_by('p.id', 'desc')
                            ->get()
                            ->result_array();

        foreach ($products as $k => $p)
        {
        	$name 		= $p['name'];
        	$url  		= url($p['slug']);
        	$url_params = [];

        	if($p['attributes'])
        	{
        		$url_params[] = 'attribute=' . $p['aid'];

        		foreach (explode(',', $p['attributes']) as $at)
        		{
        			$name .= ' ' . $attributes[$at];
        		}
        	}

        	if($gtm)
        	{
        		$url_params = array_merge($url_params, $gtm);
        		$url_params[] = 'utm_term=' . ($p['aid'] ? $p['aid'] : $p['id']);
        	}

        	if($url_params)
        	{
        		$url .= "/?" . implode('&amp;', $url_params);
        	}

        	$products[$k]['name'] 		 = $name;
        	$products[$k]['url'] 		 = $url;
        	$products[$k]['description'] = htmlentities(str_replace(["\n", "\r"], ' ', strip_tags($p['description'])));

            if(!$p['price'])
            {
            	$products[$k]['stock']      = $p['a_stock'];
                $products[$k]['price']      = $p['a_price'];
                $products[$k]['sale_price'] = $p['a_sale_price'];
                $products[$k]['sale_start'] = $p['a_sale_start'];
                $products[$k]['sale_end']   = $p['a_sale_end'];
            }

            $on_sale = $products[$k]['sale_price'] ? 1 : 0;
            $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
            $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

            $products[$k]['price'] = $on_sale ? $products[$k]['sale_price'] : $products[$k]['price'];
            $products[$k]['stock'] = $products[$k]['stock'] == '' ? 1 : (int) $products[$k]['stock'];
            $products[$k]['image'] = resize_img($products[$k]['image']);



            unset($products[$k]['attributes']);
            unset($products[$k]['sale_price']);
            unset($products[$k]['sale_start']);
            unset($products[$k]['sale_end']);
            unset($products[$k]['a_stock']);
            unset($products[$k]['a_price']);
            unset($products[$k]['a_sale_price']);
            unset($products[$k]['a_sale_start']);
            unset($products[$k]['a_sale_end']);
        }

        return $products;
	}
}
