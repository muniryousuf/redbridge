<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

    public function index()
    {
        $page = '';
        $url  = $this->SeoUrl
                    ->find()
                    ->where('slug', $this->uri->segment(1))
                    ->where_in('tbl_name', ['blogs', 'pages'])
                    ->get()
                    ->row_array();

        if($url)
        {
            if($url['tbl_name'] == 'pages')
            {
                $page = $this->Pages
                        ->find()
                        ->where('id', $url['row_id'])
                        ->where('status', 1)
                        ->get()
                        ->row_array();
            }
            elseif($url['tbl_name'] == 'blogs')
            {
                $page = $this->Blogs
                        ->find()
                        ->where('id', $url['row_id'])
                        ->where('status', 1)
                        ->get()
                        ->row_array();
            }
        }

        if($page)
        {
            $this->seo = generate_meta($page);

            $view = isset($page['template']) && $page['template'] ? $page['template'] : 'page';

            if($view == 'cart')
            {
                $cart = $this->getCart();
                // echo "<pre>";
                // print_r($cart);
                $pids = [];
                if($cart['items'])
                {
                    foreach ($cart['items'] as $c)
                    {
                        $pids[$c['product']] = $c['product'];
                    }
                }

                $crossell_products = $pids ? $this->Products->crossell_products($pids) : [];

                $this->load->front('cart', compact('page', 'cart', 'crossell_products'));
            }
            elseif($view == 'checkout')
            {
                $cart = $this->getCart();

                if(!$cart['items'])
                {
                    redirect(url('/' . $this->vars['cart_url']));
                }

                $this->js = [
                    assets('js/jquery.validate.min.js'),
                    assets('js/additional-methods.min.js'),
                ];

                $this->load->front('checkout', compact('page', 'cart'));
            }
            elseif($view == 'shop')
            {
                $search = $this->input->get('s');
                $where  = $search ? "p.name LIKE '%{$search}%'" : '1=1';

                $products = $this->Products
                                ->setAlias('p')
                                ->find()
                                ->select('p.id, p.name, p.slug, p.image, p.price, p.sale_price, p.sale_start, p.sale_end')
                                ->join('product_categories_map AS c', 'c.product_id = p.id')
                                ->where('p.status', 1)
                                ->where($where)
//                                ->order_by('p.id', 'desc')
                                ->order_by('p.sort', 'Asc')
                                ->get()
                                ->result_array();

                $today = time();

                foreach ($products as $k => $p)
                {
                    if(!$p['price'])
                    {
                        $attr = $this->ProductAttributes
                                    ->find()
                                    ->where('product_id', $p['id'])
                                    ->where('(price > 0 OR sale_price > 0)')
                                    ->order_by('sale_price', 'asc')
                                    ->order_by('price', 'asc')
                                    ->get()
                                    ->row_array();

                        $products[$k]['price']      = $attr['price'];
                        $products[$k]['sale_price'] = $attr['sale_price'];
                        $products[$k]['sale_start'] = $attr['sale_start'];
                        $products[$k]['sale_end']   = $attr['sale_end'];
                    }

                    $on_sale = $products[$k]['sale_price'] ? 1 : 0;
                    $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
                    $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

                    $products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
                    unset($products[$k]['sale_start']);
                    unset($products[$k]['sale_end']);

                    $sec_img = $this->ProductImages
                                    ->find()
                                    ->where('product_id', $p['id'])
                                    ->order_by('id', 'asc')
                                    ->get()
                                    ->row_array();

                    $products[$k]['image']     = resize_img($products[$k]['image'], 300, 400);
                    $products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], 300, 400) : $p['image'];
                }

                $this->load->front('shop', compact('page', 'products', 'search'));
            }
            elseif($view == 'account')
            {
                $inputs = $this->input->post();
                $orders = $order = $billing = $shipping = [];

                if(isset($_GET['reset_password']) && $_GET['reset_password'] == 1)
                {
                    $email = $this->input->get('email');
                    $code  = $this->input->get('code');
                    $invalid = true;

                    if($email && $code)
                    {
                        $user = $this->Customers
                                    ->find()
                                    ->where('email', $email)
                                    ->where('reset_key', $code)
                                    ->get()
                                    ->row_array();

                        if($user)
                        {
                            $invalid = false;
                        }
                    }

                    if($invalid)
                    {
                        $this->session->set_flashdata('error', 'Kan uw verzoek niet verwerken, link om wachtwoord opnieuw in te stellen is ongeldig of verlopen.');
                        redirect(account_url());
                    }
                }
                else
                {
                    if(isset($_GET['orders']))
                    {
                        $this->load->model('Orders_model', 'Orders');

                        $orders = $this->Orders
                                        ->find()
                                        ->where('customer_id', $this->customer['id'])
                                        ->get()
                                        ->result_array();
                    }
                    if(isset($_GET['view-order']))
                    {
                        $this->load->model('Orders_model', 'Orders');
                        $this->load->model('Order_address_model', 'OrderAddress');
                        $this->load->model('Order_products_model', 'OrderProducts');

                        $id         = (int) $this->input->get('id');
                        $reference  = $this->input->get('reference');
                        $newOrders  = $this->newOrders;
                        $id         = ($reference)?((int)($newOrders[$reference]??"0")):$id;

                        $order = '';
                        if ($reference)
                        {
                            $order = $this->Orders
                                        ->find()
                                        ->where('id', $id)
                                        ->get()
                                        ->row_array();
                            $page['title'] = "Order Details";
                        }
                        else
                        {
                            $order = $this->Orders
                                        ->find()
                                        ->where('id', $id)
                                        ->where('customer_id', $this->customer['id'])
                                        ->get()
                                        ->row_array();
                        }

                        if(!$order && $reference)
                        {
                            redirect(neworders_url());

                        }
                        elseif (!$order && !$reference)
                        {
                            redirect(account_url() . '?orders');
                        }

                        $order['address'] = $this->OrderAddress
                                                ->find()
                                                ->where('order_id', $id)
                                                ->get()
                                                ->result_array();

                        $order['products'] = $this->OrderProducts
                                                ->find()
                                                ->where('order_id', $id)
                                                ->get()
                                                ->result_array();
                    }
                    elseif(isset($_GET['address']))
                    {
                        $this->load->model('Customer_address_model', 'CustomerAddress');

                        $billing = $this->CustomerAddress
                                        ->find()
                                        ->where('customer_id', $this->customer['id'])
                                        ->where('type', 1)
                                        ->get()
                                        ->row_array();

                        $shipping = $this->CustomerAddress
                                        ->find()
                                        ->where('customer_id', $this->customer['id'])
                                        ->where('type', 2)
                                        ->get()
                                        ->row_array();
                    }

                    if($this->input->post('action'))
                    {
                        if($this->input->post('action') == 'address')
                        {
                            $this->form_validation->set_rules('billing[first_name]', 'Factuuradres Voornaam', 'required');
                            $this->form_validation->set_rules('billing[last_name]', 'Factuuradres Achternaam', 'required');
                            $this->form_validation->set_rules('billing[address1]', 'Factuuradres Straat en huisnummer', 'required');
                            $this->form_validation->set_rules('billing[postalcode]', 'Factuuradres Postcode', 'required');
                            $this->form_validation->set_rules('billing[city]', 'Factuuradres Plaats', 'required');

                            if ($this->form_validation->run())
                            {
                                $inputs['billing']['country'] = $inputs['shipping']['country'] = 'Pakistan';

                                if($billing)
                                {
                                    $this->CustomerAddress->update($inputs['billing'], $billing['id'], false);
                                }
                                else
                                {
                                    $inputs['billing']['type']        = 1;
                                    $inputs['billing']['customer_id'] = $this->customer['id'];
                                    $this->CustomerAddress->insert($inputs['billing'], false);
                                }

                                if($shipping)
                                {
                                    $this->CustomerAddress->update($inputs['shipping'], $shipping['id'], false);
                                }
                                else
                                {
                                    $inputs['shipping']['type']        = 2;
                                    $inputs['shipping']['customer_id'] = $this->customer['id'];
                                    $this->CustomerAddress->insert($inputs['shipping'], false);
                                }

                                $this->session->set_flashdata('success', 'Adres succesvol bijgewerkt.');
                                redirect(account_url() . '?address');
                            }
                            else
                            {
                                $this->session->set_flashdata('error', validation_errors());
                            }
                        }
                        elseif($this->input->post('action') == 'account')
                        {
                            $this->form_validation->set_rules('first_name', 'Voornaam', 'trim|required');
                            $this->form_validation->set_rules('last_name', 'Achternaam', 'trim|required');
                            $this->form_validation->set_rules('email', 'E-mailadres', 'trim|required|valid_email|callback_validate_unique_email');
                            $this->form_validation->set_message('validate_unique_email', 'Er is al een account geregistreerd met je e-mailadres.');

                            if($inputs['old_password'] || $inputs['password'] || $inputs['repassword'])
                            {
                                $this->form_validation->set_rules('old_password', 'Huidig wachtwoord', 'trim|required');
                                $this->form_validation->set_rules('password', 'Nieuw wachtwoord', 'trim|required');
                                $this->form_validation->set_rules('repassword', 'Bevestig nieuw wachtwoord', 'trim|required|matches[password]');
                            }

                            if ($this->form_validation->run())
                            {
                                if($this->customer['password'] == md5($inputs['old_password']))
                                {
                                    $this->Customers->update([
                                        'first_name' => $inputs['first_name'],
                                        'last_name'  => $inputs['last_name'],
                                        'email'      => $inputs['email'],
                                        'password'   => md5($inputs['password']),
                                    ], $this->customer['id'], false);

                                    $this->session->set_flashdata('success', 'Profiel succesvol bijgewerkt.');
                                    redirect(account_url() . '?account');
                                }
                                else
                                {
                                    $this->session->set_flashdata('error', 'Oud wachtwoord is niet correct.');
                                }
                            }
                            else
                            {
                                $this->session->set_flashdata('error', validation_errors());
                            }
                        }
                    }
                }

                $this->js = [
                    assets('js/jquery.validate.min.js'),
                    assets('js/additional-methods.min.js'),
                    assets('js/account.js'),
                ];

                $this->load->front($view, compact('page', 'inputs', 'orders', 'order', 'billing', 'shipping'));
            }
            elseif($view == 'wishlist')
            {
                $products = [];

                if($this->wishlist)
                {
                    $products = $this->Products->products_by_ids($this->wishlist);
                    $products = $products ? $products : [];
                }

                $this->load->front('wishlist', compact('page', 'products'));
            }
            elseif($view == 'neworders')
            {
                $newOrders = $this->newOrders;
                $orderIds  = [];
                $orders    = "";

                foreach ($newOrders as $idor => $id)
                {
                    $orderIds[]  = $id;
                }

                if (!empty($orderIds))
                {

                    $this->load->model('Orders_model', 'Orders');

                    $orders = $this->Orders
                                    ->find()
                                    ->where_in('id', $orderIds)
                                    ->get()
                                    ->result_array();
                }
                $this->load->front('neworders', compact('page', 'newOrders','orderIds','orders'));
            }
            elseif($view == 'blog-posts')
            {
                $this->blog_posts($page);
            }
            else
            {
                if($url['tbl_name'] == 'blogs')
                {
                    $this->view_blog($page);
                }
                else
                {
                    $this->load->front($view, compact('page'));
                }
            }
        }
        else
        {
            show_404();
        }
    }

    private function blog_posts($page)
    {
        $slug  = $this->uri->segment(1);
        $pno   = (int) $this->uri->segment(2);
        $pno   = $pno ? $pno : 1;
        $limit = 10;
        $start = ($pno - 1) * $limit;

        $model = $this->Blogs;

        $model->find()
            ->select('id')
            ->where('status', 1);

        $total = $model->count();

        $blogs = $model
                    ->setAlias('b')
                    ->find()
                    ->select('b.title, b.content, b.image, b.publish_date, s.slug, u.first_name, u.last_name, c.name AS category, c.slug AS category_slug')
                    ->join('seo_urls AS s', 'b.id = s.row_id AND s.tbl_name = "blogs"')
                    ->join('users AS u', 'u.id = b.author_id')
                    ->join('blogs_categories_map AS bc', 'bc.blog_id = b.id', 'left')
                    ->join('blog_categories AS c', 'bc.category_id = c.id', 'left')
                    ->where('b.status', 1)
                    ->limit($limit, $start)
                    ->order_by('b.id', 'DESC')
                    ->group_by('b.id')
                    ->get()
                    ->result_array();

        $this->load->library('pagination');

        $config['base_url']   = url($slug);
        $config['total_rows'] = $total;
        $config['per_page']   = $limit;

        $config['use_page_numbers']   = TRUE;
        $config['reuse_query_string'] = TRUE;

        $config['full_tag_open']  = '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul>';

        $config['cur_tag_open']   = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']  = '<span class="sr-only">(current)</span></span></li>';

        $config['num_tag_open']   = '<li class="page-item">';
        $config['next_tag_open']  = '<li class="page-item">';
        $config['prev_tag_open']  = '<li class="page-item">';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['last_tag_open']  = '<li class="page-item">';
        $config['num_tag_close']  = '</li>';

        $config['next_link']  = 'Next';
        $config['prev_link']  = 'Previous';
        $config['first_link'] = 'First';
        $config['last_link']  = 'Last';
        $config['attributes'] = array('class' => 'page-link');

        $this->pagination->initialize($config);

        $this->load->front('blog-posts', compact('page', 'blogs'));
    }

    private function view_blog($blog)
    {
        $this->load->model('Blogs_categories_map_model', 'BlogCategory');

        $author = $this->Users
                        ->find()
                        ->where('id', $blog['author_id'])
                        ->get()
                        ->row_array();

        $category = $this->BlogCategory
                        ->setAlias('bc')
                        ->find()
                        ->select('c.name, c.slug')
                        ->join('blog_categories AS c', 'c.id = bc.category_id')
                        ->where('bc.blog_id', $blog['id'])
                        ->get()
                        ->row_array();

        $latest = $this->Blogs
                        ->setAlias('b')
                        ->find()
                        ->select('b.title, b.publish_date, u.slug')
                        ->join('seo_urls AS u', 'b.id = u.row_id AND u.tbl_name = "blogs"')
                        ->where('b.status', 1)
                        ->limit(2)
                        ->order_by('b.id', 'DESC')
                        ->get()
                        ->result_array();

        $next = $this->Blogs
                        ->setAlias('b')
                        ->find()
                        ->select('b.title, u.slug')
                        ->join('seo_urls AS u', 'b.id = u.row_id AND u.tbl_name = "blogs"')
                        ->where('b.status', 1)
                        ->where('b.id > ' . $blog['id'])
                        ->order_by('b.id ASC')
                        ->get()
                        ->row_array();

        $prev = $this->Blogs
                        ->setAlias('b')
                        ->find()
                        ->select('b.title, u.slug')
                        ->join('seo_urls AS u', 'b.id = u.row_id AND u.tbl_name = "blogs"')
                        ->where('b.status', 1)
                        ->where('b.id < ' . $blog['id'])
                        ->order_by('b.id DESC')
                        ->get()
                        ->row_array();

        $pids = $this->Products
                        ->find()
                        ->select('id')
                        ->where('status', 1)
                        ->limit(3)
                        ->order_by('id', 'DESC')
                        ->get()
                        ->result_array();

        foreach ($pids as $k => $v)
        {
            $pids[$k] = $v['id'];
        }

        $products = $this->Products->products_by_ids($pids, 60, 80);

        $this->load->front('blog', compact('blog', 'author', 'category', 'latest', 'next', 'prev', 'products'));
    }

    public function validate_unique_email($email)
    {
        $user = $this->Customers
                    ->find()
                    ->where('email', $email)
                    ->where_not_in('id', $this->customer['id']);

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }
}
