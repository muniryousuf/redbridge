<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//sumair asim
class Cart extends MY_Controller {

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $inputs = $this->input->post();
            $cart   = $this->session->userdata('cart');

            $cart   = $cart ? $cart : [];

            $id             = $inputs['product'];
            $qty            = (int) $inputs['quantity'];
            $qty            = $qty ? $qty : 1;
            $discountItemId = $inputs['QuantityDiscountItem']??NULL;
            $product = $this->Products
                            ->find()
                            ->where('id', $id)
                            ->get()->row_array();


            if($product)
            {
                $key = $id;

                if(isset($inputs['attribute']))
                {
                    $key .= '-' . implode(',', $inputs['attribute']);
                }


                $key = md5($key);
                if($discountItemId){
                    $key =  $key.''.$discountItemId;
                }

                if(isset($cart[$key]))
                {
                    $cart[$key]['quantity'] = $cart[$key]['quantity'] + $qty;
                }
                else
                {
                    $cart[$key] = [
                        'product'   => $id,
                        'attribute' => !isset($inputs['attribute']) && empty($inputs['attribute']) ? [] : $inputs['attribute'],
                        'quantity'  => $qty,
                        'discountItemId'  => $discountItemId
                    ];
                }



                $this->session->set_userdata('cart', $cart);
                $this->session->set_flashdata('success', '"' . $product['name'] . '" has been added to your shopping cart.');
            }

            redirect(url('/' . $this->vars['cart_url']));
        }

        redirect(url('/'));
    }

    public function update()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $inputs = $this->input->post();
            $cart   = $this->session->userdata('cart');
            $cart   = $cart ? $cart : [];

            foreach ($inputs['quantity'] as $key => $qty)
            {
                if(isset($cart[$key]))
                {
                    $cart[$key]['quantity'] = $qty;
                }
            }

            $this->session->set_userdata('cart', $cart);
            $this->session->set_flashdata('success', 'Shopping cart updated.');
        }

        redirect(url('/' . $this->vars['cart_url']));
    }

    public function remove()
    {
        $key  = $this->input->get('key');
        $cart = $this->session->userdata('cart');

        if($key && isset($cart[$key]))
        {
            $item = $cart[$key];

            $product = $this->Products
                            ->find()
                            ->where('id', $item['product'])
                            ->get()
                            ->row_array();

            unset($cart[$key]);

            $this->session->set_flashdata('success', '"' . $product['name'] . '" removed.');
            $this->session->set_userdata('cart', $cart);
        }

        redirect(url('/' . $this->vars['cart_url']));
    }
}
