<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitesettings extends Admin_Controller {

	public function index()
	{
        $this->load->model('Pages_model', 'Pages');
		$this->pageTitle = 'General Settings';

        $data['settings'] = $this->SiteSettings->find()
                                ->where('id', 1)
                                ->get()
                                ->row_array();

        if($data['settings'])
        {
            $data['settings']['social_links'] = $data['settings']['social_links'] ? json_decode($data['settings']['social_links'], 1) : '';
        }

        $data['social'] = array('facebook', 'instagram');
        $data['pages']  = $this->Pages
                            ->find()
                            ->where('status', 1)
                            ->get()
                            ->result_array();

        $data['vars_key'] = [
            'cookie_text'
        ];

        $data['vars_val'] = [];
        foreach ($data['vars_key'] as $vk)
        {
            $data['vars_val'][$vk] = isset($this->vars[$vk]) ? $this->vars[$vk] : '';
        }

        $this->load->admin('site_settings/index', $data);
	}

	public function store()
	{
        $inputs = $this->input->post();
        $inputs['social_links'] = json_encode($inputs['social_links']);

        $row = $this->SiteSettings->find()
                ->where('id', 1)
                ->get()
                ->row_array();

        if($row)
        {
            $this->SiteSettings->update($inputs, $row['id']);
        }
        else
        {
            $inputs['id'] = 1;
            $this->SiteSettings->insert($inputs);
        }

        if(isset($inputs['vars']))
        {
            foreach ($inputs['vars'] as $k => $v)
            {
                $row = $this->SiteVars
                        ->find()
                        ->where('key', $k)
                        ->get()
                        ->row_array();

                if($row)
                {
                    $this->SiteVars->update(['value' => $v], $row['id'], false);
                }
                else
                {
                    $this->SiteVars->insert([
                        'key'   => $k,
                        'value' => $v
                    ], false);
                }
            }    
        }
        
		$this->session->set_flashdata('success', 'Settings updated successfully.');
		redirect(admin_url('sitesettings'));
	}
}