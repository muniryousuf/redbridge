<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discounts extends Admin_Controller {

    private $dataTableColumns = ["id","name","status","created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('discounts_model', 'discounts');
        $this->load->model('discount_items_model', 'discount_items');
        $this->pageTitle = 'Quantity Discounts';
    }

    public function index()
    {
        $this->load->admin('discounts/index');
    }

    public function create()
    {
        $row = $this->_save();

        $this->load->admin('discounts/form', compact('row'));
    }

    public function assign_products($discount_id)
    {
        $data           = array();
        $DiscountTitle  = $this->discounts
                                ->find()
                                ->where('id', $discount_id)
                                ->select('name')
                                ->get()
                                ->row()->name;
        switch ($this->input->method()) 
        {
            case 'get':
                $this->pageTitle = "Assign Products: $DiscountTitle";

                $this->load->model('Products_model', 'products');
                $this->load->model('Discount_assigned_products_model', 'assigned_products');

                $data['all_products'] = $this->products
                                            ->find()
                                            ->select('id, name')
                                            ->get()
                                            ->result_array();

                $assigned             = $this->assigned_products
                                            ->find()
                                            ->where('discount_id', $discount_id)
                                            ->select('product_id')
                                            ->get()
                                            ->result_array();
                $selected = array();
                foreach (($assigned?:array()) as $key => $value) 
                {
                     $selected[]   = $value['product_id'];
                }
                $data['selected'] = $selected;
                $this->load->admin('discounts/assign_products_form',$data);

                break;
            
            case 'post':
                $this->load->model('Discount_assigned_products_model', 'assigned_products');
                $assigned_ids = $this->input->post('AssignedProducts');
                $batch_data = array();
                foreach ($assigned_ids as $key => $value) 
                {
                    $batch_data[$key]['discount_id'] = $discount_id;
                    $batch_data[$key]['product_id'] = $value;
                }

                $this->assigned_products
                    ->find()
                    ->where('discount_id', $discount_id)
                    ->delete();

                $this->assigned_products
                    ->batchInsert($batch_data,false);
                if ($this->assigned_products->getAffectedRows() > 0) 
                {
                   $this->session->set_flashdata('success', "Products successfully assigned to '$DiscountTitle'");
                   redirect(admin_url('discounts')); 
                }
                else
                {

                    $this->load->model('Products_model', 'products');
                    $data['all_products'] = $this->products
                                                ->find()
                                                ->select('id, name')
                                                ->get()
                                                ->result_array();

                    $data['selected']     = $assigned_ids;
                    $this->session->set_flashdata('error', "Unable to assign products to '$DiscountTitle'");
                    $this->load->admin('discounts/assign_products_form',$data);
                    

                }

                break;
        }
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $row['values'] = $this->discount_items
                            ->find()
                            ->where('discount_id', $id)
                            ->get()
                            ->result_array();

        $row = $this->_save($row);
        
        $this->load->admin('discounts/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->discounts->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('discounts'));
    }


    public function change_status($id,$key)
    {
        $data['status'] = ($key)?'Activated':'De-Activated';
        $this->discounts->update($data,$id,false);
        if ($this->discounts->getAffectedRows() > 0) 
        {
            $this->session->set_flashdata('success', 'Discount '.$data['status'].' successfully.');
        }
        else
        {
            $this->session->set_flashdata('error', 'Unable to '.$data['status'].' Discount.');
        }

        redirect(admin_url('discounts'));
    }

    public function datatable()
    {
        $model = $this->discounts->find();
        $totalData = $totalFiltered = $this->discounts->count();
        $model = $this->discounts;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    $row[] = in_array($c, $this->dateFields) ? date('d-M-Y h:ia', strtotime($d[$c])) : $d[$c];
                }

                $update = in_array('discounts.update', $this->permissions) ? admin_url('discounts/update/' . $d['id']) : '';
                $delete = in_array('discounts.delete', $this->permissions) ? admin_url('discounts/delete/' . $d['id']) : '';
                $assign_products = in_array('discounts.assign_products', $this->permissions) ? admin_url('discounts/assign_products/' . $d['id']) : '';
                $change_status = in_array('discounts.change_status', $this->permissions) ? admin_url('discounts/change_status/' . $d['id']."/".(($d['status'] == 'Activated')?"0":"1")) : '';



                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                if($assign_products)
                {
                    $actions .= "  <a href='{$assign_products}'  class='btn btn-warning btn-sm' title='Assign Products'><i class='fa fa-plus' ></i></button>";
                }

                if($change_status)
                {
                    $actions .= ($d['status'] == 'Activated')?"  <a href='{$change_status}' class='btn btn-info btn-sm' title='De-Activate' onclick='return confirm(\"Are you sure you want to de-activate this?\")'><i class='fa fa-pause'></i></button>":"  <a href='{$change_status}' class='btn btn-success btn-sm' title='Activate' onclick='return confirm(\"Are you sure you want to activate this?\")'><i class='fa fa-check'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete || $assign_products || $change_status) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->discounts
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('discounts'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();
            $values = isset($inputs['values']) ? $inputs['values'] : array();

            if ($this->discounts->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->discounts->update($inputs, $row['id'], false);
                }
                else
                {
                    $this->discounts->insert($inputs, false);
                    $row['id'] = $this->discounts->getLastInsertID();
                }

                $vids = [0];
                foreach ($values as $v)
                {
                    $v['discount_id'] = $row['id'];

                    $val = $this->discount_items
                                    ->find()
                                    ->where('id', (int) $v['id'])
                                    ->get()
                                    ->row_array();

                    if($val)
                    {
                        $v['is_default'] = $v['is_default']??0;
                        $this->discount_items->update($v, $v['id'], false);
                    }
                    else
                    {
                        unset($v['id']);

                        $this->discount_items->insert($v, false);
                        $val['id'] = $this->discount_items->getLastInsertID();
                    }

                    $vids[] = $val['id'];
                }

                $this->discount_items
                    ->find()
                    ->where('discount_id', $row['id'])
                    ->where_not_in('id', $vids)
                    ->delete();

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('discounts'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_name($name)
    {   
        $id = $this->uri->segment('4');
        
        $row = $this->discounts->find()->where('name', $name);

        if($id)
        {
            $row->where_not_in('id', $id);
        }

        $result = $row->get()->num_rows();

        return $result == 0 ? true : false;
    }
}
