<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

	public function index()
	{
        $this->load->model('Orders_model', 'Orders');

        $date = isset($_GET['date']) && $_GET['date'] ? explode('|', $_GET['date']) : [];

        $start_date = isset($date[0]) && $date[0] ? date('Y-m-d 00:00:00', strtotime($date[0])) : date('Y-m-d 00:00:00', strtotime('-7 days'));
        $end_date   = isset($date[1]) && $date[1] ? date('Y-m-d 23:59:59', strtotime($date[1])) : date('Y-m-d 23:59:59');

		$open_order = $this->Orders
                            ->find()
                            ->select('COUNT(id) as cnt')
                            ->where('status', 1)
                            ->where("created_at BETWEEN '{$start_date}' AND '{$end_date}'")
                            ->get()
                            ->row_array();

        $total_order = $this->Orders
                            ->find()
                            ->select('COUNT(id) as cnt')
                            ->where("created_at BETWEEN '{$start_date}' AND '{$end_date}'")
                            ->get()
                            ->row_array();

        $total_revenue = $this->Orders
                            ->find()
                            ->select('SUM(net_total) as total')
                            ->where("created_at BETWEEN '{$start_date}' AND '{$end_date}'")
                            ->get()
                            ->row_array();

        $refunded = $this->Orders
                            ->find()
                            ->select('COUNT(id) as cnt')
                            ->where('status', 3)
                            ->where("created_at BETWEEN '{$start_date}' AND '{$end_date}'")
                            ->get()
                            ->row_array();

        $total_order_date = $this->Orders
                                ->find()
                                ->select('COUNT(id) as cnt, DATE_FORMAT(created_at, "%Y-%m-%d") as created')
                                ->where("created_at BETWEEN '{$start_date}' AND '{$end_date}'")
                                ->group_by('DATE_FORMAT(created_at, "%Y-%m-%d")')
                                ->get()
                                ->result_array();

        $total_revenue_date = $this->Orders
                                ->find()
                                ->select('SUM(net_total) as total, DATE_FORMAT(created_at, "%Y-%m-%d") as created')
                                ->where("created_at BETWEEN '{$start_date}' AND '{$end_date}'")
                                ->group_by('DATE_FORMAT(created_at, "%Y-%m-%d")')
                                ->get()
                                ->result_array();

        // dd($total_revenue_date);

        $data['start_date']    = $start_date;
        $data['end_date']      = $end_date;
        $data['open_order']    = $open_order ? $open_order['cnt'] : 0;
        $data['total_order']   = $total_order ? $total_order['cnt'] : 0;
        $data['total_revenue'] = $total_revenue ? '€'.number_format($total_revenue['total'], 2, ",", ".") : 0;
        $data['refunded']      = $refunded ? $refunded['cnt'] : 0;
        $data['returns']       = 0;

        while (strtotime($start_date) <= strtotime($end_date)) {
            $date = date ("Y-m-d", strtotime($start_date));

            $order = $revenue = 0;

            foreach ($total_order_date as $v)
            {
                if($v['created'] == $date)
                {
                    $order = $v['cnt'];
                }
            }

            foreach ($total_revenue_date as $v)
            {
                if($v['created'] == $date)
                {
                    $revenue = $v['total'];
                }
            }

            $data['orders_by_date'][] = [
                'y' => $date,
                'orders' => $order
            ];

            $data['revenue_by_date'][] = [
                'y' => $date,
                'revenue' => $revenue
            ];

            $start_date = date ("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }

		$this->load->admin('dashboard', $data);
	}

	public function profile()
    {
    	$this->pageTitle = 'Update Profile';

    	$user = $this->Users
                    ->find()
                    ->where('id', $this->user['id'])
                    ->get()
                    ->row_array();

    	if($this->input->server('REQUEST_METHOD') == 'POST')
        {
        	$user = $this->input->post();

        	$this->form_validation->set_rules('first_name', 'first name', 'trim|required');
        	$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|callback_validate_unique_email');
            $this->form_validation->set_message('validate_unique_email', 'The %s is already taken');

            if($user['password'] || $user['repassword'])
            {
            	$this->form_validation->set_rules('password', 'password', 'trim|required');
            	$this->form_validation->set_rules('repassword', 'confirm password', 'trim|required|matches[password]');
            }

            if ($this->form_validation->run())
            {
            	$data = [
            		'first_name' 	=> $user['first_name'],
            		'last_name' 	=> $user['last_name'],
            		'email'			=> $user['email'],
            		'profile_photo' => $user['profile_photo'],
            	];

            	if($user['password'])
                {
                    $data['password'] = $data['repassword'] = md5($user['password']);
                }

                $this->Users->update($data, $this->user['id'], false);

            	$this->session->set_flashdata('success', 'Profile updated successfully.');
            	redirect(admin_url('dashboard'));
            }
            else
            {
            	$this->session->set_flashdata('error', validation_errors());
            }
        }

        $this->load->admin('profile', compact('user'));
    }

    public function validate_unique_email($email)
    {   
        $user = $this->Users->find()->where('email', $email);
        $user->where_not_in('id', $this->user['id']);

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }
}