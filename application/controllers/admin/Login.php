<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function index()
	{
        $user = $this->session->has_userdata('admin_user') ? $this->session->userdata('admin_user') : '';

        if($user)
        {
            $user = $this->Users->find()
                        ->where('id', $user['id'])
                        ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                        ->get()
                        ->row_array();

            if($user)
            {
                redirect(admin_url('dashboard'));
            }
            else
            {
                $this->session->unset_userdata('admin_user');
            }
        }

        if($this->input->post('signin'))
        {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'trim|required');

            if ($this->form_validation->run())
            {
                $user = $this->Users->find()
                        ->where('email', $email)
                        ->where('password', MD5($password))
                        ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                        ->get()
                        ->row_array();

                if($user)
                {
                    $this->session->set_userdata(array('admin_user' => $user));
                    redirect(admin_url('dashboard'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Invalid Email/Password.');
                }
            }
            else
            {
                $this->session->set_flashdata('error', validation_errors());
            }

            redirect(url('office'));
        }

		$this->load->admin_login('login');
	}

    public function logout()  
    {  
        $this->session->unset_userdata('admin_user');
        $this->session->set_flashdata('success', 'Successfully Logged Out...');
        redirect(url('office'));  
    }

    public function forgot_password()
    {
        if($this->input->post('forgot'))
        {
            $email = $this->input->post('email');

            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

            if ($this->form_validation->run())
            {
                $user = $this->Users->find()
                        ->where('email', $email)
                        ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                        ->get()
                        ->row_array();

                if($user)
                {
                    $reset_key = md5(time());
                    $this->Users->update(['reset_key' => $reset_key], $user['id']);

                    $params = array(
                        'reset_url' => admin_url('login/reset_password/?email=' . $user['email'] . '&code=' . $reset_key)
                    );

                    $email_body = $this->load->email('admin/reset_password', $params);

                    $this->load->library('email');

                    $this->email->from(NO_REPLY_EMAIL, SITE_NAME);
                    $this->email->to($user['email']);
                    $this->email->subject('Reset Password');
                    $this->email->message($email_body);
                    $this->email->send();

                    $this->session->set_flashdata('success', 'An email as been sent with further instructions to reset your.');
                    redirect(url('office'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'No user found matching provided Email address.');
                }
            }
            else
            {
                $this->session->set_flashdata('error', validation_errors());
            }

            redirect(admin_url('login/forgot_password'));
        }

        $this->load->admin_login('forgot_password');
    }

    public function reset_password()
    {
        $data['email'] = $this->input->post('email') ? $this->input->post('email') : $this->input->get('email');
        $data['code'] = $this->input->post('code') ? $this->input->post('code') : $this->input->get('code');
        $invalid = true;

        if($data['email'] && $data['code'])
        {
            $user = $this->Users->find()
                    ->where('email', $data['email'])
                    ->where('reset_key', $data['code'])
                    ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                    ->get()
                    ->row_array();

            if($user)
            {
                $invalid = false;

                if($this->input->post('reset'))
                {
                    $password = $this->input->post('password');
                    $repassword = $this->input->post('repassword');

                    $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');
                    $this->form_validation->set_rules('repassword', 'confirm password', 'trim|required|matches[password]');

                    if ($this->form_validation->run())
                    {
                        $this->Users->update([
                            'password' => md5($password),
                            'reset_key' => '',
                        ], $user['id']);

                        $this->session->set_flashdata('success', 'Password updated successfully.');
                        redirect(url('office'));
                    }
                    else
                    {
                        $this->session->set_flashdata('error', validation_errors());
                    }
                }
            }
        }
        
        if($invalid)
        {
            $this->session->set_flashdata('error', 'Unable to process your request, link to reset password is invalid or expired.');
            redirect(url('office'));    
        }
        
        $this->load->admin_login('reset_password', $data);        
    }  
}