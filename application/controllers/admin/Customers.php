<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller {

    private $dataTableColumns = ["id","first_name","last_name","email"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Customers_model', 'Customers');
        $this->load->model('Customer_address_model', 'CustomerAddress');
        $this->pageTitle = 'Customers';
    }

    public function index()
    {
        $this->load->admin('customers/index');
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $row = $this->_save($row);

        if(!isset($row['billing']))
        {
            $address = $this->CustomerAddress
                        ->find()
                        ->where('customer_id', $id)
                        ->get()
                        ->result_array();

            foreach ($address as $a)
            {
                if($a['type'] == 1)
                {
                    $row['billing'] = $a;
                }
                elseif($a['type'] == 2)
                {
                    $row['shipping'] = $a;
                }
            }
        }
        
        $this->load->admin('customers/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Customers->update(['email' => $row['email'] . '_deleted'], $id, false);
        $this->Customers->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('customers'));
    }

    public function datatable()
    {   
        $model = $this->Customers->find();
        $totalData = $totalFiltered = $this->Customers->count();
        $model = $this->Customers;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if(in_array($c, $this->dateFields))
                    {
                        $row[] =  date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];    
                    }
                }

                $update = in_array('customers.update', $this->permissions) ? admin_url('customers/update/' . $d['id']) : '';
                $delete = in_array('customers.delete', $this->permissions) ? admin_url('customers/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";

                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Customers
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('customers'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();

            $this->Customers->is_new(0);

            if ($this->Customers->validate($inputs))
            {
                if(isset($inputs['password']) && !$inputs['password'])
                {
                    $inputs['password'] = $inputs['repassword'] = $row['password'];
                }
                else
                {
                    $inputs['password'] = $inputs['repassword'] = md5($inputs['password']);
                }

                $this->Customers->update($inputs, $row['id'], false);

                $bill = $this->CustomerAddress
                            ->find()
                            ->where('customer_id', $row['id'])
                            ->where('type', 1)
                            ->get()
                            ->row_array();

                if($bill)
                {
                    $this->CustomerAddress->update($inputs['billing'], $bill['id'], false);
                }
                else
                {
                    $inputs['billing']['customer_id'] = $row['id'];
                    $inputs['billing']['type'] = 1;

                    $this->CustomerAddress->insert($inputs['billing'], false);
                }

                $ship = $this->CustomerAddress
                            ->find()
                            ->where('customer_id', $row['id'])
                            ->where('type', 2)
                            ->get()
                            ->row_array();

                if($ship)
                {
                    $this->CustomerAddress->update($inputs['shipping'], $ship['id'], false);
                }
                else
                {
                    $inputs['shipping']['customer_id'] = $row['id'];
                    $inputs['shipping']['type'] = 2;

                    $this->CustomerAddress->insert($inputs['shipping'], false);
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('customers'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_unique_email($email)
    {   
        $id = $this->uri->segment('4');
        
        $user = $this->Customers->find()->where('email', $email);

        if($id)
        {
            $user->where_not_in('id', $id);
        }

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }
}
