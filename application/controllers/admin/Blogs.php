<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends Admin_Controller {

    private $dataTableColumns = ["id","title","author_id","status","publish_date"];
    private $dateFields = ["created_at","updated_at","publish_date"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Blogs_model', 'Blogs');
        $this->load->model('Blog_categories_model', 'Categories');
        $this->load->model('Blogs_categories_map_model', 'CategoryMap');
        $this->load->model('Seo_urls_model', 'SeoUrl');
        $this->pageTitle = 'Blogs';
    }

    public function index()
    {
        $this->load->admin('blogs/index');
    }

    public function create()
    {
        $row = $this->_save();

        $categories = $this->Categories
                        ->find()
                        ->get()
                        ->result_array();

        $this->load->admin('blogs/form', compact('row', 'categories'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $url = $this->SeoUrl
                    ->find()
                    ->where('row_id', $id)
                    ->where('tbl_name', 'blogs')
                    ->get()
                    ->row_array();

        $row['slug'] = $url['slug'];

        $catmap = $this->CategoryMap
                    ->find()
                    ->where('blog_id', $row['id'])
                    ->get()
                    ->result_array();

        $row['categories'] = array();

        foreach ($catmap as $c)
        {
            $row['categories'][] = $c['category_id'];
        }

        $row = $this->_save($row);

        $categories = $this->Categories
                        ->find()
                        ->get()
                        ->result_array();

        $this->load->admin('blogs/form', compact('row', 'categories'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Blogs->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('blogs'));
    }

    public function datatable()
    {
        $model = $this->Blogs->find();
        $totalData = $totalFiltered = $this->Pages->count();
        $model = $this->Blogs;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if($c == 'author_id')
                {
                    $where[] = '(u.first_name LIKE "%' . $search . '%" OR u.last_name LIKE "%' . $search . '%")';
                }
                elseif (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(b.' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = 'b.' . $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->setAlias('b')
                ->find()
                ->join('users AS u', 'u.id = b.author_id')
                ->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->setAlias('b')
                        ->find()
                        ->select('b.*, u.first_name, u.last_name')
                        ->join('users AS u', 'u.id = b.author_id')
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'author_id')
                    {
                        $row[] = $d['first_name'] . ' ' . $d['last_name'];
                    }
                    elseif($c == 'status')
                    {
                        switch ($d[$c]) {
                            case 0:
                                $row[] = '<span class="label label-default">Draft</span>';
                                break;
                            case 1:
                                $row[] = '<span class="label label-success">Published</span>';
                                break;
                            case 2:
                                $row[] = '<span class="label label-primary">Scheduled</span>';
                                break;
                        }
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] = date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];
                    }
                }

                $update = in_array('blogs.update', $this->permissions) ? admin_url('blogs/update/' . $d['id']) : '';
                $delete = in_array('blogs.delete', $this->permissions) ? admin_url('blogs/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Blogs
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('blogs'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs     = $this->input->post();
            $slug       = $inputs['slug'];
            $categories = $inputs['categories'];

            if ($this->Blogs->validate($inputs))
            {
                if($inputs['status'] == 2)
                {
                    $inputs['publish_date'] = $inputs['publish_date'] ? date('Y-m-d H:i:s', strtotime($inputs['publish_date'])) : date('Y-m-d H:i:s');
                }

                $inputs['publish_date'] = $inputs['publish_date'] ? $inputs['publish_date'] : date('Y-m-d H:i:s');

                if(isset($row['id']) && $row['id'])
                {
                    $this->Blogs->update($inputs, $row['id'], false);
                }
                else
                {
                    $inputs['author_id'] = current_uid();

                    $this->Blogs->insert($inputs, false);
                    $row['id'] = $this->Blogs->getLastInsertID();
                }

                $seo_url = $this->SeoUrl
                                ->find()
                                ->where('row_id', $row['id'])
                                ->where('tbl_name', 'blogs')
                                ->get()
                                ->row_array();

                $slug_arr = array(
                    'slug' => $slug,
                    'tbl_name' => 'blogs',
                    'row_id' => $row['id']
                );

                if($seo_url)
                {
                    $this->SeoUrl->update($slug_arr, $seo_url['id'], false);
                }
                else
                {
                    $this->SeoUrl->insert($slug_arr, false);
                }

                $cids = [0];
                foreach ($categories as $c)
                {
                    $category = $this->CategoryMap
                                    ->find()
                                    ->where('blog_id', $row['id'])
                                    ->where('category_id', $c)
                                    ->get()
                                    ->row_array();

                    if($category)
                    {
                        $cids[] = $category['id'];
                    }
                    else
                    {
                        $this->CategoryMap->insert([
                            'blog_id'     => $row['id'],
                            'category_id' => $c,
                        ], false);

                        $cids[] = $this->CategoryMap->getLastInsertID();
                    }
                }

                $this->CategoryMap
                    ->find()
                    ->where('blog_id', $row['id'])
                    ->where_not_in('id', $cids)
                    ->delete();

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('blogs'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_slug($slug)
    {   
        $id = $this->uri->segment('4');
        
        $row = $this->SeoUrl
                    ->find()
                    ->where('slug', $slug)
                    ->where_in('tbl_name', ['blogs', 'pages']);

        $result = $row->get()->row_array();

        if($result)
        {
            return ($result['tbl_name'] == 'blogs' && $result['row_id'] == $id) ? true : false;
        }
        else
        {
            return true;
        }
    }
}
