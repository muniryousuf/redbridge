<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends Admin_Controller {

	public function index()
	{
		$this->pageTitle = 'Roles Permissions';

		$data['controllers'] = $data['roles'] = $data['permissions'] = array();

		$data['cLabels'] = [
            'Sitesettings' => 'General Settings',
            'Sitevariables' => 'Config Variables',
            'Menu' => 'Menu Management',
            'Faqcategories' => 'Faq Categories',
            'Blogcategories' => 'Blog Categories',
            'Productcategories' => 'Product Categories',
            'Productattributes' => 'Product Attributes',
        ];

        $data['aLabels'] = [
            'index'     => 'List Data',
            'create'    => 'Add Record',
            'update'    => 'Edit Record',
            'view'      => 'View Record',
            'delete'    => 'Delete Record',
            'hierarchy' => 'Update Sorting',
        ];

        foreach (glob(__DIR__.'/*.php') as $file) {
            $filename = str_replace('.php', '', basename($file));

            if(!in_array($filename, ['Login', 'Dashboard', 'Menuitems', 'Crud'])) {
                $content = file_get_contents($file);
                preg_match_all('/public[a-zA-Z\s]+function\s[a-zA-Z0-9]+\s?\(/', $content, $actions);

                if(isset($actions[0])) {
                    foreach ($actions[0] as $k => $v) {
                        $v = trim(str_replace(['public', 'static', 'function', '(', '('], '', $v));

                        if(in_array($v, ['store', 'datatable', 'updatehierarchy'])) {
                            unset($actions[$k]);
                        } else {
                            $actions[$k] = $v;
                        }
                    }
                }

                $data['controllers'][$filename] = $actions;
            }
        }

        $this->load->model('Roles_model', 'Roles');

        $roles = $this->Roles->find()
        				->where_in('id', explode(',', $this->vars['backend-roles']))
        				->get()
        				->result_array();

        foreach ($roles as $r)
        {
        	$data['roles'][$r['id']] = $r['name'];
        }
		
        foreach ($this->Permissions->findAll() as $p)
        {
            $data['permissions'][] = $p->role_id .'.'. $p->controller .'.'. $p->action;
        }

        $this->load->admin('permissions/index', $data);
	}

	public function store()
	{
		$this->db->truncate('permissions');

        $rows = array();
        $permissions = $this->input->post('permissions');

        if($permissions)
        {
        	foreach ($this->input->post('permissions') as $r => $controller)
        	{
	            foreach ($controller as $c => $actions)
	            {
	                foreach ($actions as $a => $v)
	                {
	                    $rows[] = [
	                        'controller' => strtolower($c),
	                        'action' => strtolower($a),
	                        'role_id' => $r
	                    ];
	                }
	            }
	        }

	        $this->Permissions->batchInsert($rows);
        }
        
		$this->session->set_flashdata('success', 'Permissions updated successfully.');
		redirect(admin_url('permissions'));
	}
}