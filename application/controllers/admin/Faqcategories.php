<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqcategories extends Admin_Controller {

    private $dataTableColumns = ["id","name","slug","created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Faq_categories_model', 'Faq_categories');
        $this->pageTitle = 'Faq Categories';
    }

    public function index()
    {
        $this->load->admin('faq_categories/index');
    }

    public function create()
    {
        $row = $this->_save();

        $this->load->admin('faq_categories/form', compact('row'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $row = $this->_save($row);
        
        $this->load->admin('faq_categories/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Faq_categories->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('faqcategories'));
    }

    public function datatable()
    {
        $model = $this->Faq_categories->find();
        $totalData = $totalFiltered = $this->Faq_categories->count();
        $model = $this->Faq_categories;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    $row[] = in_array($c, $this->dateFields) ? date('d-M-Y h:ia', strtotime($d[$c])) : $d[$c];
                }

                $update = in_array('faqcategories.update', $this->permissions) ? admin_url('faqcategories/update/' . $d['id']) : '';
                $delete = in_array('faqcategories.delete', $this->permissions) ? admin_url('faqcategories/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Faq_categories
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('faqcategories'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();

            if ($this->Faq_categories->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->Faq_categories->update($inputs, $row['id'], false);
                }
                else
                {
                    $this->Faq_categories->insert($inputs, false);
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('faqcategories'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_slug($slug)
    {   
        $id = $this->uri->segment('4');
        
        $row = $this->Faq_categories->find()->where('slug', $slug);

        if($id)
        {
            $row->where_not_in('id', $id);
        }

        $result = $row->get()->num_rows();

        return $result == 0 ? true : false;
    }
}
