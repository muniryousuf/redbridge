<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productattributes extends Admin_Controller {

    private $dataTableColumns = ["id","name","created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Product_attributes_model', 'Product_attributes');
        $this->load->model('Product_attributes_vals_model', 'AttributesVals');
        $this->pageTitle = 'Product Attributes';
    }

    public function index()
    {
        $this->load->admin('product_attributes/index');
    }

    public function create()
    {
        $row = $this->_save();

        $this->load->admin('product_attributes/form', compact('row'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $row['values'] = $this->AttributesVals
                            ->find()
                            ->where('attribute_id', $id)
                            ->get()
                            ->result_array();

        $row = $this->_save($row);
        
        $this->load->admin('product_attributes/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Product_attributes->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('productattributes'));
    }

    public function datatable()
    {
        $model = $this->Product_attributes->find();
        $totalData = $totalFiltered = $this->Product_attributes->count();
        $model = $this->Product_attributes;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    $row[] = in_array($c, $this->dateFields) ? date('d-M-Y h:ia', strtotime($d[$c])) : $d[$c];
                }

                $update = in_array('productattributes.update', $this->permissions) ? admin_url('productattributes/update/' . $d['id']) : '';
                $delete = in_array('productattributes.delete', $this->permissions) ? admin_url('productattributes/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Product_attributes
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('productattributes'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();
            $values = isset($inputs['values']) ? $inputs['values'] : array();

            if ($this->Product_attributes->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->Product_attributes->update($inputs, $row['id'], false);
                }
                else
                {
                    $this->Product_attributes->insert($inputs, false);
                    $row['id'] = $this->Product_attributes->getLastInsertID();
                }

                $vids = [0];
                foreach ($values as $v)
                {
                    $v['attribute_id'] = $row['id'];

                    $val = $this->AttributesVals
                                    ->find()
                                    ->where('id', (int) $v['id'])
                                    ->get()
                                    ->row_array();

                    if($val)
                    {
                        $this->AttributesVals->update($v, $v['id'], false);
                    }
                    else
                    {
                        unset($v['id']);

                        $this->AttributesVals->insert($v, false);
                        $val['id'] = $this->AttributesVals->getLastInsertID();
                    }

                    $vids[] = $val['id'];
                }

                $this->AttributesVals
                    ->find()
                    ->where('attribute_id', $row['id'])
                    ->where_not_in('id', $vids)
                    ->delete();

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('productattributes'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_name($name)
    {   
        $id = $this->uri->segment('4');
        
        $row = $this->Product_attributes->find()->where('name', $name);

        if($id)
        {
            $row->where_not_in('id', $id);
        }

        $result = $row->get()->num_rows();

        return $result == 0 ? true : false;
    }
}
