<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admins extends Admin_Controller {

    private $dataTableColumns = ["id","first_name","last_name","email","role_id"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Users_model', 'Users');
        $this->load->model('Roles_model', 'Roles');
        $this->pageTitle = 'Admins';
    }

    public function index()
    {
        $this->load->admin('admins/index');
    }

    public function create()
    {
        $row = $this->_save();

        $roles = $this->Roles
                    ->find()
                    ->where_in('id', explode(',', $this->vars['backend-roles']))
                    ->get()
                    ->result_array();

        $this->load->admin('admins/form', compact('row', 'roles'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $row = $this->_save($row);
        
        $roles = $this->Roles
                    ->find()
                    ->where_in('id', explode(',', $this->vars['backend-roles']))
                    ->get()
                    ->result_array();

        $this->load->admin('admins/form', compact('row', 'roles'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Users->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('admins'));
    }

    public function datatable()
    {   
        $model = $this->Users->find();
        $totalData = $totalFiltered = $this->Users->count();
        $model = $this->Users;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = 'u.' . $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if($c == 'role_id')
                {
                    $where[] = 'r.name LIKE "%' . $search . '%"';
                }
                elseif (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(u.' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = 'u.' . $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->setAlias('u')
                    ->find()
                    ->join('roles AS r', 'r.id = u.role_id')
                    ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                    ->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->setAlias('u')
                        ->find()
                        ->select('u.*, r.name')
                        ->join('roles AS r', 'r.id = u.role_id')
                        ->where_in('role_id', explode(',', $this->vars['backend-roles']))
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'role_id')
                    {
                        $row[] = $d['name'];
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] =  date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];    
                    }
                }

                $update = in_array('admins.update', $this->permissions) ? admin_url('admins/update/' . $d['id']) : '';
                $delete = in_array('admins.delete', $this->permissions) ? admin_url('admins/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";

                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Users
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('admins'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();

            if(!(isset($row['id']) && $row['id']))
            {
                $this->Users->is_new(1);
            }

            if ($this->Users->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    if(isset($inputs['password']) && !$inputs['password'])
                    {
                        $inputs['password'] = $inputs['repassword'] = $row['password'];
                    }
                    else
                    {
                        $inputs['password'] = $inputs['repassword'] = md5($inputs['password']);
                    }

                    $this->Users->update($inputs, $row['id'], false);
                }
                else
                {
                    $inputs['password'] = $inputs['repassword'] = md5($inputs['password']);

                    $this->Users->insert($inputs, false);
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('admins'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_unique_email($email)
    {   
        $id = $this->uri->segment('4');
        
        $user = $this->Users->find()->where('email', $email);

        if($id)
        {
            $user->where_not_in('id', $id);
        }

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }
}
