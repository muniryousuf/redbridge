<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feeds extends Admin_Controller {

	public function index()
	{
		$this->pageTitle = 'Product Feeds';

        $data['vars_key'] = [
            'fb_utm_source',
            'fb_utm_campaign',
            'fb_utm_medium',
            'fb_utm_content',

            'google_utm_source',
            'google_utm_campaign',
            'google_utm_medium',
            'google_utm_content',
        ];

        $data['vars_val'] = [];
        foreach ($data['vars_key'] as $vk)
        {
            $data['vars_val'][$vk] = isset($this->vars[$vk]) ? $this->vars[$vk] : '';
        }

        $this->load->admin('feeds/index', $data);
	}

	public function store()
	{
        $inputs = $this->input->post();
        
        if(isset($inputs['vars']))
        {
            foreach ($inputs['vars'] as $k => $v)
            {
                $row = $this->SiteVars
                        ->find()
                        ->where('key', $k)
                        ->get()
                        ->row_array();

                if($row)
                {
                    $this->SiteVars->update(['value' => $v], $row['id'], false);
                }
                else
                {
                    $this->SiteVars->insert([
                        'key'   => $k,
                        'value' => $v
                    ], false);
                }
            }    
        }
        
		$this->session->set_flashdata('success', 'Feed settings updated successfully.');
		redirect(admin_url('feeds'));
	}
}