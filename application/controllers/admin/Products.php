<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends Admin_Controller {

    private $dataTableColumns = ["id", "name", "sku", "price", "status","sort", "created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Products_model', 'Products');
        $this->load->model('Product_attributes_map_model', 'ProductAttributes');
        $this->load->model('Product_categories_map_model', 'ProductCategories');
        $this->load->model('Product_images_model', 'ProductImages');
        $this->load->model('Product_related_products_model', 'RelatedProducts');
        $this->load->model('Product_categories_model', 'Categories');
        $this->load->model('Product_attributes_model', 'Attributes');
        $this->load->model('Product_attributes_vals_model', 'AttributesValues');

        $this->pageTitle = 'Products';
    }

    public function index()
    {
        $this->load->admin('products/index');
    }

    public function create()
    {
        $row = $this->_save();

        $attributes = $attributesValues = $attributesLabels = $products = [];

        $data = $this->Attributes->find()->get()->result_array();

        foreach ($data as $d) {
            $attributes[$d['id']] = $d['name'];
        }

        $data = $this->AttributesValues->find()->get()->result_array();

        foreach ($data as $d) {
            if(array_key_exists($d['attribute_id'], $attributes)) {
                $attributesValues[$d['attribute_id']][$d['id']] = $d['name'];
                $attributesLabels[$d['id']] = $d['name'];
            }
        }

        $categories = $this->categories();
        $all_products = $this->Products
                            ->find()
                            ->select('id, name')
                            ->get()
                            ->result_array();

        $this->load->admin('products/form', compact('row', 'attributes', 'attributesValues', 'attributesLabels', 'categories', 'all_products'));
    }

    public function update($id)
    {

        $row = $this->_load($id);

        $row = $this->_save($row);

        $product_cats = $this->ProductCategories
                                ->find()
                                ->where('product_id', $id)
                                ->get()
                                ->result_array();

        $row['is_feature'] = isset($product_cats[0])?$product_cats[0]['is_feature']:0;


        $row['categories'] = [];
        foreach ($product_cats as $pc)
        {
            $row['categories'][] = $pc['category_id'];
        }

        $row['images'] = $this->ProductImages
                                ->find()
                                ->where('product_id', $id)
                                ->get()
                                ->result_array();

        $row['variations'] = $this->ProductAttributes
                                ->find()
                                ->where('product_id', $id)
                                ->get()
                                ->result_array();

        $related = $this->RelatedProducts
                            ->find()
                            ->where('product_id', $id)
                            ->get()
                            ->result_array();

        $row['upsell'] = $row['crossell'] = [];
        foreach ($related as $r)
        {
            if($r['related_type'] == 1)
            {
                $row['upsell'][] = $r['related_id'];
            }
            elseif($r['related_type'] == 2)
            {
                $row['crossell'][] = $r['related_id'];
            }
        }

        $attributes = $attributesValues = $attributesLabels = [];

        $data = $this->Attributes->find()->get()->result_array();

        foreach ($data as $d) {
            $attributes[$d['id']] = $d['name'];
        }

        $data = $this->AttributesValues->find()->get()->result_array();

        foreach ($data as $d) {
            if(array_key_exists($d['attribute_id'], $attributes)) {
                $attributesValues[$d['attribute_id']][$d['id']] = $d['name'];
                $attributesLabels[$d['id']] = $d['name'];
            }
        }

        $categories = $this->categories();
        $all_products = $this->Products
                            ->find()
                            ->select('id, name')
                            ->where('id != ' . $id)
                            ->get()
                            ->result_array();

        $this->load->admin('products/form', compact('row', 'attributes', 'attributesValues', 'attributesLabels', 'categories', 'all_products'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Products->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('products'));
    }

    public function datatable()
    {
        $model = $this->Products->find();
        $totalData = $totalFiltered = $this->Products->count();
        $model = $this->Products;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');

            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'status')
                    {
                        $row[] = $d[$c] ? 'Active' : 'Inactive';
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] = date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];
                    }
                }

                $update = in_array('products.update', $this->permissions) ? admin_url('products/update/' . $d['id']) : '';
                $delete = in_array('products.delete', $this->permissions) ? admin_url('products/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";

                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';

                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Products
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('products'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();



            $categories = isset($inputs['categories']) ? $inputs['categories'] : array();
            $images     = isset($inputs['images']) ? $inputs['images'] : array();
            $variations = isset($inputs['variations']) ? $inputs['variations'] : array();
            $upsell     = isset($inputs['upsell']) ? $inputs['upsell'] : array();
            $crossell   = isset($inputs['crossell']) ? $inputs['crossell'] : array();

            $inputs['tags'] = isset($inputs['tags']) ? implode(',', $inputs['tags']) : '';

            if($inputs['stock'] == '') {
                $inputs['stock'] = 'NULL';
            }
            if(!$inputs['price']) {
                unset($inputs['price']);
            }
            if(!$inputs['sale_price'] || $inputs['sale_price'] == 0)
            {
                $inputs['sale_price'] = "";
                $inputs['sale_start'] = NULL;
                $inputs['sale_end']   = NULL;
            }
            if(!$inputs['sale_start']) {
                $inputs['sale_start'] = NULL;
            }
            if(!$inputs['sale_end']) {
                $inputs['sale_end']   = NULL;
            }

            if ($this->Products->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->Products->update($inputs, $row['id'], false);
                    $p_id = $row['id'];
                }
                else
                {
                    $this->Products->insert($inputs, false);
                    $row['id'] = $p_id = $this->Products->getLastInsertID();
                }

                if($inputs['stock'] == 'NULL') {
                    $this->db->query('UPDATE `products` SET `stock` = NULL WHERE `id` = ' . $p_id);
                }

                if($inputs['stock']) {
                    $this->db->query('UPDATE `restock_alerts` SET `status` = 2 WHERE `product_id` = ' . $row['id'] . ' AND (attributes IS NULL OR attributes = "")');
                }



                // Categories
                $cids = [0];


                foreach ($categories as $c)
                {
                    $val = $this->ProductCategories
                                    ->find()
                                    ->where('product_id', $row['id'])
                                    ->where('category_id', $c)
                                    ->get()
                                    ->row_array();

                    if($val)
                    {
                        $cids[] = $val['id'];
                    }
                    else
                    {
                        $this->ProductCategories->insert([
                            'product_id'  => $row['id'],
                            'category_id' => $c,
                            'is_feature'=>$inputs['feature']
                        ], false);

                        $cids[] = $this->ProductCategories->getLastInsertID();
                    }
                }

                $this->db->query('UPDATE `product_categories_map` SET `is_feature` = '.(int) $inputs['feature'].' WHERE `product_id` = ' . $p_id);


                $this->ProductCategories
                    ->find()
                    ->where('product_id', $row['id'])
                    ->where_not_in('id', $cids)
                    ->delete();

                // Images
                $iids = [0];
                foreach ($images as $v)
                {
                    $v['id'] = isset($v['id']) ? (int) $v['id'] : 0;
                    $v['product_id'] = $row['id'];

                    $val = $this->ProductImages
                                    ->find()
                                    ->where('id', $v['id'])
                                    ->get()
                                    ->row_array();

                    if($val)
                    {
                        $this->ProductImages->update($v, $v['id'], false);
                    }
                    else
                    {
                        unset($v['id']);
                        $this->ProductImages->insert($v, false);
                        $val['id'] = $this->ProductImages->getLastInsertID();
                    }

                    $iids[] = $val['id'];
                }

                $this->ProductImages
                    ->find()
                    ->where('product_id', $row['id'])
                    ->where_not_in('id', $iids)
                    ->delete();

                // Variations
                $vids = [0];
                foreach ($variations as $v)
                {
                    $v['id'] = isset($v['id']) ? (int) $v['id'] : 0;
                    $v['product_id'] = $row['id'];

                    $val = $this->ProductAttributes
                                    ->find()
                                    ->where('id', $v['id'])
                                    ->get()
                                    ->row_array();

                    if($v['stock'] == '') {
                        $v['stock'] = 'NULL';
                    }
                    if(!$v['price']) {
                        unset($v['price']);
                    }
                    if(!$v['sale_price']) {
                        unset($v['sale_price']);
                    }
                    if(!$v['sale_start']) {
                        unset($v['sale_start']);
                    }
                    if(!$v['sale_end']) {
                        unset($v['sale_end']);
                    }

                    if($val)
                    {
                        $this->ProductAttributes->update($v, $v['id'], false);
                        $pa_id = $v['id'];
                    }
                    else
                    {
                        unset($v['id']);
                        $this->ProductAttributes->insert($v, false);
                        $val['id'] = $pa_id = $this->ProductAttributes->getLastInsertID();
                    }

                    if($v['stock'] == 'NULL') {
                        $this->db->query('UPDATE `product_attributes_map` SET `stock` = NULL WHERE `id` = ' . $pa_id);
                    }

                    if($v['stock'])
                    {
                        $this->db->query('UPDATE `restock_alerts` SET `status` = 2 WHERE `product_id` = ' . $row['id'] . ' AND attributes = "' . $v['attributes'] . '"');
                    }

                    $vids[] = $val['id'];
                }

                $this->ProductAttributes
                    ->find()
                    ->where('product_id', $row['id'])
                    ->where_not_in('id', $vids)
                    ->delete();

                // Upsell
                $rids = [0];
                foreach ($upsell as $u)
                {
                    $val = $this->RelatedProducts
                                    ->find()
                                    ->where('product_id', $row['id'])
                                    ->where('related_id', $u)
                                    ->where('related_type', '1')
                                    ->get()
                                    ->row_array();

                    if($val)
                    {
                        $rids[] = $val['id'];
                    }
                    else
                    {
                        $this->RelatedProducts->insert([
                            'product_id'   => $row['id'],
                            'related_id'   => $u,
                            'related_type' => '1'
                        ], false);

                        $rids[] = $this->RelatedProducts->getLastInsertID();
                    }
                }

                // Crossell
                foreach ($crossell as $c)
                {
                    $val = $this->RelatedProducts
                                    ->find()
                                    ->where('product_id', $row['id'])
                                    ->where('related_id', $c)
                                    ->where('related_type', '2')
                                    ->get()
                                    ->row_array();

                    if($val)
                    {
                        $rids[] = $val['id'];
                    }
                    else
                    {
                        $this->RelatedProducts->insert([
                            'product_id'   => $row['id'],
                            'related_id'   => $c,
                            'related_type' => '2'
                        ], false);

                        $rids[] = $this->RelatedProducts->getLastInsertID();
                    }
                }

                $this->RelatedProducts
                    ->find()
                    ->where('product_id', $row['id'])
                    ->where_not_in('id', $rids)
                    ->delete();

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('products'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_slug($slug)
    {
        $id = $this->uri->segment('4');

        $row = $this->Products->find()->where('slug', $slug);

        if($id)
        {
            $row->where_not_in('id', $id);
        }

        $result = $row->get()->num_rows();

        return $result == 0 ? true : false;
    }

    private function categories(&$array = [], $parent_id = 0, $dept = 0)
    {
        $where = $parent_id ? 'parent_id = ' . $parent_id : 'parent_id IS NULL OR parent_id = 0';

        $cats = $this->Categories
                    ->find()
                    ->where($where)
                    ->get()
                    ->result_array();

        foreach ($cats as $c)
        {
            $pad = str_pad('', $dept, '-', STR_PAD_LEFT);
            $pad = $pad ? $pad . ' ' : '';

            $array[$c['id']] = $pad . $c['name'];

            if($dept < 3)
            {
                $this->categories($array, $c['id'], ($dept+1));
            }
        }

        return $array;
    }
}
