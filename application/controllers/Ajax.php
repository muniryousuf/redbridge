<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

    public function restock_subscribe()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->model('Restock_alerts_model', 'RestockAlerts');

            $inputs = $this->input->post();
            
            $row = $this->RestockAlerts
                        ->find()
                        ->where('product_id', $inputs['pid'])
                        ->where('attributes', $inputs['attributes'])
                        ->where('email', $inputs['email'])
                        ->get()
                        ->row_array();

            if($row)
            {
                if($row['status'] == 0)
                {
                    echo 'Je bent al ingeschreven';
                    exit;
                }

                $this->RestockAlerts->update(['status' => 0], $row['id'], false);
            }
            else
            {
                $this->RestockAlerts->insert([
                    'status'     => 0,
                    'product_id' => $inputs['pid'],
                    'attributes' => $inputs['attributes'],
                    'email'      => $inputs['email']
                ], false);
            }

            echo 'Je bent succesvol ingeschreven';
            exit;
        }
    }

    public function contact()
    {
        $data  = $this->input->post();

        $this->form_validation->set_rules('name', 'Naam', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('message', 'Bericht', 'trim|required');

        if ($this->form_validation->run())
        {
            $this->load->library('email');

            // Admin
            $data['title'] = 'Contact formulier - ' . site_name();
            $email_body = $this->load->front_email('contact', $data);

            $this->email->from(NO_REPLY_EMAIL, site_name());
            $this->email->to($this->vars['contact_email']);
            $this->email->subject($data['title']);
            $this->email->message($email_body);
            $this->email->send();

            // Customer
            $data['title'] = 'Bedankt voor je bericht';
            $email_body = $this->load->front_email('contact_customer', $data);

            $this->email->from($this->vars['contact_email'], site_name());
            $this->email->to($data['email']);
            $this->email->subject($data['title']);
            $this->email->message($email_body);
            $this->email->send();

            $result = [
                'success' => 1,
                'message' => '<div class="row"><div class="col-md-4 text-center"><i class="far fa-check-circle fa-2x my-4"></i><h3>Bericht ontvangen</h3></div></div>'
            ];
        }
        else
        {
            $result = [
                'success' => 0,
                'message' => '<div class="alert alert-danger mb-4">' . validation_errors() . '</div>'
            ];
        }

        echo json_encode($result);
        exit;
    }

    public function returnform()
    {
        $data  = $this->input->post();

        $this->form_validation->set_rules('orderno', 'Ordernummer', 'trim|required');
        $this->form_validation->set_rules('name', 'Voor- en achternaam', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mailadres', 'trim|required|valid_email');
        $this->form_validation->set_rules('delivery_date', 'Leverdatum', 'trim|required');
        $this->form_validation->set_rules('reason1', 'Reden retour', 'trim|required');

        if ($this->form_validation->run())
        {
            $this->load->library('email');

            // Admin
            $data['title'] = 'Bevestiging retour';
            $email_body = $this->load->front_email('return', $data);

            $this->email->from(NO_REPLY_EMAIL, site_name());
            $this->email->to($this->vars['return_email']);
            $this->email->subject($data['title']);
            $this->email->message($email_body);
            $this->email->send();

            // Customer
            $data['title'] = 'Print retourformulier';
            $email_body = $this->load->front_email('return_customer', $data);

            $this->email->from($this->vars['return_email'], site_name());
            $this->email->to($data['email']);
            $this->email->subject($data['title']);
            $this->email->message($email_body);
            $this->email->send();

            $result = [
                'success' => 1,
                'message' => '<div class="row"><div class="col-md-6 text-center"><i class="far fa-check-circle fa-2x my-4"></i><h3>Het retourformulier is per e-mail naar je verzonden, bekijk ook de ongewenste inbox.</h3></div></div>'
            ];
        }
        else
        {
            $result = [
                'success' => 0,
                'message' => '<div class="alert alert-danger mb-4">' . validation_errors() . '</div>'
            ];
        }

        echo json_encode($result);
        exit;
    }

    public function verify_confirmation_code()
    {
        $response['status'] = false;
        $response['message'] = '';

        $confirmationCode = $this->input->post('confirmationCode');
        $code = $this->session->confirmationSent['code'];
        $time = $this->session->confirmationSent['time']?:30;
        if ((time() - $time) <= 60 && $code == $confirmationCode) 
        {
            $response['status'] = true;
            $response['message'] = 'Confirmation code verified, proceeding with order placement';
        }
        else
        {
            $response['message'] = ($code != $confirmationCode)?'Invalid code entered.':"";
            $response['message'] = ((time() - $time) > 30)?'Code has expired.':$response['message'];
        }
        exit(json_encode($response));
    }

    public function order_confirmation()
    {
        $response['status'] = false;
        if ($this->input->method() == "post" && ($this->input->is_ajax_request())) 
        {
            $cellNumber   = $this->input->post('cellNumber');
            $emailAddress = $this->input->post('emailAddress');
            $firstName    = $this->input->post('firstName');
            $lastName     = $this->input->post('lastName');
            $code         = mt_rand(10000,99999);
            $message      = "Dear {$firstName} {$lastName},<br>Your order confirmation code is $code. Kindly do not disclose this code to others for you own financial security.<br><br>Regards,<br>Fabricano<br>support@fabricano.pk<br>http://fabricano.pk<br><br>PS:Please ingnore this message if received by mistake";

            $config = array();
            $config['protocol']     = 'smtp';
            $config['smtp_host']    = 'smtp.gmail.com';
            $config['smtp_user']    = 'buyfabricano@gmail.com';
            $config['smtp_pass']    = 'Karachi@123';
            $config['smtp_crypto']  = 'tls';
            $config['smtp_port']    = '587';
            $config['charset']      = 'utf8';
            $config['mailtype']     = "html";
            $config['newline']      = "\r\n";


            $this->email->initialize($config);


            $this->email->from('noreply@fabricano.pk', 'Fabricano.pk');
            $this->email->to($emailAddress);

            $this->email->subject('Order Confirmation');
            $this->email->message($message);
            $this->email->send();
            
            $ch  = curl_init();
            $url = "https://smsgateway24.com/getdata/gettoken";

            $postarray = [
                'email' => 'sharjeelahmed92@hotmail.com',
                'pass'  => 'karachi123'
                ];

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$postarray);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $output = json_decode(curl_exec($ch),true);
            

            if (!$output['error']) 
            {
                $token= $output['token'];

                
                $ch = curl_init();
                $url = "https://smsgateway24.com/getdata/addsms";

                $postarray = [
                'token'     => $token,
                'sendto'    => $cellNumber,
                'body'      => "fabricano.pk. Your order confirmation code is $code",
                'device_id' => "1778",
                ];

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$postarray);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
                $output = json_decode(curl_exec($ch),true);
            }

            $response['status'] = true;
            
            
            $confirmationSent['time'] = time();
            $confirmationSent['code'] = $code;
            $this->session->set_userdata('confirmationSent',$confirmationSent);
        }
        exit(json_encode($response));
    }
}