<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductCategory extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Product_categories_model', 'Categories');
    }

    public function index()
    {
        $category = $this->Categories
                        ->find()
                        ->where('slug', $this->uri->segment(2))
                        ->get()
                        ->row_array();

        if($category)
        {
            $this->seo = generate_meta($category);

            $products = $this->Products
                            ->setAlias('p')
                            ->find()
                            ->select('p.id, p.name, p.slug, p.image, p.price, p.sale_price, p.sale_start, p.sale_end')
                            ->join('product_categories_map AS c', 'c.product_id = p.id')
                            ->where('c.category_id', $category['id'])
                            ->where('status', 1)
//                            ->order_by('p.id', 'desc')
                ->order_by('p.sort', 'Asc')
                            ->get()
                            ->result_array();

            $today = time();

            foreach ($products as $k => $p)
            {
                if(!$p['price'])
                {
                    $attr = $this->ProductAttributes
                                ->find()
                                ->where('product_id', $p['id'])
                                ->where('(price > 0 OR sale_price > 0)')
                                ->order_by('sale_price', 'asc')
                                ->order_by('price', 'asc')
                                ->get()
                                ->row_array();

                    $products[$k]['price']      = $attr['price'];
                    $products[$k]['sale_price'] = $attr['sale_price'];
                    $products[$k]['sale_start'] = $attr['sale_start'];
                    $products[$k]['sale_end']   = $attr['sale_end'];
                }

                $on_sale = $products[$k]['sale_price'] ? 1 : 0;
                $on_sale = $products[$k]['sale_start'] && (strtotime($products[$k]['sale_start']) > $today) ? 0 : $on_sale;
                $on_sale = $products[$k]['sale_end'] && (strtotime($products[$k]['sale_end']) < $today) ? 0 : $on_sale;

                $products[$k]['sale_price'] = $on_sale ? $products[$k]['sale_price'] : '';
                unset($products[$k]['sale_start']);
                unset($products[$k]['sale_end']);

                $sec_img = $this->ProductImages
                                ->find()
                                ->where('product_id', $p['id'])
                                ->order_by('id', 'asc')
                                ->get()
                                ->row_array();

                $products[$k]['image']     = resize_img($products[$k]['image'], 300, 400);
                $products[$k]['sec_image'] = $sec_img ? resize_img($sec_img['image'], 300, 400) : $p['image'];
            }

            $this->load->front('product-category', compact('category', 'products'));
        }
        else
        {
            show_404();
        }
    }
}
