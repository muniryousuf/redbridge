<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once('vendor/autoload.php');

use GlobalPayments\Api\ServiceConfigs;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\PaymentMethods\CreditCardData;

class Order extends MY_Controller
{

    function __construct()
    {

        parent::__construct();
        $this->load->library('PushNotifications');
        $this->load->model('Customers_model', 'Customers');
        $this->load->model('Orders_model', 'Orders');
        $this->load->model('Order_address_model', 'OrderAddress');
        $this->load->model('Order_products_model', 'OrderProducts');
    }

    public function process()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $cart = $this->getCart();
            $inputs = $this->input->post();
//            $mollie = mollie_client();

            $this->form_validation->set_rules('billing[first_name]', 'Voornaam', 'required');
            $this->form_validation->set_rules('billing[last_name]', 'Achternaam', 'required');
            $this->form_validation->set_rules('billing[address1]', 'Straat en huisnummer', 'required');
            $this->form_validation->set_rules('billing[postalcode]', 'Postcode', 'required');
            $this->form_validation->set_rules('billing[city]', 'Plaats', 'required');
            $this->form_validation->set_rules('email', 'E-mailadres', 'required|valid_email');
            $this->form_validation->set_rules('payment_method', 'Payment Method', 'required');

            if (isset($inputs['create_account']) && $inputs['create_account']) {
                $this->form_validation->set_rules('email', 'Accountwachtwoord aanmaken', 'required|valid_email|callback_validate_unique_email', ['validate_unique_email' => 'Email address already registered.']);
                $this->form_validation->set_rules('account_password', 'Accountwachtwoord aanmaken', 'required');
            }

            if (isset($inputs['new_shipping_address']) && $inputs['new_shipping_address']) {
                $this->form_validation->set_rules('shipping[first_name]', 'Shipping Voornaam', 'required');
                $this->form_validation->set_rules('shipping[last_name]', 'Shipping Achternaam', 'required');
                $this->form_validation->set_rules('shipping[address1]', 'Shipping Straat en huisnummer', 'required');
                $this->form_validation->set_rules('shipping[postalcode]', 'Shipping Postcode', 'required');
                $this->form_validation->set_rules('shipping[city]', 'Shipping Plaats', 'required');
            }

            $this->form_validation->set_data($inputs);

            if ($this->form_validation->run() == FALSE) {
                $this->session->set_flashdata('error', '<p>Please fix the below mentioned errors:</p>' . validation_errors('- ', '<br />'));
                redirect(url('/' . $this->vars['checkout_url']));
            }

            // Create account
            $customer_id = 0;
            if (isset($inputs['create_account']) && $inputs['create_account']) {
                $data = [
                    'first_name' => $inputs['billing']['first_name'],
                    'last_name' => $inputs['billing']['last_name'],
                    'email' => $inputs['email'],
                    'password' => md5($inputs['account_password']),
                ];

                $this->Customers->insert($data, false);
                $customer_id = $this->Customers->getLastInsertID();
            }

            // Payment method
            //$payment_method_list   = payment_methods();
            $payment_method_id = $inputs['payment_method'];
            $payment_method_issuer = isset($inputs['issuer'][$inputs['payment_method']]) ? $inputs['issuer'][$inputs['payment_method']] : '';
            $payment_method = $payment_method_id;

            $paymentResponse = $this->paymentProcess($inputs, $cart);

            if (isset($paymentResponse)) {
                $result = $paymentResponse->responseCode; // 00 == Success
                $message = $paymentResponse->responseMessage; // [ test system ] AUTHORISED
                $paymentsReference = $paymentResponse->transactionId; // pasref: 14610544313177922

                // Create order
                $data = [
                    'customer_id' => $customer_id,
                    'email' => $inputs['email'],
                    'phone' => $inputs['phone'],
                    'status' => 0,
                    'payment_method' => "globalPayments",
                    'sub_total' => $cart['subtotal'],
                    'tax' => $cart['tax'],
                    'net_total' => $cart['nettotal'],
                    'comments' => $inputs['comments'],
                    'custom_date' => $inputs['custom_date'],
                    'payment_id' => $paymentsReference
                ];

                $this->Orders->insert($data, false);
                $order_id = $this->Orders->getLastInsertID();

                // Order billing address
                $bill_data = $inputs['billing'];
                $bill_data['type'] = 1;
                $bill_data['order_id'] = $order_id;
                $bill_data['country'] = 'Pakistan';

                $this->OrderAddress->insert($bill_data, false);

                // Order shipping address
                if (isset($inputs['new_shipping_address']) && $inputs['new_shipping_address']) {
                    $ship_data = $inputs['shipping'];
                    $ship_data['type'] = 2;
                    $ship_data['order_id'] = $order_id;
                    $ship_data['country'] = 'Pakistan';

                    $this->OrderAddress->insert($ship_data, false);
                }

                // Order products
                foreach ($cart['items'] as $pro) {
                    $name = $pro['product_name'];

                    if ($pro['attribute']) {
                        $name .= ' - ' . implode(', ', isset($pro['attribute_name'][0]) ? $pro['attribute_name'][0] : []);
                    }

                    $product_data = [
                        'order_id' => $order_id,
                        'product_id' => $pro['product'],
                        'name' => $name,
                        'attributes' => 'small',//isset($pro['attribute_name'][0])  ? implode(',', $pro['attribute']):'-',
                        'price' => $pro['sale_price'] && $pro['sale_price'] > 0 ? $pro['sale_price'] : $pro['price'],
                        'quantity' => $pro['quantity'],
                    ];

                    $this->OrderProducts->insert($product_data, false);
                }

                // Clear cart
                $this->session->set_userdata('cart', '');

                $this->sendNotification($order_id);

                // Redirect to payment
                header("Location: " . $this->vars['thankyou_url'], true, 303);
                exit;
            }

            redirect(url('/' . $this->vars['checkout_url']));
        }

        redirect(url('/' . $this->vars['checkout_url']));

    }

    public function validate_unique_email($email)
    {
        $user = $this->Customers->find()->where('email', $email);

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }

    public function getDeliveryCharges()
    {
        $delivery_charges = 0;

        $inputs = $this->input->post();
        $inputs['postcode'] = strtolower(str_replace(' ', '', trim($inputs['postcode'])));

        $url = $url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyDwX7Hvp5q6fuQ8uwCEYARNwYIgJ-6_3mo&origins=" . $inputs['postcode'] . "&destinations=IG5&mode=driving&language=en-EN&sensor=false";

        $data = @file_get_contents($url);
        $result = json_decode($data, true);

        if (isset($result["rows"][0]["elements"][0]["distance"])) {

            $distance = array( // converts the units
                "meters" => $result["rows"][0]["elements"][0]["distance"]["value"],
                "kilometers" => $result["rows"][0]["elements"][0]["distance"]["value"] / 1000,
                "yards" => $result["rows"][0]["elements"][0]["distance"]["value"] * 1.0936133,
                "miles" => $result["rows"][0]["elements"][0]["distance"]["value"] * 0.000621371
            );

            if ($distance['miles'] >= 0 && $distance['miles'] <= 3) {
                if ($inputs['order_total'] <= 120) {
                    $delivery_charges = 0;
                }
            } else if ($distance['miles'] > 3 && $distance['miles'] <= 6) {
                if ($inputs['order_total'] <= 200) {
                    $delivery_charges = 20;
                }
            } else if ($distance['miles'] > 6 && $distance['miles'] <= 10) {
                if ($inputs['order_total'] <= 300) {
                    $delivery_charges = 30;
                }
            }

            $response = ['success' => true, 'miles' => $distance['miles'], 'delivery_charges' => $delivery_charges];

        } else {

            $response = ['success' => false, "message" => "Please enter a valid post code to place your order "];

        }
        echo json_encode($response);
    }

    public function sendNotification($order_id){
        $data = array();
        $push = new PushNotifications();
        $push->android($order_id, 'ee3ihygL8R0:APA91bFgEHyxObkqygnTf3NDcCOUXOxJJ7mTfizTVEf7QzCOsnd9g9y3FMB3K-PxZmWp14lpFzTmWT43c4KLterCTHJDgBd24JKjPVx7LVu-YRDIxg91_eVixUrT8vkXgtKkpAT3o1zI');
    }

    public function getOrderData($order_id){


        $orders = $this->Orders
            ->find()
            ->where('id', $order_id)
            ->get()
            ->row_array();

        $address = $this->OrderAddress
            ->find()
            ->where('order_id', $order_id)
            ->get()
            ->row_array();

        $products = $this->OrderProducts
            ->find()
            ->where('order_id', $order_id)
            ->get()
            ->result_array();

        $data = array();
        $data['data']['id'] = $orders['id'];
        $data['data']['reference'] = '#redBridge'.$orders['id'];
        $data['data']['user_id'] = $orders['customer_id'];
        $data['data']['user_id'] = $orders['customer_id'];
        $data['data']['total_amount_with_fee'] = $orders['net_total'];
        $data['data']['total_amount_with_fee'] = $orders['net_total'];
        $data['data']['delivery_fees'] =10;
        $data['data']['payment'] ='Cash';
        $data['data']['delivery_address'] = $address['address1']." ".$address['address2']." ".$address['postal_code'] ;

        $data['data']['status'] = 'Recevied' ;
        $data['data']['order_type'] = 'Delivery' ;
        $data['data']['order_type'] = 'Delivery' ;
        $data['data']['discounted_amount'] = 0 ;
        $data['data']['phone_number'] = $orders['phone'];
        $details = array();
        foreach ($products as $product){
            $p = array('id'=> $product['id'],'order_id'=>$order_id,'product_id'=>$product['product_id'],'product_name' =>$product['name'],'price'=>$product['price'],'quantity'=>$product['quantity'],'extras'=>null,'special_instructions'=>null);
            $details[] = $p;
        }
        $data['data']['details'] =$details;
        $data['message'] = 'your order status has been updated successfully';


        echo json_encode($data);
        exit;

    }

    public function paymentProcess($data, $cart) {

        $config = new ServiceConfigs\Gateways\GpEcomConfig();
        $config->merchantId = "redbr009";
        $config->accountId = "internet";
        $config->sharedSecret = "agdZM8nxG4";
        $config->serviceUrl = "https://pay.sandbox.realexpayments.com/pay";
        ServicesContainer::configureService($config);

        // create the card object
        $card = new CreditCardData();
        $card->number = "51";
        $card->expMonth = 12;
        $card->expYear = 2025;
        $card->cvn = "131";
        $card->cardHolderName = "James Mason";

        try {
            // process an auto-capture authorization
            $response = $card->charge($cart['nettotal']+$cart['tax'])
                ->withCurrency("GBP")
                ->execute();

            echo "<pre>";
            print_r($response);
            exit;

            return $response;

        } catch (ApiException $e) {

            $this->session->set_flashdata('error', '<p>' . $e->getMessage().'</p>');
            redirect(url('/' . $this->vars['checkout_url']));

            // TODO: Add your error handling here
        }
    }

}
