<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BlogCategory extends MY_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->model('Blog_categories_model', 'Category');
    }

    public function index()
    {
        $category = $this->Category
                        ->find()
                        ->where('slug', $this->uri->segment(2))
                        ->get()
                        ->row_array();

        if($category)
        {
            $this->seo = generate_meta($category);

            $page  = (int) $this->uri->segment(2);
            $page  = $page ? $page : 1;
            $limit = 10;
            $start = ($page - 1) * $limit;

            $model = $this->Blogs;

            $model->setAlias('b')
                ->find()
                ->select('b.id')
                ->where('b.status', 1)
                ->join('blogs_categories_map AS bc', 'bc.blog_id = b.id')
                ->join('blog_categories AS c', 'bc.category_id = c.id')
                ->where('c.id', $category['id'])
                ->group_by('b.id');

            $total = $model->count();

            $blogs = $model
                        ->setAlias('b')
                        ->find()
                        ->select('b.title, b.content, b.image, b.publish_date, s.slug, u.first_name, u.last_name, c.name AS category, c.slug AS category_slug')
                        ->join('seo_urls AS s', 'b.id = s.row_id AND s.tbl_name = "blogs"')
                        ->join('users AS u', 'u.id = b.author_id')
                        ->join('blogs_categories_map AS bc', 'bc.blog_id = b.id')
                        ->join('blog_categories AS c', 'bc.category_id = c.id')
                        ->where('b.status', 1)
                        ->where('c.id', $category['id'])
                        ->limit($limit, $start)
                        ->order_by('b.id', 'DESC')
                        ->group_by('b.id')
                        ->get()
                        ->result_array();

            $this->load->library('pagination');

            $config['base_url']   = url('/category/' . $category['slug']);
            $config['total_rows'] = $total;
            $config['per_page']   = $limit;

            $config['use_page_numbers']   = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['full_tag_open']  = '<ul class="pagination justify-content-center">';
            $config['full_tag_close'] = '</ul>';
            
            $config['cur_tag_open']   = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']  = '<span class="sr-only">(current)</span></span></li>';

            $config['num_tag_open']   = '<li class="page-item">';
            $config['next_tag_open']  = '<li class="page-item">';
            $config['prev_tag_open']  = '<li class="page-item">';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['last_tag_open']  = '<li class="page-item">';
            $config['num_tag_close']  = '</li>';
            
            $config['next_link']  = 'Next';
            $config['prev_link']  = 'Previous';
            $config['first_link'] = 'First';
            $config['last_link']  = 'Last';       
            $config['attributes'] = array('class' => 'page-link');
            
            $this->pagination->initialize($config);

            $this->load->front('blog-category', compact('category', 'blogs'));
        }
        else
        {
            show_404();
        }
    }
}