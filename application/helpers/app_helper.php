<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('dd'))
{
    function dd($array = '')
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }   
}

if (!function_exists('assets'))
{
    function assets($path = '')
    {
        return base_url('assets/'.$path);
    }   
}

if (!function_exists('admin_url'))
{
    function admin_url($path = '')
    {
        return base_url('admin/'.$path);
    }   
}

if (!function_exists('url'))
{
    function url($path = '')
    {
        return base_url($path);
    }   
}

if (!function_exists('is_admin'))
{
    function is_admin()
    {
        $ci =& get_instance();

        $user = $ci->session->has_userdata('admin_user') ? $ci->session->userdata('admin_user') : '';

        if($user)
        {
            $user = $ci->Users->find()
                        ->where('id', $user['id'])
                        ->where_in('role_id', explode(',', $ci->vars['backend-roles']))
                        ->get()
                        ->row_array();

            if($user)
            {
                return $user;
            }
        }

        return false;
    }   
}

if (!function_exists('is_customer'))
{
    function is_customer()
    {
        $ci =& get_instance();

        $user = $ci->session->has_userdata('logged_user') ? $ci->session->userdata('logged_user') : '';

        if($user)
        {
            $user = $ci->Customers->find()
                        ->where('id', $user['id'])
                        ->get()
                        ->row_array();

            if($user)
            {
                return $user;
            }
        }

        return false;
    }   
}

if (!function_exists('current_uid'))
{
    function current_uid()
    {
        $ci =& get_instance();

        $user = $ci->session->has_userdata('admin_user') ? $ci->session->userdata('admin_user') : '';

        if($user)
        {
            $user = $ci->Users->find()
                        ->where('id', $user['id'])
                        ->where_in('role_id', explode(',', $ci->vars['backend-roles']))
                        ->get()
                        ->row_array();

            if($user)
            {
                return $user['id'];
            }
        }

        return false;
    }   
}

if (!function_exists('is_request'))
{
    function is_request($controller = '')
    {
        $ci =& get_instance();

        return strtolower($ci->router->fetch_class()) == strtolower($controller);
    }   
}

if (!function_exists('file_manager'))
{
    function file_manager($field = '')
    {
        return assets('js/filemanager/dialog.php?akey=d357621c8fdb0300fe9db6d81a063a73&type=1&relative_url=1&field_id=' . $field);
    }   
}

if (!function_exists('uploaded_url'))
{
    function uploaded_url($file = '')
    {
        return assets('uploads/' . $file);
    }   
}

if (!function_exists('generate_meta'))
{
    function generate_meta($data = '')
    {
        $ci =& get_instance();

        $meta = [
            'title'  => $ci->vars['site']['site_title'],
            'desc'   => isset($data['meta_desc']) ? $data['meta_desc'] : '',
            'robots' => '',
        ];

        // Meta title
        if(isset($data['meta_title']) && $data['meta_title'])
        {
            $meta['title'] = $data['meta_title'];
        }
        elseif(isset($data['title']) && $data['title'])
        {
            $meta['title'] = $data['title'];
        }
        elseif(isset($data['name']) && $data['name'])
        {
            $meta['title'] = $data['name'];
        }

        // Robots
        if(!$ci->vars['site']['allow_index'])
        {
            $meta['robots'] = 'noindex,nofollow';
        }
        elseif(isset($data['index_follow']))
        {
            $meta['robots'] = $data['index_follow'];
        }

        // Replace placeholders
        $title = (isset($data['title']) && $data['title']) ? $data['title'] : ((isset($data['name']) && $data['name']) ? $data['name'] : '');

        $search = [
            '[SITE_NAME]',
            '[SITE_TITLE]',
            '[TITLE]'
        ];

        $replace = [
            $ci->vars['site']['site_name'],
            $ci->vars['site']['site_title'],
            $title
        ];

        $meta['title'] = str_replace($search, $replace, $meta['title']);
        $meta['desc']  = str_replace($search, $replace, $meta['desc']);

        return $meta;
    }
}

if (!function_exists('menu_items'))
{
    function menu_items($slug = '')
    {
        $ci =& get_instance();

        $ci->load->model('Menu_model', 'Menu');

        $items = $ci->Menu
                    ->setAlias('m')
                    ->find()
                    ->select('mi.label, mi.link, mi.attributes, su.slug')
                    ->join('menu_items AS mi', 'm.id = mi.menu_id')
                    ->join('seo_urls AS su', 'mi.page_id = su.row_id AND su.tbl_name = "pages"', 'left')
                    ->where('m.slug', $slug)
                    ->where('mi.deleted', '0')
                    ->where('mi.status', '1')
                    ->order_by('mi.sort_order', 'ASC')
                    ->get()
                    ->result_array();


        foreach ($items as $k => $v)
        {
            if($v['slug'])
            {
                $items[$k]['link'] = url($v['slug']);
            }

            unset($items[$k]['slug']);
        }

        return $items;
    }   
}

if (!function_exists('widget'))
{
    function widget($slug = '')
    {
        $ci =& get_instance();

        $ci->load->model('Widgets_model', 'Widgets');

        $widget = $ci->Widgets
                    ->find()
                    ->select('name, image, content')
                    ->where('slug', $slug)
                    ->get()
                    ->row_array();

        if(!$widget)
        {
            $widget = [
                'name' => '',
                'image' => '',
                'content' => '',
            ];
        }
        else
        {
            $widget['content'] = prepare_content($widget['content']);
        }
        
        return $widget;
    }   
}

if (!function_exists('prepare_content'))
{
    function prepare_content($content)
    {
        $ci =& get_instance();

        $content    = html_entity_decode($content);
        $shortcodes = [];

        preg_match_all('/\[(.+?)\]/', $content, $shortcodes);


        if(isset($shortcodes[0])) {
            foreach ($shortcodes[0] as $s) {
                $shortcode = str_replace(['[', ']'], '', $s);

                $parts  = explode(" ", $shortcode);
                $params = [];

                foreach ($parts as $p)
                {
                    if (strpos($p, '=') !== false)
                    {
                        list($key, $val) = explode("=", $p);
                        $params[$key] = trim($val, '"');
                    }
                }


                $template = $parts[0];


                if(file_exists(APPPATH . "views/shortcodes/{$template}.php"))
                {
                    $view = $ci->load->view('shortcodes/' . $template, $params, true);
                    $content = str_replace($s, $view, $content);
                }
            }
        }

        return $content;
    }
}

if (!function_exists('resize_img'))
{
    function resize_img($image, $thumb_width = 0, $thumb_height = 0)
    {
        if($thumb_width && $thumb_height)
        {
            $file = explode('/', $image);
            $file = end($file);

            $ext = explode('.', $file);
            $ext = end($ext);

            $newname = trim($file, '.'.$ext) . '-' . $thumb_width . 'x' . $thumb_height . '.' . $ext;
            $newpath = 'assets/cache/' . str_replace(array('../assets/', 'assets/'), '', str_replace($file, '', $image));

            $newfile = $newpath . $newname;

            if(!file_exists($newfile))
            {
                if (!file_exists($newpath))
                {
                    mkdir($newpath, 0755, true);
                }

                $image = 'assets/uploads/' . $image;

                switch ($ext)
                {
                    case 'jpeg':
                    case 'jpg':
                        $image = imagecreatefromjpeg($image);
                        break;
                    case 'png':
                        $image = imagecreatefrompng($image);
                        break;
                    case 'gif':
                        $image = imagecreatefromgif($image);
                        break;
                    default:
                        return assets(ltrim($newfile, 'assets/'));;
                        break;
                }

                $width = imagesx($image);
                $height = imagesy($image);

                $original_aspect = $width / $height;
                $thumb_aspect = $thumb_width / $thumb_height;

                if ($original_aspect >= $thumb_aspect)
                {
                   $new_height = $thumb_height;
                   $new_width = $width / ($height / $thumb_height);
                }
                else
                {
                   $new_width = $thumb_width;
                   $new_height = $height / ($width / $thumb_width);
                }

                $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );

                // Resize and crop
                imagecopyresampled($thumb,
                                   $image,
                                   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                                   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                                   0, 0,
                                   $new_width, $new_height,
                                   $width, $height);
                
                imagejpeg($thumb, $newfile, 80);
            }
        }
        else
        {
            $newfile = 'assets/uploads/' . $image;
        }
        
        return assets(ltrim($newfile, 'assets/'));
    }
}

if (!function_exists('format_money'))
{
    function format_money($amount)
    {
        return '£'.number_format($amount, 2);
    }
}

if (!function_exists('get_cart'))
{
    function get_cart()
    {
        $ci =& get_instance();

        return $ci->getCart();
    }   
}

if (!function_exists('site_name'))
{
    function site_name()
    {
        $ci =& get_instance();

        return $ci->vars['site']['site_name'];
    }
}

if (!function_exists('site_title'))
{
    function site_title()
    {
        $ci =& get_instance();

        return $ci->vars['site']['site_title'];
    }
}

if (!function_exists('shop_url'))
{
    function shop_url()
    {
        $ci =& get_instance();

        return url('/'. $ci->vars['shop_url']);
    }
}

if (!function_exists('cart_url'))
{
    function cart_url()
    {
        $ci =& get_instance();

        return url('/'. $ci->vars['cart_url']);
    }
}

if (!function_exists('checkout_url'))
{
    function checkout_url()
    {
        $ci =& get_instance();

        return url('/'. $ci->vars['checkout_url']);
    }
}

if (!function_exists('account_url'))
{
    function account_url()
    {
        $ci =& get_instance();

        return url('/'. $ci->vars['account_url']);
    }
}

if (!function_exists('wishlist_url'))
{
    function wishlist_url()
    {
        $ci =& get_instance();

        return url('/'. $ci->vars['wishlist_url']);
    }
}

if (!function_exists('neworders_url'))
{
    function neworders_url()
    {
        $ci =& get_instance();

        return url('/'. $ci->vars['neworders_url']);
    }
}

if (!function_exists('order_statuses'))
{
    function order_statuses($lang = 'en')
    {
        $status = [
            'en' => [
                0 => 'Confirmation Pending',
                1 => 'Confirmed',
                2 => 'Processing',
                3 => 'Dispatched',
                4 => 'Completed',
                5 => 'Returned'
            ],
            'dt' => [
                0 => 'Confirmation Pending',
                1 => 'Confirmed',
                2 => 'Processing',
                3 => 'Dispatched',
                4 => 'Completed',
                5 => 'Returned'
            ]
        ];

        return $status[$lang];
    }
}

if (!function_exists('mollie_client'))
{
    function mollie_client()
    {
        $ci =& get_instance();

        $mollie = new \Mollie\Api\MollieApiClient();
        $mollie->setApiKey($ci->vars['mollie_api_key']);

        return $mollie;
    }
}

if (!function_exists('payment_methods'))
{
    function payment_methods()
    {
        $list = [];

        $mollie = mollie_client();

        try {
            $methods = $mollie->methods->all(["include" => "issuers"]);

            foreach ($methods as $method){
                $list[] = [
                    'id' => $method->id,
                    'name' => $method->description,
                    'image' => $method->image->svg,
                    'issuers' => $method->issuers,
                ];
            }
        } catch (\Mollie\Api\Exceptions\ApiException $e) {
        }

        return $list;
    }
}

if (!function_exists('sendcloud_client'))
{
    function sendcloud_client()
    {
        $ci =& get_instance();

        $connection = new \Picqer\Carriers\SendCloud\Connection($ci->vars['sendcloud_api_key'], $ci->vars['sendcloud_api_secret']);
        $sendcloud = new \Picqer\Carriers\SendCloud\SendCloud($connection);

        return $sendcloud;
    }
}

if (!function_exists('limit_text'))
{
    function limit_text($text, $limit)
    {
        $text = strip_tags($text);
        
        if (str_word_count($text, 0) > $limit)
        {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }

        return $text;
    }
}