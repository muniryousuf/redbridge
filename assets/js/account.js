$(function(){
    if($('#frm-login').length) {
        $('#frm-login').validate();
    }

    if($('#frm-register').length) {
        $('#frm-register').validate();
    }

    if($('#frm-pwdreset').length) {
        $('#frm-pwdreset').validate();
    }

    if($('#frm-pwdupdate').length) {
        $('#frm-pwdupdate').validate();
    }

    if($('#frm-account').length) {
        $('#frm-account').validate();
    }

    if($('#frm-address').length) {
        $('#frm-address').validate();
    }
});