function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

var ckeOptions = {
    filebrowserImageBrowseUrl: '/assets/js/filemanager/dialog.php?akey=d357621c8fdb0300fe9db6d81a063a73&type=1&editor=ckeditor',
    filebrowserBrowseUrl: '/assets/js/filemanager/dialog.php?akey=d357621c8fdb0300fe9db6d81a063a73&type=1&editor=ckeditor',
};

$(function(){
    if($('textarea.editor').length) {
        $('textarea.editor').ckeditor(ckeOptions);
    }

    if($('.upload-btn').length) {
        $('.upload-btn').fancybox({ 
            width: 900,
            minHeight: 600,
            type: 'iframe',
            autoScale: true
        });
    }

    if($('.select2').length) {
        $('.select2').select2();
    }

    if($('.select2-tags').length) {
        $('.select2-tags').select2({
            tags: true,
            tokenSeparators: [',']
        });
    }

    $(document).on('keydown', ".numberonly", function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

function responsive_filemanager_callback(field_id) {
    if($('#' + field_id + '-preview').length) {
        var url = $('#' + field_id).val();
        $('#' + field_id + '-preview').attr('src', '/assets/uploads/' + url);
    }

    if($('#media-details').length) {
        var img = $('#' + field_id).val();
        $('#image-url').val(uploadUrl + img);
        $('#image-shortcode').val('[image src="' + img + '" width="" height=""]');
        $('#iframe-div').addClass('col-sm-9').removeClass('col-sm-12');
        $('#media-details').show();

        $('#' + field_id + '-preview').on('load', function(){
            $('#image-shortcode').val('[image src="' + img + '" width="' + $(this).prop('naturalWidth') + '" height="' + $(this).prop('naturalHeight') + '"]');
        });
    }
}