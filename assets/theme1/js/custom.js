$(function(){



	$(document).on('keydown', ".numberonly", function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(document).on("click",".actQuantityDiscount",function(){
        let quantity = 1 ;
        $('label.checked').removeClass("checked");
        if ($(this).is(":checked")) 
        {
            $(".actQuantityDiscount").not(this).prop("checked",false);
            $(this).closest('label').addClass("checked");
            let amount   = $(this).val();
            quantity     = parseInt($(this).data('quantity'));   
        }
        $('input[name="quantity"]').val(quantity);
        fnManageProductAttributes();
    });

    $(".sidebar_open_btn").click(function(){
        $('aside#mobile-menu').removeClass('d-none').addClass('d-block').css("right", 0);
    });
    $('.sidebar_close_btn').click(function(){
        $('aside').removeClass('d-block').addClass('d-none').css("right", -500);
    });

    $('#mobile-order-btn').click(function(){
        if(!$('#mobile-order-div .frm #frm-addtocart').length) {
            $('#mobile-order-div .frm').append($('#frm-addtocart').detach());
            $('#frm-addtocart').removeClass('d-none');
        }
        $('aside#mobile-order-div').removeClass('d-none').addClass('d-block').css("right", 0);
    });

    var hideCookie = localStorage.getItem('hideCookie');
    if (hideCookie == null || hideCookie == '') {
        $('#cookie-popup').addClass('show');
    }

    $('#cookie-popup button.accept').click(function(){
        localStorage.setItem('hideCookie', 1);
        $('#cookie-popup').removeClass('show');
    });

    $('.product-quantity .btn-plus').click(function(e){
        e.preventDefault();
        var qtyField = $(this).parents('.product-quantity').find('.quantity');
        var quantity = parseInt(qtyField.val());
        qtyField.val(quantity + 1);
        $('.actQuantityDiscount').prop("checked",false);
        fnManageProductAttributes();
    });

    $('.product-quantity .btn-minus').click(function(e){
        e.preventDefault();
        var qtyField = $(this).parents('.product-quantity').find('.quantity');
        var quantity = parseInt(qtyField.val());

        if(quantity > 1) {
            qtyField.val(quantity - 1);
            fnManageProductAttributes();
        }
        
        $('.actQuantityDiscount').prop("checked",false);
    });

    $('.quantity').blur(function(){
        if($(this).val() == '' || $(this).val() == 0) {
            $(this).val(1);
        }
    });

    if($('#frm-addtocart').length) {
        $('.product-attribute').change(function(){
            $('#cart-btn-div').show();
            $('#stock-alert-div').hide();

            var c = true;
            $('.product-attribute').each(function(){
                c = $(this).val() ? c : false;
            });

            if(c){
                var attr;
                var p = '';
                var hasStock = false;

                $.each(product_attrs, function(i1, v1){
                    var matched = [];
                    var attrlen = 0;

                    $.each(v1.attributes, function(i2, v2){
                        if($('#attr' + i2).val() && $('#attr' + i2).val() == v2) {
                            matched.push(v2);
                        }

                        attrlen++;
                    });

                    if(matched.length == attrlen) {
                        attr = v1;
                    }
                });
                if ($(".actQuantityDiscount:checked").length == 0)
                {
                    if(typeof attr  !== "undefined") {
                        hasStock = attr.stock > 0 ? true : false;

                        if(attr.sale_price) {
                            p = '<del class="text_gray">' + attr.price_formated + '</del> <span class="text_black">' + attr.sale_price_formated + '</span>';
                        } else {
                            p = '<span class="text_black">' + attr.price_formated + '</span>';
                        }
                    } else {
                        if(sale_price) {
                            p = '<del class="text_gray">' + price + '</del> <span class="text_black">' + sale_price + '</span>';
                        } else {
                            p = '<span class="text_black">' + price + '</span>';
                        }
                    }
                }

                if(hasStock == false) {
                    $('#cart-btn-div').hide();
                    $('#stock-alert-div').show();
                    $('#restock_btn').prop('disabled', false);
                    $('#restock_btn').html('Abonneren');
                }

                $('#price').html(p);
            }
        });
    }

    $('#btn-addtocart').click(function() {
        var valid = true;
        $('#frm-addtocart .product-attribute').each(function(){
            if($(this).val() == '') 
            {
                valid = false;
            }
        });

        if(valid) {

            $('#frm-addtocart').submit();
        } else {
            alert('Please select product options before placing this product in the shopping cart.\n');
        }
    });

    if($('#frm-checkout').length) {
        $('#frm-checkout').validate({
              rules: {
                    phone: {
                      required: true,
                      number: true
                    }
                  }
        });

        $('#create_account').change(function(){
            if($(this).is(':checked')) {
                $('#password-div').slideDown();
            } else {
                $('#password-div').slideUp();
            }
        });

        $('#new_shipping_address').change(function(){
            if($(this).is(':checked')) {
                $('#shipping-div').slideDown();
            } else {
                $('#shipping-div').slideUp();
            }
        });

        $('.payment-method').change(function() {
            var el = $('.payment-method:checked');
            $('.single-payment-method .issuers').slideUp();
            el.parents('.single-payment-method').find('.issuers').slideDown();
        });

        $('.payment-method:checked').trigger('change');
    }

    $('.wishlist_btn').click(function(e){
        e.preventDefault();
        var type = '';

        if($(this).find('.fa-heart').hasClass('fas')) {
            type = 'remove';
            $(this).find('.fa-heart').removeClass('fas').addClass('far');
        }
        else if($(this).find('.fa-heart').hasClass('far')) {
            type = 'add';
            $(this).find('.fa-heart').removeClass('far').addClass('fas');
        }

        $.ajax({
            type: 'POST',
            url: '/wishlist/' + type,
            data: {id: $(this).attr('data-id')},
            success: function(response) {
                $('.wishlist_count').html(response);
            }
        });
    });

    $('.remove_wishlist').click(function(e){
        e.preventDefault();

        var id = $(this).attr('data-id');

        $(this).parents('.single-pro-div').remove();

        $.ajax({
            type: 'POST',
            url: '/wishlist/remove',
            data: {id: id},
            success: function(response) {
                $('.wishlist_count').html(response);

                if(!$('.single-pro-div').length) {
                    location.reload();
                }
            }
        });
    });

    $('#restock_btn').click(function(e){
        e.preventDefault();
        var email = $('#restock_email').val();
        var pid = $('#frm-addtocart input[name="product"]').val();
        var attr = [];

        $('.product-attribute').each(function(){
            attr.push($(this).val());
        });

        attr = attr.join(',');

        if(email) {
            if(!isValidEmail(email)) {
                alert('Vul alstublieft een geldig e-mailadres in');
            } else {
                $('#restock_btn').prop('disabled', true);
                $('#restock_btn').html('Wacht alsjeblieft...');

                $.ajax({
                    type: 'POST',
                    url: '/ajax/restock_subscribe',
                    data: {email: email, pid: pid, attributes: attr},
                    success: function(response) {
                        if(response) {
                            alert(response);    
                        }
                        
                        $('#restock_btn').prop('disabled', false);
                        $('#restock_btn').html('Abonneren');
                    }
                });
            }
        } else {
            alert('Vul alstublieft uw e-mail adres in');
        }
    });

    if($('#frm-contact').length) {
        $('#frm-contact').validate({
            submitHandler: function(form) {
                $('#frm-contact button[type="submit"]').prop('disabled', true);
                $('#frm-contact button[type="submit"]').html('Wacht alsjeblieft...');

                $.ajax({
                    url: '/ajax/contact',
                    type: form.method,
                    data: $(form).serialize(),
                    dataType: 'JSON',
                    success: function(response) {
                        if(response.success) {
                            $('#frm-contact').replaceWith(response.message);    
                        } else {
                            $('#frm-contact .err').html(response.message);
                            $('#frm-contact button[type="submit"]').prop('disabled', false);
                            $('#frm-contact button[type="submit"]').html('Bericht versturen');
                        }
                    }
                });
            }
        });
    }

    if($('#frm-return').length) {
        $('#frm-return').validate({
            submitHandler: function(form) {
                $('#frm-return button[type="submit"]').prop('disabled', true);
                $('#frm-return button[type="submit"]').html('Wacht alsjeblieft...');

                $.ajax({
                    url: '/ajax/returnform',
                    type: form.method,
                    data: $(form).serialize(),
                    dataType: 'JSON',
                    success: function(response) {
                        if(response.success) {
                            $('#frm-return').replaceWith(response.message);    
                        } else {
                            $('#frm-return .err').html(response.message);
                            $('#frm-return button[type="submit"]').prop('disabled', false);
                            $('#frm-return button[type="submit"]').html('Verzenden');
                        }
                    }
                });
            }
        });
    }

    $(".owl-carousel").owlCarousel({
        autoPlay: 3000,
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        center: true,
        autoPlay : true,
        navigation : true,
        navigationText : ["<i class='fas fa-angle-left'></i>","<i class='fas fa-angle-right'></i>"],
        rewindNav : false,
        loop:true,
        responsive: {
            600: {
                items: 2
            }
        }   
    });
    $("#categories_carousel .card-footer,#categories_carousel #categorypointer").each(function(){
        if ($(this).text().length > 20) {
            $(this).text($(this).text().substr(0, 6));
            $(this).append('...');
        }
    });
        
});

function isValidEmail(email) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
}

function fnManageProductAttributes() 
{
    let productQuantity =  parseInt($('input[name="quantity"]').val());
    let proAttrRowCount  =  Math.ceil(proAttrCount/3);
    $(".proAttrRow").slice(proAttrRowCount).remove();
    for (var a = 1; a < productQuantity; a++) 
    {
        for (var i = 0; i < proAttrRowCount; i++) 
        {
           let newProAttrRow = $( ".proAttrRow" ).eq(i).clone();
           newProAttrRow.find('select').each(function()
           {
               $(this).prop('name',$(this).prop('name').replace(/^attribute\[(\d+)\]/,'attribute\['+a+'\]'))
           });
           $(".proAttrRow").last().after(newProAttrRow);
        }    
    }
}

function startTimer()
{
    var t = new Date();
    t.setSeconds(t.getSeconds() + 60);
    var countDownDate = t.getTime();

    timer = setInterval(function() {      
      var now = new Date().getTime();
      var distance = countDownDate - now;
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      $("#confirmationTimer").html(seconds + ":00");
      if (distance < 0) {
        clearInterval(timer);

        $("#confirmationTimer").html("");

        $("#btnVerify").remove();

        $("#ConfirmationDialogue .modal-footer").append('<button type="button" class="btn btn-danger" id="btnResend"> <span class="spinner-border spinner-border-sm" style="display: none" role="status" aria-hidden="true"></span> Resend Code</button> ');

      }
    }, 1000);
}

function sendVerificationCode(fnCallBack)
{
    
}
